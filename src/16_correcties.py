"""
In dit script worden de peilen en bodemhoogtes van enkele systemen gecorrigeerd.
Hierbij wordt gezorgd dat:
- bodemhoogte/peil_tertiair >= peil_secundair >= peil_primair
- hoogte_buis/sof/greppel >= peil_primair_zomer
- hoogte_buis/sof/greppel >= peil_secundair_zomer
- er geen locaties zijn met NoData in een parameter, terwijl er wel een sloot ligt

Bij de laatste controle worden de infiltratiefactoren die al in het LHM bestaan ook ingeladen.

Er worden ook grids weggeschreven met het verschil tussen de gecorrigeerde en ongecorrigeerde data. 
"""
from os import path
import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

## Zorg ervoor dat geldt:
# peil_tertiair >= peil_secundair >= peil_primair
for seizoen in ['winter', 'zomer']:
    tertiair_peil = xr.open_dataarray(path_input[f"peil_tertiair_{seizoen}_buitenland"])
    secundair_peil = xr.open_dataarray(path_input[f"peil_secundair_{seizoen}_buitenland"])
    primair_peil = xr.open_dataarray(path_input[f"peil_primair_{seizoen}_buitenland"])
    tertiair_bodemhoogte = xr.open_dataarray(path_input[f"bodemhoogte_tertiair_{seizoen}_buitenland"])
    # Secundair peil >= primair peil
    # Dus zet primair peil op de plaatsen waar secundair peil < primair peil

    secundair_peil = xr.where(
        (secundair_peil.notnull()) & (primair_peil.notnull()) & (secundair_peil<primair_peil),
        primair_peil, 
        secundair_peil
    )
    secundair_peil.to_netcdf(path_output[f"peil_secundair_{seizoen}_gecorrigeerd_nc"])
    imod.idf.save(path_output[f"peil_secundair_{seizoen}_gecorrigeerd_idf"], secundair_peil)

    # Tertiair peil >= secundair peil
    # Dus zet secundair peil op de plaatsen waar tertiair peil < secundair peil
    tertiair_peil = xr.where(
        (tertiair_peil.notnull())&(secundair_peil.notnull()) & (tertiair_peil<secundair_peil),
        secundair_peil, 
        tertiair_peil
    )

    tertiair_peil.to_netcdf(path_output[f"peil_tertiair_{seizoen}_gecorrigeerd_nc"])
    imod.idf.save(path_output[f"peil_tertiair_{seizoen}_gecorrigeerd_idf"], tertiair_peil)
    
    # Tertiair bodemhoogte = tertiair peil >= secundair peil
    # Dus zet secundair peil op de plaatsen waar tertiair peil < secundair peil
    tertiair_bodemhoogte = xr.where(
        (tertiair_bodemhoogte.notnull())&(secundair_peil.notnull()) & (tertiair_bodemhoogte<secundair_peil),
        secundair_peil, 
        tertiair_bodemhoogte
    )

    tertiair_bodemhoogte.to_netcdf(path_output[f"bodemhoogte_tertiair_{seizoen}_gecorrigeerd_nc"])
    imod.idf.save(path_output[f"bodemhoogte_tertiair_{seizoen}_gecorrigeerd_idf"], tertiair_bodemhoogte)
    
    

## Pas de hoogte van de buisdrainage, sof en greppels
# aan aan de hoogte van het zomerpeil van primair & secundair
for soort in ['buisdrainage', 'SOF', 'greppels']:
    bodemhoogte = xr.open_dataarray(path_input[f"bodemhoogte_{soort}_buitenland"])
    primair_peil = xr.open_dataarray(path_input["peil_primair_zomer_buitenland"])
    # ES: om een magische reden werkt het script niet als we de volgende variable
    # 'secundair_peil' noemen. Het bestand wordt dan namelijk niet goed gesloten o.i.d.
    # Daarom heet het secundaire peil nu 'peil' in dit stukje script:
    peil = xr.open_dataarray(path_output["peil_secundair_zomer_gecorrigeerd_nc"])
    peil.close() # Close zodat het bestand straks overschreven kan worden

    # hoogte_buis/sof/greppel >= peil_primair_zomer
    bodemhoogte = xr.where(
        (bodemhoogte.notnull()) & (primair_peil.notnull()) & (bodemhoogte<primair_peil),
        primair_peil,
        bodemhoogte
    )
    #hoogte_buis/sof/greppel >= peil_secundair_zomer
    bodemhoogte = xr.where(
        (bodemhoogte.notnull()) & (peil.notnull()) & (bodemhoogte<peil),
        peil,
        bodemhoogte
    )
    

    # bodemhoogte niet hoger dan AHN 
    AHN = imod.idf.open(path_input["AHN_F250"])
    bodemhoogte = xr.where(
        (bodemhoogte.notnull()) & (bodemhoogte>AHN),
        AHN,
        bodemhoogte
        )

    bodemhoogte.to_netcdf(path_output[f'bodemhoogte_{soort}_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'bodemhoogte_{soort}_gecorrigeerd_idf'], bodemhoogte)

## Vul NoData plekken in op locaties waar één param bekend is bij de sloten
# Aan het eind wordt ook de infiltratiefactorgrids aangemaakt
for sloot in ['primair', 'secundair', 'tertiair']:
    conductance = imod.idf.open(path_input[f'cond_leakage_{sloot}'])
    for seizoen in ['winter', 'zomer']:
        if sloot == 'primair': #Primair is verder niet gecorrigeerd in dit script, dus open de input:
            peil = xr.open_dataarray(path_input[f'peil_{sloot}_{seizoen}_buitenland'])
        else:
            peil = xr.open_dataarray(path_output[f'peil_{sloot}_{seizoen}_gecorrigeerd_nc'])
        
        if sloot == 'tertiair':
            bodemhoogte = xr.open_dataarray(path_output[f'bodemhoogte_{sloot}_{seizoen}_gecorrigeerd_nc']) 
        else:
            bodemhoogte = xr.open_dataarray(path_input[f'bodemhoogte_{sloot}_{seizoen}_buitenland'])
        
        if sloot == 'primair':
            bodemhoogte = xr.where(bodemhoogte>peil, peil, bodemhoogte)
            
        peil = xr.where(
            (peil.isnull()) # geen peil bekend
            &               # maar wel één van de andere params bekend:
            (conductance.notnull()|bodemhoogte.notnull()),
            imod.prepare.fill(peil), # nodata plekken opgevuld met nearest value
            peil
        )

        bodemhoogte = xr.where(
            (bodemhoogte.isnull()) # geen hoogte bekend
            &                      # maar wel één van de andere params bekend:
            (conductance.notnull()|peil.notnull()),
            imod.prepare.fill(bodemhoogte), # nodata plekken opgevuld met nearest value
            bodemhoogte
        )

        conductance = xr.where(
            (conductance.isnull()) # geen cond bekend
            &                      # maar wel één van de andere params bekend:
            (bodemhoogte.notnull()|peil.notnull()),
            imod.prepare.fill(conductance), # nodata plekken opgevuld met nearest value
            conductance
        )
        
        if sloot == 'primair': 
            peil_42 = imod.idf.open(path_input[f'peil_lhm42_{sloot}'])
            peil = xr.where(peil_42.notnull(),peil,np.nan)
            bodemhoogte = xr.where(peil_42.notnull(),bodemhoogte,np.nan)
            conductance = xr.where(peil_42.notnull(),conductance,np.nan)
            

            
        peil.close()
        peil.to_netcdf(path_output[f'peil_{sloot}_{seizoen}_gecorrigeerd_nc'])
        imod.idf.save(path_output[f'peil_{sloot}_{seizoen}_gecorrigeerd_idf'], peil)
        bodemhoogte.to_netcdf(path_output[f'bodemhoogte_{sloot}_{seizoen}_gecorrigeerd_nc'])
        imod.idf.save(path_output[f'bodemhoogte_{sloot}_{seizoen}_gecorrigeerd_idf'], bodemhoogte)
        
        
    
    conductance.to_netcdf(path_output[f'cond_leakage_{sloot}_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'cond_leakage_{sloot}_gecorrigeerd_idf'],conductance)

    # Infiltratiefactoren aanmaken (dit moet later anders, maar nu de data overnemen van LHM):
    infiltratiefactor_mozart = imod.idf.open(path_input[f'infiltratie_mozart_{sloot}'])
    infiltratiefactor_modflow = imod.idf.open(path_input[f'infiltratie_modflow_{sloot}'])

    # Het grid met peilen bevat nu de juiste locaties waar de sloten liggen
    infiltratiefactor_mozart = xr.where(
        (peil.notnull()) & (infiltratiefactor_mozart.notnull()), # Factor voor sloot is bekend
        infiltratiefactor_mozart,
        # Als de factor niet bekend is voor een sloot:
        imod.prepare.fill(infiltratiefactor_mozart).where((peil.notnull()) & (infiltratiefactor_mozart.isnull()))  
    )

    infiltratiefactor_modflow = xr.where(
        (peil.notnull()) & (infiltratiefactor_modflow.notnull()), # Factor voor sloot is bekend
        infiltratiefactor_modflow,
        # Als de factor niet bekend is voor een sloot:
        imod.prepare.fill(infiltratiefactor_modflow).where((peil.notnull()) & (infiltratiefactor_modflow.isnull()))  
    )

    infiltratiefactor_mozart.to_netcdf(path_output[f'infiltratiefactor_{sloot}_mozart_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'infiltratiefactor_{sloot}_mozart_gecorrigeerd_idf'], infiltratiefactor_mozart)
    infiltratiefactor_modflow.to_netcdf(path_output[f'infiltratiefactor_{sloot}_modflow_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'infiltratiefactor_{sloot}_modflow_gecorrigeerd_idf'], infiltratiefactor_modflow)

## Vul NoData plekken in op locaties waar één param bekend is bij de drains
for systeem in ['buisdrainage', 'SOF', 'greppels']:
    conductance = xr.open_dataarray(path_input[f'conductance_{systeem}_buitenland'])
    bodemhoogte = xr.open_dataarray(path_output[f'bodemhoogte_{systeem}_gecorrigeerd_nc'])
    conductance.close() # Close zodat het bestand straks overschreven kan worden
    bodemhoogte.close() # Close zodat het bestand straks overschreven kan worden

    bodemhoogte = xr.where(
        (bodemhoogte.isnull()) # geen hoogte bekend
        &                      
        (conductance.notnull()),# maar wel conductance bekend
        imod.prepare.fill(bodemhoogte), # nodata plekken opgevuld met nearest value
        bodemhoogte
    )
    
    conductance = xr.where(
        (conductance.isnull()) # geen cond bekend
        &                      
        (bodemhoogte.notnull()),# maar wel hoogte bekend
        imod.prepare.fill(conductance), # nodata plekken opgevuld met nearest value
        conductance
    )    
  
        
    bodemhoogte.to_netcdf(path_output[f'bodemhoogte_{systeem}_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'bodemhoogte_{systeem}_gecorrigeerd_idf'], bodemhoogte)
    conductance.to_netcdf(path_output[f'conductance_{systeem}_gecorrigeerd_nc'])
    imod.idf.save(path_output[f'conductance_{systeem}_gecorrigeerd_idf'], conductance)


## Maak rasters met de verschillen tov de ongecorrigeerde grids

for filename in path_input: 
    if filename.endswith('buitenland'):
        filename_out = filename[:-11]+'_gecorrigeerd_nc'
        filename_diff = filename[:-11]+'_diff'
        param_in = xr.open_dataarray(path_input[filename])
        param_correctie = xr.open_dataarray(path_output[filename_out])
        diff = param_correctie - param_in

        diff.to_netcdf(path_output[f'{filename_diff}_nc'])
        imod.idf.save(path_output[f'{filename_diff}_idf'], diff)