"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

dx, xmin, xmax, dy, ymin, ymax = (25.0, 0.0, 300000.0, -25.0, 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

LSW_raster = imod.prepare.gdal_rasterize(path_input['LSW_type'], column='TYPEint', like=like_25m)
LSW_raster = LSW_raster.where(LSW_raster.notnull(), 1) 
primair_watergang = imod.rasterio.open(path_input['primair_watergang'])
primair_vrijafwater = xr.where(primair_watergang == 1, primair_watergang.where(LSW_raster != 1, np.nan), np.nan)
primair_peilgestuurd = xr.where(primair_watergang == 1, primair_watergang.where(LSW_raster == 1, np.nan), np.nan)
primair_vrijafwater.to_netcdf(path_output['primair_vrijafwater'])
primair_peilgestuurd.to_netcdf(path_output['primair_peilgestuurd'])

secundair_watergang = imod.rasterio.open(path_input['secundair_watergang'])
secundair_vrijafwater = xr.where(secundair_watergang == 1, secundair_watergang.where(LSW_raster != 1, np.nan), np.nan)
secundair_peilgestuurd = xr.where(secundair_watergang == 1, secundair_watergang.where(LSW_raster == 1, np.nan), np.nan)
secundair_vrijafwater.to_netcdf(path_output['secundair_vrijafwater'])
secundair_peilgestuurd.to_netcdf(path_output['secundair_peilgestuurd'])

tertiair_watergang = imod.rasterio.open(path_input['tertiair_watergang'])
tertiair_vrijafwater = xr.where(tertiair_watergang == 1, tertiair_watergang.where(LSW_raster != 1, np.nan), np.nan)
tertiair_peilgestuurd = xr.where(tertiair_watergang == 1, tertiair_watergang.where(LSW_raster == 1, np.nan), np.nan)
tertiair_vrijafwater.to_netcdf(path_output['tertiair_vrijafwater'])
tertiair_peilgestuurd.to_netcdf(path_output['tertiair_peilgestuurd'])

