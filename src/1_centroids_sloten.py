'''
This script prepares the model area, by clipping the TOP10 and finding the centroids of all waterdelen.
Er wordt gebruik gemaakt van de GDAL package ogr2ogr. 
Deze statements worden normaliter als commanline aangeroepen, maar nu wordt hier de Python package subprocess gebruikt.
Het format van een ogr2ogr command is als volgt: outputfile - inputfile - commando_wat_er_moet_gebeuren
Er wordt gebruik gemaakt van SQL voor de ogr2ogr commando's
'''

import subprocess # Package to run command line
import os 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

xmin_NL = 0
xmax_NL = 300000
ymin_NL = 300000
ymax_NL = 625000

xmin = xmin_NL
xmax = xmax_NL
ymin = ymin_NL
ymax = ymax_NL

 
# Clip TOP10 om alleen een subgebiedje te draaien
print('Clipping')
process = subprocess.Popen(
    args=( 
        'ogr2ogr ' # package
        f'{path_output["slootGebied"]} -nln slootGebied ' # uitvoer
        f'-clipsrc {xmin} {ymin} {xmax} {ymax} ' # invoer
        f'{path_input["top10nl_Waterdeel"]} top10nl_Waterdeel_lijn' # commando
    ),
)
stdout, stderr = process.communicate()

# find centroids & copy id column
print('Finding centroids')
process = subprocess.Popen(
    args=( 
        'ogr2ogr '
        f'{path_output["centroidGebiedgpkg"]} -nln centroidGebied '
        f'{path_output["slootGebied"]} slootGebied '
        '-dialect sqlite -sql "SELECT ST_Centroid(geom) AS geom, lokaalid FROM slootGebied"'
    ),
)
stdout, stderr = process.communicate()

# create csv of centroids
print('Create centroid csv')
process = subprocess.Popen(
    args=( 
        'ogr2ogr -f CSV '
        f'{path_output["centroidGebiedcsv"]} '
        f'{path_output["centroidGebiedgpkg"]} centroidGebied '
        '-lco GEOMETRY=AS_XYZ'

    ),
)
stdout, stderr = process.communicate()

## Vlakken
 
# Clip TOP10 om alleen een subgebiedje te draaien
print('Clipping vlakken')
process = subprocess.Popen(
    args=( 
        'ogr2ogr '
        f'{path_output["slootGebied_vlakken"]} -nln slootGebied_vlakken '
        f'-clipsrc {xmin} {ymin} {xmax} {ymax} ' #xmin, ymin, xmax, ymax
        f'{path_input["top10nl_Waterdeel"]} top10nl_Waterdeel_vlak'
    ),
)
stdout, stderr = process.communicate()

# find centroids & copy id column
print('Finding centroids van vlakken')
process = subprocess.Popen(
    args=( 
        'ogr2ogr '
        f'{path_output["centroidGebied_vlakkengpkg"]} -nln centroidGebied_vlakken '
        f'{path_output["slootGebied_vlakken"]} slootGebied_vlakken '
        '-dialect sqlite -sql "SELECT ST_Centroid(geom) AS geom, lokaalid FROM slootGebied_vlakken"'
    ),
)
stdout, stderr = process.communicate()

# create csv of centroids
print('Create centroid csv van vlakken')
process = subprocess.Popen(
    args=( 
        'ogr2ogr -f CSV '
        f'{path_output["centroidGebied_vlakkencsv"]} '
        f'{path_output["centroidGebied_vlakkengpkg"]} centroidGebied_vlakken '
        '-lco GEOMETRY=AS_XYZ'

    ),
)
stdout, stderr = process.communicate()
