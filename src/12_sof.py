"""
Surface Overland Flow (SOF)
===========================

In dit script wordt de locatie en de weerstand van de surface overland
flow (SOF) bepaald. 

SOF bevindt zich op alle locaties waar landoppervlak aanwezig is. Dit is alleen op locaties
waar in het oorspronkelijke TOP10 bestand géén watervlakken (meertjes etc.) aanwezig zijn.

De hoogte is maaiveldhoogte.

De conductance van SOF is het landoppervlak (celoppervlak) per cel, gedeeld door de
weerstand. De weerstand is vastgesteld op 30 dagen.
"""
import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

modus_2d_regridder = imod.prepare.Regridder(method="mode")

watervlakken_raster = imod.idf.open(path_input["watervlakken"])
watervlakken_raster = modus_2d_regridder.regrid(watervlakken_raster, like_25m)
AHN = xr.open_dataarray(path_input["AHN"])
pleistocene = imod.rasterio.open(path_input['pleistocene'])
                
bodemhoogte_SOF = xr.where(watervlakken_raster.isnull(), (AHN - params['SOF_diepte_holoceen']), np.nan)
bodemhoogte_SOF = xr.where((pleistocene == 2)&(watervlakken_raster.isnull()),(AHN - params['SOF_diepte_pleistocene']),bodemhoogte_SOF)
bodemhoogte_SOF.to_netcdf(path_output["bodemhoogte_SOF"])

conductance_SOF = xr.where(watervlakken_raster.isnull(), params['celoppervlak_klein'] / params["SOF_weerstand"], np.nan)
conductance_SOF.to_netcdf(path_output["conductance_SOF"])
