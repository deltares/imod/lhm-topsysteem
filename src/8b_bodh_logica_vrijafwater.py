"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))


hydrotypes = gpd.read_file(path_input['hydrotypen'])
waterdiepte_tabel = pd.read_csv(path_input['waterdiepte_hydrotype'], index_col='hydrotype2')
regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer'])

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

hydrotypes = hydrotypes.dissolve(by='hydrotype2')
waterdiepte = pd.concat([hydrotypes, waterdiepte_tabel], axis=1)
waterdiepte_secundair = imod.prepare.rasterize(waterdiepte, column='sloot13', like=regiopeil_zomer)
waterdiepte_primair = imod.prepare.rasterize(waterdiepte, column='sloot36', like=regiopeil_zomer)


#-------------
## Primair
#-------------
primair_zomerpeil = xr.open_dataarray(path_input['primair_peil_zomer_vrijafwater'])
primair_winterpeil = xr.open_dataarray(path_input['primair_peil_winter_vrijafwater'])


# zomer Vrijafwater: bodh = peil - diepte
bodemhoogte_zomer_primair = primair_zomerpeil - waterdiepte_primair
bodemhoogte_zomer_primair.to_netcdf(path_output['primair_bodemhoogte_zomer_vrijafwater'])


# winter Vrijafwater: bodh = peil - diepte
bodemhoogte_winter_primair = primair_winterpeil - waterdiepte_primair
bodemhoogte_winter_primair.to_netcdf(path_output['primair_bodemhoogte_winter_vrijafwater'])


# -----
## Secundair
# -----
secundair_zomerpeil = xr.open_dataarray(path_input['secundair_peil_zomer_vrijafwater'])
secundair_winterpeil = xr.open_dataarray(path_input['secundair_peil_winter_vrijafwater'])

# zomer Vrijafwater: bodh = peil - diepte
bodemhoogte_zomer_secundair = secundair_zomerpeil - waterdiepte_secundair
bodemhoogte_zomer_secundair.to_netcdf(path_output['secundair_bodemhoogte_zomer_vrijafwater'])

# winter Vrijafwater: bodh = peil - diepte
bodemhoogte_winter_secundair = secundair_winterpeil - waterdiepte_secundair
bodemhoogte_winter_secundair.to_netcdf(path_output['secundair_bodemhoogte_winter_vrijafwater'])


# -----
## Tertiair
# -----
tertiair_zomerpeil = xr.open_dataarray(path_input['tertiair_peil_zomer_vrijafwater'])
tertiair_winterpeil = xr.open_dataarray(path_input['tertiair_peil_winter_vrijafwater'])

# zomer Vrijafwater: bodh = peil
tertiair_zomerpeil.to_netcdf(path_output['tertiair_bodemhoogte_zomer_vrijafwater'])

# winter Vrijafwater: bodh = peil
tertiair_winterpeil.to_netcdf(path_output['tertiair_bodemhoogte_winter_vrijafwater'])

