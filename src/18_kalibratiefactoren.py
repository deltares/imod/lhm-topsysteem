# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 13:38:44 2022

@author: meeusen

In dit script worden de kalibratiefactoren die zijn gebruikt om de conductance aan te passen, vermenigvuldigd met de conducatance kaarten

"""
import os
import subprocess
from subprocess import PIPE, Popen
import shutil

from os import path
import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# %% Functie
def run_batch(batchfile):
    """
    Function to run a .batch command. 
    Execute the external command and get its exitcode, stdout and stderr.
    (Agrees to iMODFLOW licensing prompt automatically.)
    """
    # based on https://stackoverflow.com/questions/1996518/retrieving-the-output-of-subprocess-call
    # executes "echo y|imodflow.exe runfile.run"
    p1 = Popen(["echo", "y"], stdout=PIPE, stderr=PIPE, shell=True)
    p2 = Popen(
        [batchfile],
        stdin=p1.stdout,
        stdout=PIPE,
        shell=True,
    )
    out, _ = p2.communicate()
    exitcode = p2.returncode
    if exitcode != 0:
      raise RuntimeError(
          f"Running {batchfile} has failed,"
          f"with output:\n{out.decode()}"
        )
    
#%%     
path_output = r'e:\Topsysteem\3-output\basisdata250'
loc_configuratie = 'e:\Topsysteem\kalibratie_batch'

os.chdir(params['loc_configuratie'])
with open(path_input['batch_file'],"r") as file:
    data = file.readlines()
    data[1] = f'SET DIR = {params["path_output"]}\n'
    data[8] = f'ECHO A = {path_input["COND_buisdrainage"]} >> %INI2% \n'
    data[9] = f'ECHO B = {path_input["DC_sys_1"]} >> %INI2% \n'
    data[14] = f'ECHO OUTPUTIDF = {params["COND_buisdrainage"]}.idf >> %INI2% \n'
    data[22] = f'ECHO A = {path_input["COND_greppels"]} >> %INI2% \n'
    data[23] = f'ECHO B = {path_input["DC_sys_2"]} >> %INI2% \n'
    data[28] = f'ECHO OUTPUTIDF = {params["COND_greppels"]}.idf >> %INI2% \n'
    data[36] = f'ECHO A = {path_input["COND_SOF"]} >> %INI2% \n'
    data[37] = f'ECHO B = {path_input["DC_sys_3"]} >> %INI2% \n'
    data[42] = f'ECHO OUTPUTIDF = {params["COND_SOF"]}.idf >> %INI2% \n'
    data[50] = f'ECHO A = {path_input["COND_primair"]} >> %INI2% \n'
    data[51] = f'ECHO B = {path_input["RC_zsys_3"]} >> %INI2% \n'
    data[56] = f'ECHO OUTPUTIDF = {params["COND_primair_zomer"]}.idf >> %INI2% \n'
    data[64] = f'ECHO A = {path_input["COND_secundair"]} >> %INI2% \n'
    data[65] = f'ECHO B = {path_input["RC_zsys_4"]} >> %INI2% \n'
    data[70] = f'ECHO OUTPUTIDF = {params["COND_secundair_zomer"]}.idf >> %INI2% \n'
    data[78] = f'ECHO A = {path_input["COND_tertiair"]} >> %INI2% \n'
    data[79] = f'ECHO B = {path_input["RC_zsys_5"]} >> %INI2% \n'
    data[84] = f'ECHO OUTPUTIDF = {params["COND_tertiair_zomer"]}.idf >> %INI2% \n'
    data[92] = f'ECHO A = {path_input["INF_mozart_primair"]} >> %INI2% \n'
    data[93] = f'ECHO B = {path_input["RI_sys_3"]} >> %INI2% \n'
    data[99] = f'ECHO OUTPUTIDF = {params["INF_mozart_primair"]}.idf >> %INI2% \n'
    data[106] = f'ECHO A = {path_input["INF_modflow_primair"]} >> %INI2% \n'
    data[107] = f'ECHO B = {path_input["RI_sys_3"]} >> %INI2% \n'
    data[112] = f'ECHO OUTPUTIDF = {params["INF_modflow_primair"]}.idf >> %INI2% \n'
    data[120] = f'ECHO A = {path_input["INF_mozart_secundair"]} >> %INI2% \n'
    data[121] = f'ECHO B = {path_input["RI_sys_4"]} >> %INI2% \n'
    data[126] = f'ECHO OUTPUTIDF = {params["INF_mozart_secundair"]}.idf >> %INI2% \n'
    data[134] = f'ECHO A = {path_input["INF_modflow_secundair"]} >> %INI2% \n'
    data[135] = f'ECHO B = {path_input["RI_sys_4"]} >> %INI2% \n'
    data[140] = f'ECHO OUTPUTIDF = {params["INF_modflow_secundair"]}.idf >> %INI2% \n'
    data[148] = f'ECHO A = {path_input["INF_mozart_tertiair"]} >> %INI2% \n'
    data[149] = f'ECHO B = {path_input["RI_sys_5"]} >> %INI2% \n'
    data[154] = f'ECHO OUTPUTIDF = {params["INF_mozart_tertiair"]}.idf >> %INI2% \n'
    data[162] = f'ECHO A = {path_input["INF_modflow_tertiair"]} >> %INI2% \n'
    data[163] = f'ECHO B = {path_input["RI_sys_5"]} >> %INI2% \n'
    data[168] = f'ECHO OUTPUTIDF = {params["INF_modflow_tertiair"]}.idf >> %INI2% \n'
    data[176] = f'ECHO A = {path_input["COND_primair"]} >> %INI2% \n'
    data[177] = f'ECHO B = {path_input["RC_wsys_3"]} >> %INI2% \n'
    data[182] = f'ECHO OUTPUTIDF = {params["COND_primair_winter"]}.idf >> %INI2% \n'
    data[190] = f'ECHO A = {path_input["COND_secundair"]} >> %INI2% \n'
    data[191] = f'ECHO B = {path_input["RC_wsys_4"]} >> %INI2% \n'
    data[196] = f'ECHO OUTPUTIDF = {params["COND_secundair_winter"]}.idf >> %INI2% \n'
    data[204] = f'ECHO A = {path_input["COND_tertiair"]} >> %INI2% \n'
    data[205] = f'ECHO B = {path_input["RC_wsys_5"]} >> %INI2% \n'
    data[210] = f'ECHO OUTPUTIDF = {params["COND_tertiair_winter"]}.idf >> %INI2% \n'
    writing_file = open('kalibratiefactor_run.bat',"w")
    writing_file.writelines(data)
    writing_file.close()
#    run_batch(r'kalibratiefactor_run.bat')
    subprocess.call('kalibratiefactor_run.bat')  
 