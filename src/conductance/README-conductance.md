Conductance
===========

Doel van het script
-------------------

In dit script wordt de conductance van de primaire, secundaire en
tertiaire waterlopen bepaald.

Toelichting
-----------

Het water in het primair, secundair en tertiair riviersysteem kan naar
het grondwater stromen en andersom. De mate waarin dit gebeurt hangt
onder andere af van de conductance. De conductance is afhankelijk van,
onder andere, de samenstelling van de ondergrond en de diepte van het
grondwater. In een zestal scripts die achtereenvolgens worden
aangeroepen, wordt de conductance voor de riviersysteemen bepaald.

Locatie van de scripts
----------------------

De berekening van de conductance wordt uitgevoerd in zes verschillende
scripts. Deze scripts zijn vrij geheugenintensief, dus dienen gedraaid
te worden op een pc met voldoende geheugen. De scripts zijn:

-   src/conductance/1-geotop_to_c1.py (zie bijlage T)

-   src/conductance/2-brabant_geotop_to_c1.py (zie bijlage U)

-   src/conductance/3-hydrotype_to_c1.py (zie bijlage V)

-   src/conductance/4-merge_kd_c.py (zie bijlage W)

-   src/conductance/5-hydrotype_k.py (zie bijlage X)

-   src/conductance/6-leakage_c.py (zie bijlage Y)

Invoer
------

+-------------------+--------------+-----------+-------------------+
| Bestand of        | Eenheid      | Resolutie | Bron of locatie   |
| dataset           |              |           |                   |
+===================+==============+===========+===================+
| REGIS v2          | \-           | 100 meter | REGIS_v2_2.nc     |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Doorlatendheden   | Meter/dag    | Tabel     | k_waarden_GeoTOP  |
| GeoTOP ZW         |              |           | _ZW_Nederland.csv |
| Nederland         |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Lithostratigrafie | \-           | Tabel     | lith              |
|                   |              |           | ostratigrafie.csv |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| GeoTOP + GeoTOP   | \-           | 100 meter | geotop.zarr       |
| Brabant           |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
|                   |              |           |                   |
|                   |              |           | ge                |
|                   |              |           | otop_brabant.zarr |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Gebied Brabant &  | \-           | 100 meter | gebied_br         |
| Limburg           |              |           | abant_limburg.asc |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Niba_tcc          | Meter        | 100 meter | geotop            |
|                   |              |           | _doorlatendheid/n |
| *Basisveen        |              |           | iba_lagenbestand/ |
| bovenkant*        |              |           |                   |
|                   |              |           | niba_tcc.img      |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| niba_bcc          | Meter        | 100 meter | geotop            |
|                   |              |           | _doorlatendheid/n |
| *Basisveen        |              |           | iba_lagenbestand/ |
| onderkant*        |              |           |                   |
|                   |              |           | niba_bcc.img      |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Hydrotype         | \-           | Vector    | hydrotype.shp     |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Doorlatendheden   | Meter/dag    | Tabel     | k_waard           |
| hydrotypes        |              |           | en_hydrotypes.csv |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| AHN_F250          |              | 250 meter | AHN_F250.IDF      |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Singraven         | \-           | Vector    | singraven.shp     |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Verzadigde dikte  | Meter        | 250 meter | D.asc (Hunink et  |
|                   |              |           | al., 2020)        |
+-------------------+--------------+-----------+-------------------+
| Intreeweerstand   | Meter        | 250 meter | C0.asc (Hunink et |
|                   |              |           | al., 2020)        |
+-------------------+--------------+-----------+-------------------+
| Weerstand van de  | Meter        | 250 meter | nieuw_lagenmode   |
| eerste modellaag  |              |           | l/c/mdl_vc_l1.idf |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Bovenkant         | Meter        | 250 meter | nieuw_lagenm      |
| modellagen        |              |           | odel/c/top/\*.idf |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Onderkant         | Meter        | 250 meter | nieuw_lagenm      |
| modellagen        |              |           | odel/c/bot/\*.idf |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Kd modellagen     | Meter^2^/dag | 250 meter | nieuw_lagen       |
|                   |              |           | model/c/kd/\*.idf |
|                   |              |           |                   |
|                   |              |           | (Hunink et al.,   |
|                   |              |           | 2020)             |
+-------------------+--------------+-----------+-------------------+
| Areaal            | Meter^2^     | 250 meter | Uitvoer van       |
| primair/s         |              |           | 'herschalen.py'   |
| ecundair/tertiair |              |           |                   |
+-------------------+--------------+-----------+-------------------+
| Lengte            | Meter        | 250 meter | Uitvoer van       |
| primair/s         |              |           | 'herschalen.py'   |
| ecundair/tertiair |              |           |                   |
+-------------------+--------------+-----------+-------------------+
| Peil              | Meter        | 250 meter | Uitvoer van       |
| primair/s         |              |           | 'herschalen.py'   |
| ecundair/tertiair |              |           |                   |
+-------------------+--------------+-----------+-------------------+

Uitvoer
-------

+-------------------+--------------+-----------+-------------------+
| Bestand of        | Eenheid      | Resolutie | Locatie           |
| dataset           |              |           |                   |
+===================+==============+===========+===================+
| HLc_kh (HLc =     | Meter/dag    | 100 meter | HLc-kh.idf        |
| Holoceen)         |              |           |                   |
+-------------------+--------------+-----------+-------------------+
| HLc_kv            | Meter/dag    | 100 meter | HLc-kv.idf        |
+-------------------+--------------+-----------+-------------------+
| HLc_dikte         | Meter        | 100 meter | HLc-dikte.idf     |
+-------------------+--------------+-----------+-------------------+
| HLc_top           | Meter        | 100 meter | HLc-top.idf       |
+-------------------+--------------+-----------+-------------------+
| HLc_bot           | Meter        | 100 meter | HLc-bot.idf       |
+-------------------+--------------+-----------+-------------------+
| HLc_kd            | Meter^2^/dag | 100 meter | HLc-kd.idf        |
+-------------------+--------------+-----------+-------------------+
| HLc_c             | Dagen        | 100 meter | HLc-c.idf         |
+-------------------+--------------+-----------+-------------------+
| Conductance       | Meter^2^/dag | 250 meter | cond_primair.idf  |
| primair/s         |              |           |                   |
| ecundair/tertiair |              |           | c                 |
|                   |              |           | ond_secundair.idf |
|                   |              |           |                   |
|                   |              |           | cond_tertiair.idf |
+-------------------+--------------+-----------+-------------------+
| C_wet             | Dagen        | 250 meter | c_primair.idf     |
| primair/s         |              |           |                   |
| ecundair/tertiair |              |           | c_secundair.idf   |
|                   |              |           |                   |
|                   |              |           | c_tertiair.idf    |
+-------------------+--------------+-----------+-------------------+

Beschrijving
------------

Dit deel van de topsysteem programmatuur is al eerder ontwikkeld voor
LHM 4.0 en uitvoerig beschreven in de veranderingsrapportage LHM 4.0
door Hunink et al., 2020.

Kort samengevat worden de volgende stappen genomen in de scripts:

1.  Per voxel van GeoTOP v1.3 is de samenstelling met de grootste kans
    van voorkomen geselecteerd;

2.  Uit GeoTOP wordt het deel dat zich boven de onderkant van REGIS-HLc
    (de holocene laag in REGIS II V2.2) bevindt uitgesneden;

3.  Aan alle voxels van GeoTOP v1.3 wordt een kh en kv toegekend, op
    basis van een basistabel (k_waarden_GeoTOP_ZW_Nederland.csv) en
    aanvullende info voor enkele geologische informaties;

4.  De weerstand en transmissiviteit van het basisveen worden berekend
    aan de hand van de exacte laagdikte;

5.  Voor GeoTOP Brabant is het deel dat zich boven de onderkant van
    REGIS-HLc bevindt uitgesneden;

6.  De weerstanden en transmissiviteiten voor de holocene sedimenten van
    GeoTOP Brabant zijn gesommeerd;

7.  De transmisssiviteiten en weerstanden zijn toegekend aan de Formatie
    van Singraven (de beekdalen volgens de hydrotypenkaart;

8.  De transmissiviteiten en weerstanden van basisveen, GeoTOP Brabant,
    Hydrotypes en Singraven zijn gecombineerd met GeoTOP v1.3;

9.  Er zijn verticale doorlatendheden afgeleid door de
    transmissiviteiten en weerstanden door de dikte te delen;

10. De horizontale doorlatendheid is vervolgens samengesteld uit de
    horizontale doorlatendheid van de hydrotype voor de eerste twee
    meter van de doorstroomde dikte, en de horizontale doorlatendheid
    van de daar aanwezige laag uit het nieuwe lagenmodel.

Vervolgens wordt de conductance berekend aan de hand van de berekende
doorlatendheden. De freatische lekweerstanden worden berekend met
behulp van de formule van de Lange (zie W. de Lange et al., 2008),
op basis van de implementatie in imod-python.
