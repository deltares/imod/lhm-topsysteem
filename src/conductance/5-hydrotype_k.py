"""
1. Create a full grid of hydrotypes kh and kv
2. Dit is anders dan de parameterisatie van kD en c voor het Holoceen
3. Want de c* stroming treedt op in het bovenste freatische stuk, waarvoor (aangenomen) hydrotypes bepalend zijn
4. Dit script genereert een volledige kaart, alleen gebaseerd op hydrotypes
"""

import geopandas as gpd
import imod
import numba
import numpy as np
import pandas as pd
import xarray as xr

# Importeer snakemake invoer en uitvoer:
params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# Saturated thickness
D = imod.rasterio.open(path_input["D"])

# Hydrotype
# ---------
@numba.njit
def fill_hydrotype(hydrotype, lookup, out):
    hydrotype = hydrotype.ravel()
    out = out.ravel()
    size = hydrotype.size
    for i in range(size):
        h = hydrotype[i]
        if np.isnan(h):
            continue
        out[i] = lookup[int(h)]


hydrotype_vector = gpd.read_file(path_input["hydrotype"])
hydrotype_vector["naam_hydro"] = hydrotype_vector["naam_hydro"].str.lower()
hydrotype_vector = hydrotype_vector[hydrotype_vector["naam_hydro"] != "water"]
hydrotype = imod.prepare.rasterize(hydrotype_vector, column="hydrotype2", like=D)

tabel_hydrotype = pd.read_csv(path_input["k_waarden_hydrotypes"])
replace_dict = {
    k: v
    for k, v in zip(
        hydrotype_vector["naam_hydro"].values, hydrotype_vector["hydrotype2"].values
    )
}
tabel_hydrotype["nummercode"] = (
    tabel_hydrotype["Hydrotype"].str.lower().replace(replace_dict)
)

unique_hydrotype = np.unique(hydrotype)
unique_hydrotype = unique_hydrotype[~np.isnan(unique_hydrotype)]
missing_hydrotype = unique_hydrotype[
    ~np.isin(unique_hydrotype, tabel_hydrotype["nummercode"].values)
]
assert missing_hydrotype.size == 0

hydro_kh_lookup = np.full(int(tabel_hydrotype["nummercode"].max()) + 1, np.nan)
hydro_kv_lookup = np.full(int(tabel_hydrotype["nummercode"].max()) + 1, np.nan)
for h, hkh, hkv in zip(
    tabel_hydrotype["nummercode"].values,
    tabel_hydrotype["kh"].values,
    tabel_hydrotype["kv"].values,
):
    h_int = int(h)
    hydro_kh_lookup[h_int] = hkh
    hydro_kv_lookup[h_int] = hkv

kh_hydrotype = xr.full_like(hydrotype, np.nan)
kv_hydrotype = xr.full_like(hydrotype, np.nan)
fill_hydrotype(hydrotype.values, hydro_kh_lookup, kh_hydrotype.values)
fill_hydrotype(hydrotype.values, hydro_kv_lookup, kv_hydrotype.values)

imod.idf.save(path_output["hydrotype_kh"], kh_hydrotype)
imod.idf.save(path_output["hydrotype_kv"], kv_hydrotype)


# Create map of depth
D = imod.rasterio.open(path_input["D"]).compute()
out = xr.full_like(D, np.nan)
uniques = np.unique(hydrotype)
uniques = uniques[~np.isnan(uniques)]


for i in uniques:
    print(i)
    is_value = hydrotype == i
    value = np.nanpercentile(D.where(is_value), 95)
    t = xr.full_like(D, value).where(is_value)
    out = out.combine_first(t)

imod.rasterio.write(path_output["test_D"], out)
