"""
Berekening van Wim de Lange's freatische lekweerstand (c*)
"""
import imod
import numpy as np
import xarray as xr

# Importeer snakemake invoer en uitvoer:
params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

for systemname in ["primair", "secundair", "tertiair"]:
    # for systemname in ["primary"]:
    # Primair
    # kh = imod.rasterio.open("data/NHI_server/kh_2.asc")
    # kv = imod.rasterio.open("data/NHI_server/kv_2.asc")
    D = imod.rasterio.open(path_input["D"])
    c0 = imod.rasterio.open(path_input["C0"])
    c1 = imod.idf.open(path_input["mdl_vc_l1"])
    dx = 250.0
    dy = 250.0

    # Hydrotype data
    hydrotype_kh = imod.idf.open(path_input["hydrotype_kh"])
    kv = imod.idf.open(path_input["hydrotype_kv"])

    # Layermodel
    ahn = imod.idf.open(path_input["AHN_F250"])
    bot = imod.idf.open(path_input["bot"] + "/*.idf")
    top = imod.idf.open(path_input["top"] + "/*.idf")
    kd = imod.idf.open(path_input["kd"] + "/*.idf")
    aquifer_thickness = top - bot
    kh = kd / aquifer_thickness
    kh = kh.where(kh < 50.0, other=50.0)
    kh1 = kh.isel(layer=0)
    kh2 = kh.isel(layer=1)
    imod.idf.write(path_output['kh1'], kh1)
    imod.idf.write(path_output['kh2'], kh2)

    lengte = xr.open_dataarray(path_input[f"lengte_{systemname}_herschaald"]).compute()
    breedte = (xr.open_dataarray(path_input[f"area_{systemname}_herschaald"]) / lengte).compute()
    peil_zomer = xr.open_dataarray(path_input[f"peil_{systemname}_zomer_herschaald"]).compute()
    peil_winter = xr.open_dataarray(path_input[f"peil_{systemname}_winter_herschaald"]).compute()
    peil = (peil_winter + peil_zomer) / 2

    threshold_top = peil - 2.0
    threshold_bot = peil - D

    active_layers = (top > threshold_bot) & (bot < threshold_top)
    out_thickness = aquifer_thickness.where(active_layers)

    tmp_top = top.where(top < threshold_top, threshold_top).where(active_layers)
    tmp_bot = bot.where(bot > threshold_bot, threshold_bot).where(active_layers)
    tmp_thickness = tmp_top - tmp_bot

    total_thickness = tmp_thickness.sum("layer")
    total_thickness = total_thickness.where(total_thickness > 0.0)
    # imod.idf.save("thickness", out_thickness)
    # imod.idf.save("peil_p", peil)
    # imod.idf.save("tmpthickness", tmp_thickness)
    # imod.idf.write("total_thickness.idf", total_thickness)

    extra_kd = (kh * tmp_thickness).sum("layer")
    extra_kd = extra_kd.where(D > 2.0).fillna(0.0)
    kd = hydrotype_kh * 2.0 + extra_kd
    imod.idf.write(path_output['kd'], kd)

    # correct c1 for additional of D/kv in function
    # it will be re-added in the function
    c1 = c1 - (D / kv)

    # Get rid of tiny values, nodata
    c1 = c1.where(c1 > 0.1, 0.1)
    kv = kv.where(kv > 0.0001, 0.0001)
    D = D.where(D > 0.1, 0.1)
    kh = kh.where(kh > 0.1, 0.1)
    c0 = c0.where(c0 > 0.5, 0.5)

    c_leakage = imod.prepare.surface_water.c_leakage(
        kh=kd / D,
        kv=kv,
        D=D,
        c0=c0,
        c1=c1,
        B=breedte,
        length=lengte,
        dx=dx,
        dy=dy,
    ).compute()

    c_leakage = c_leakage.fillna(c0).where(peil.notnull())
    cond_leakage = (250.0 * 250.0) / c_leakage
    imod.idf.write(path_output[f"cond_leakage_{systemname}"], cond_leakage.squeeze(drop=True))

    c_wet = cond_leakage / (breedte * lengte)
    imod.idf.write(path_output[f"c_wet_{systemname}"], c_wet.squeeze(drop=True))
