"""
In dit script wordt een landsdekkend peil voor de zomer- en
wintersituatie aangemaakt, waarbij er voor elke rastercel een peil
bepaald wordt, ook voor cellen waar geen waterloop doorheen loopt. Dit
peil is in een van de volgende scripts nodig voor het aanmaken van de
buisdrainage.

Voor elke cel wordt een peil uit een van de onderstaande grids gebruikt.
Bij voorkeur wordt het peil van het primair riviersysteem gebruikt,
indien dat niet aanwezig is in een cel, wordt naar het secundaire
riviersysteem gekeken, et cetera:

-   Primair peil

-   Secundair peil

-   Tertiair peil

-   Regiopeil

-   AHN_peil

-   AHN
"""
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# open grids
grids = {}
for name in path_input:
    path = path_input[name]
    if path.endswith('.py'):
        continue
    grids[name] = xr.open_dataarray(path_input[name])

# clac
peil_ps_zomer = (
    grids["peil_primair_zomer"]
    .fillna(grids["peil_secundair_zomer"])
)

peil_ps_winter = (
    grids["peil_primair_winter"]
    .fillna(grids["peil_secundair_winter"])
)

# save
peil_ps_zomer.to_netcdf(path_output["peil_ps_zomer"])
peil_ps_winter.to_netcdf(path_output["peil_ps_winter"])
