"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het peilgestuurde gebied te bepalen.
Voor ieder polygoon zijn in het vorige script alle aparte gegevens verzameld, aan de hand van deze parameters 
wordt het zomerpeil en winterpeil bepaald.
Zie de documentatie voor de rekenregels
"""

import pandas as pd 
import numpy as np

# Let op: LSW TYPEint: 1 = P, 2 = V / O 
# Of willen we liever TYPEint 1= p/o, 2 = v?
params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# -----------------------------------
### PRIMAIR
# -----------------------------------
pad_peil_params_primair = path_input["primair_zonal_agg_peil_peilgestuurd"]
peil_params_primair = pd.read_csv(pad_peil_params_primair, index_col='lokaalid')
vrijafwater = peil_params_primair['LSWtype']==2.0

### Zomerpeil
peil_params_primair['peil_zomer'] = np.nan

peil_params_primair['peil_zomer'] = peil_params_primair['zomerpeil_regiopeil'].where(
    ~vrijafwater & peil_params_primair['zomerpeil_regiopeil'].notnull(),
    peil_params_primair['zomerpeil_peilvak']
        .where(
            ~vrijafwater & peil_params_primair['zomerpeil_peilvak'].notnull(),
            peil_params_primair[['ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
    )
)


### Winterpeil
peil_params_primair['peil_winter'] = np.nan


peil_params_primair['peil_winter'] = peil_params_primair['winterpeil_regiopeil'].where(
    ~vrijafwater & peil_params_primair['winterpeil_regiopeil'].notnull(),
    peil_params_primair['winterpeil_peilvak']
        .where(
            ~vrijafwater & peil_params_primair['winterpeil_peilvak'].notnull(),
            peil_params_primair[['ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
    )
)

peil_params_primair.to_csv(path_output['peil_bodemhoogte_primair'])


# -----------------------------------
### SECUNDAIR
# -----------------------------------
pad_peil_params_secundair = path_input["secundair_zonal_agg_peil_peilgestuurd"]
peil_params_secundair = pd.read_csv(pad_peil_params_secundair, index_col='lokaalid')

vrijafwater = peil_params_secundair['LSWtype']==2.0
### Zomerpeil
peil_params_secundair['peil_zomer'] = np.nan

peil_params_secundair['peil_zomer'] = peil_params_secundair['zomerpeil_regiopeil'].where(
    ~vrijafwater & peil_params_secundair['zomerpeil_regiopeil'].notnull(),
    peil_params_secundair[['zomerpeil_peilvak', 'ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
)




### Winterpeil
peil_params_secundair['peil_winter'] = np.nan

peil_params_secundair['peil_winter'] = peil_params_secundair['winterpeil_regiopeil'].where(
    ~vrijafwater & peil_params_secundair['winterpeil_regiopeil'].notnull(),
    peil_params_secundair[['winterpeil_peilvak', 'ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
)


peil_params_secundair.to_csv(path_output['peil_bodemhoogte_secundair'])

# -----------------------------------
### tertiair
# -----------------------------------
pad_peil_params_tertiair = path_input["tertiair_zonal_agg_peil_peilgestuurd"]
peil_params_tertiair = pd.read_csv(pad_peil_params_tertiair, index_col='lokaalid')

vrijafwater = peil_params_tertiair['LSWtype']==2.0

### Zomerpeil
peil_params_tertiair['peil_zomer'] = np.nan

peil_params_tertiair['peil_zomer'] = peil_params_tertiair['zomerpeil_regiopeil'].where(
    ~vrijafwater & peil_params_tertiair['zomerpeil_regiopeil'].notnull(),
    peil_params_tertiair[['zomerpeil_peilvak', 'ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
)


### Winterpeil
peil_params_tertiair['peil_winter'] = np.nan

peil_params_tertiair['peil_winter'] = peil_params_tertiair['winterpeil_regiopeil'].where(
    ~vrijafwater & peil_params_tertiair['winterpeil_regiopeil'].notnull(),
    peil_params_tertiair[['winterpeil_peilvak', 'ahn_drooglegging', 'ahnpeil']].max(axis=1).where(~vrijafwater)
)


peil_params_tertiair.to_csv(path_output['peil_bodemhoogte_tertiair'])

