"""
This script finds the hydrotype & LSWtype corresponding to the waterloop.
The hydrotype & LSWtype is assigned based on the location of the centroid of the waterloop. 
It uses the centroids of the waterlopen as input.
"""
import imod
import pandas as pd
import xarray as xr
import numpy as np
import os

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

dx, xmin, xmax, dy, ymin, ymax = (25.0, 0.0, 300000.0, -25.0, 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

hydrotype_raster = imod.prepare.gdal_rasterize(path_input['hydrotypen'], column="hydrotype2", like=like_25m)
LSW_raster = imod.prepare.gdal_rasterize(path_input['LSW_type'], column='TYPEint', like=like_25m)

## Lijnen
centroid_table = pd.read_csv(path_input['centroidGebiedcsv'])
iy = ((centroid_table["Y"] - ymax) / dy).astype(int)
ix = ((centroid_table["X"] - xmin) / dx).astype(int)
centroid_table["hydrotype2"] = hydrotype_raster.values[iy, ix]
centroid_table['LSWtype'] = LSW_raster.values[iy, ix]
centroid_table.to_csv(path_output['hydrotypesGebied'])

## Vlakken
centroid_table = pd.read_csv(path_input['centroidGebied_vlakkencsv'])
iy = ((centroid_table["Y"] - ymax) / dy).astype(int)
ix = ((centroid_table["X"] - xmin) / dx).astype(int)
centroid_table["hydrotype2"] = hydrotype_raster.values[iy, ix]
centroid_table['LSWtype'] = LSW_raster.values[iy, ix]
centroid_table.to_csv(path_output['hydrotypesGebied_vlakken'])
