"""
This script joins the hydrotypes & LSWtype to the geopackages of the sloten.
"""

import subprocess # Package to run command line
import os 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# Add hydrotypes csv to gpkg (explained in last comment in this thread https://gis.stackexchange.com/questions/95746/join-csv-file-to-shapefile-using-gdal-ogr)
print('Join hydrotypes')
process = subprocess.Popen(
    args=( 
        'sqlite3 '
        f'{path_input["slootGebied"]} '
        f'--cmd ".mode csv" ".import {path_input["hydrotypesGebied"]} hydrotypes"'
    ),
)
stdout, stderr = process.communicate()

process = subprocess.Popen(
    args=( 
        'ogr2ogr -f GPKG '
        f'{path_output["slootGebied_hydrotype"]} -nln slootGebied_hydrotype ' # Uitvoer
        f'{path_input["slootGebied"]} ' # Invoer
        '-sql "SELECT shp.*, csv.* FROM slootGebied shp JOIN hydrotypes csv ON shp.lokaalid = csv.lokaalid"'
    ),
)
stdout, stderr = process.communicate()

## Vlakken
# Add hydrotypes csv to gpkg (explained in last comment in this thread https://gis.stackexchange.com/questions/95746/join-csv-file-to-shapefile-using-gdal-ogr)
print('Join hydrotypes')
process = subprocess.Popen(
    args=( 
        'sqlite3 '
        f'{path_input["slootGebied_vlakken"]} '
        f'--cmd ".mode csv" ".import {path_input["hydrotypesGebied_vlakken"]} hydrotypes"'
    ),
)
stdout, stderr = process.communicate()

process = subprocess.Popen(
    args=( 
        'ogr2ogr -f GPKG '
        f'{path_output["slootGebied_vlakken_hydrotype"]} -nln slootGebied_vlakken_hydrotype ' # Uitvoer
        f'{path_input["slootGebied_vlakken"]} ' # Invoer
        '-sql "SELECT shp.*, csv.* FROM slootGebied_vlakken shp JOIN hydrotypes csv ON shp.lokaalid = csv.lokaalid"'
    ),
)
stdout, stderr = process.communicate()

