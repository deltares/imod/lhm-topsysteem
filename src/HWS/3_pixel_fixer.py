"""
Script overgenomen (en aangepast naar Snakefile etc.) van: https://gitlab.com/deltares/imod/LHM-automation/-/tree/master/1_model_setup/make-HWS-peilen 
"""

import imod
import xarray as xr

"""
Sommige pixel in de input hebben een te hoog peil. En dus ook in de output.
Het gaat om 32 pixel.
Dit script kent aan deze pixels een waarde toe die (meer) correct is. Deze waarde wordt gekopieerd  van een naburige pixel
"""

idfs = imod.idf.open(f"data/2-interim/peil_HWS_*.idf").load()

pixels = [
    # [wrongX, wrongY correctX, correctY]
    # fortran/imod coords (base 1)
    [735, 1147, 736, 1146],
    [736, 1147, 736, 1146],
    [740, 1133, 740, 1132],
    [748, 1115, 747, 1115],
    [750, 1112, 753, 1101],
    [749, 1111, 753, 1101],
    [750, 1111, 753, 1101],
    [750, 1110, 753, 1101],
    [750, 1109, 753, 1101],
    [751, 1109, 753, 1101],
    [752, 1109, 753, 1101],
    [750, 1108, 753, 1101],
    [751, 1108, 753, 1101],
    [752, 1108, 753, 1101],
    [751, 1107, 753, 1101],
    [752, 1107, 753, 1101],
    [753, 1107, 753, 1101],
    [751, 1106, 753, 1101],
    [752, 1106, 753, 1101],
    [753, 1106, 753, 1101],
    [753, 1105, 753, 1101],
    [754, 1105, 753, 1101],
    [753, 1104, 753, 1101],
    [754, 1104, 753, 1101],
    [754, 1103, 753, 1101],
    [755, 1103, 753, 1101],
    [753, 1102, 753, 1101],
    [755, 1102, 753, 1101],
    [755, 1101, 753, 1101],
    [756, 1101, 753, 1101],
    [754, 1100, 753, 1101],
    [755, 1100, 753, 1101],
    [756, 1100, 753, 1101],
    [756, 1099, 753, 1101],
    [757, 1099, 753, 1101],
    [757, 1098, 753, 1101],
    [758, 1099, 759, 1098],
    [759, 1099, 759, 1098],
    [758, 1098, 759, 1098],
]

for pixel in pixels:
    # -1 because imod/fortran is base 1. Python is base 0
    idfs[{"x": pixel[0] - 1, "y": pixel[1] - 1}] = idfs[
        {"x": pixel[2] - 1, "y": pixel[3] - 1}
    ]

# imod.idf.save(path_output["output_name"], idfs, pattern="{name}_{time:%Y%m%d}{extension}")
imod.idf.save("data/3-output/PEILH", idfs, pattern="{name}_{time:%Y%m%d}{extension}")
