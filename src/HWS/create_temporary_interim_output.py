import geopandas as gpd
import imod
import numpy as np
import xarray as xr
import itertools

# peil
soort = ['primair', 'secundair', 'tertiair']
seizoen = ['winter', 'zomer', 'jaar']
alles = list(itertools.product(soort, seizoen))

for grid in alles:
    soort = grid[0]
    seizoen = grid[1]
    peil = imod.rasterio.open(fr'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\output_folder_v67\pyresults\peil\peil_{soort[0]}{seizoen[0]}_25mi')
    peil.to_netcdf(f'c:/git/LHM-topsysteem/data/2-interim/{soort}_peil_{seizoen}_m.nc')


# areaal & lengte
soorten = ['primair', 'secundair', 'tertiair']

for soort in soorten:
    print(soort[0])
    areaal = imod.rasterio.open(fr'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\output_folder_v67\pyresults\aanw\opp_{soort[0]}_25m')
    lengte = imod.rasterio.open(fr'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\output_folder_v67\pyresults\brensl\lengte_{soort[0]}_25m')
    areaal.to_netcdf(f'c:/git/LHM-topsysteem/data/2-interim/{soort}_areaal_m2.nc')
    lengte.to_netcdf(f'c:/git/LHM-topsysteem/data/2-interim/{soort}_lengte_m.nc')

# Ook HWS areaal voor bepaling wateropp
areaal_HWS = imod.rasterio.open(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\output_folder_v67\pyresults\aanw\opp_h_25m')
#areaal.to_netcdf(f'c:/git/LHM-topsysteem/data/2-interim/HWS_areaal_m2.nc')
areaal_primair = xr.open_dataarray(r'c:/git/LHM-topsysteem/data/2-interim/primair_areaal_m2.nc')