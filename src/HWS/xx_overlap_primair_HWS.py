import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd
import os

path = r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\3-output\peilgestuurd_vrijafwater\full_area'
os.chdir(path)

bodemhoogte_LHM = imod.idf.open(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\1-input\both_z_l1.idf').squeeze(drop=True).compute()

dm_netwerk = gpd.read_file(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\1-input\dmnetwerk_v17.shp')
dm_netwerk_noboezem = dm_netwerk[dm_netwerk['WATER_TYPE']!='boezem'] # Geen HWS in boezems

dx, xmin, xmax, dy, ymin, ymax = (25.0, 0.0, 300000.0, -25.0, 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

dm_netwerk_noboezem_raster = imod.prepare.rasterize(dm_netwerk_noboezem, like=like_25m, all_touched=True)
dm_netwerk_raster = imod.prepare.rasterize(dm_netwerk, like=like_25m, all_touched=True)
dm_netwerk_raster.to_netcdf(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\2-interim\dm_netwerk_raster_alltouched.nc')

bodemhoogte_LHM_fijn = imod.prepare.Regridder(method='mean').regrid(bodemhoogte_LHM, like=like_25m)

# # Brand de locaties van HWS (dus dm_netwerk incl de boezems) uit alle primaire scripts:
# for filename in list(os.listdir(path)):
#     if filename.startswith('primair'):
#         raster = xr.open_dataarray(f'{path}/{filename}')
#         raster_noHWS = xr.where(dm_netwerk_raster.isnull(), raster, np.nan)
#         raster_noHWS.to_netcdf(f'{filename[:-14]}_noHWS.nc')

# Neem LHM HWS peil over op locaties van HWS die geen boezem zijn
# Neem primair peil over op locaties van HWS die wel boezem zijn
HWS_zomerpeil_LHM = xr.open_dataarray(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\2-interim\peil_HWS_zomer_LHM.nc')
zomerpeil_primair = xr.open_dataarray('primair_peil_zomer_HWSoverlap.nc')
HWS_zomerpeil = xr.where(dm_netwerk_raster.notnull(), zomerpeil_primair, np.nan)
HWS_zomerpeil = xr.where(dm_netwerk_noboezem_raster.notnull(), HWS_zomerpeil_LHM, HWS_zomerpeil)
HWS_zomerpeil.to_netcdf(r'peil_HWS_zomer.nc')

bodemhoogte_primair_zomer = xr.open_dataarray(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\3-output\peilgestuurd_vrijafwater\full_area\primair_bodemhoogte_zomer_HWSoverlap.nc')
HWS_zomerhoogte = xr.where(dm_netwerk_raster.notnull(), bodemhoogte_primair_zomer, np.nan)
HWS_zomerhoogte = xr.where(dm_netwerk_noboezem_raster.notnull(), bodemhoogte_LHM_fijn, HWS_zomerhoogte)
HWS_zomerhoogte.to_netcdf(r'hoogte_HWS_zomer.nc')


HWS_winterpeil_LHM = xr.open_dataarray(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\2-interim\peil_HWS_winter_LHM.nc')
winterpeil_primair = xr.open_dataarray('primair_peil_winter_HWSoverlap.nc')
HWS_winterpeil = xr.where(dm_netwerk_raster.notnull(), winterpeil_primair, np.nan)
HWS_winterpeil = xr.where(dm_netwerk_noboezem_raster.notnull(), HWS_winterpeil_LHM, HWS_winterpeil)
HWS_winterpeil.to_netcdf(r'peil_HWS_winter.nc')

bodemhoogte_primair_winter = xr.open_dataarray(r'c:\Users\schoonde\OneDrive - Stichting Deltares\Documents\_LHM\Topsystemen\_nieuweRonde\data\3-output\peilgestuurd_vrijafwater\full_area\primair_bodemhoogte_winter_HWSoverlap.nc')
HWS_winterhoogte = xr.where(dm_netwerk_raster.notnull(), bodemhoogte_primair_winter, np.nan)
HWS_winterhoogte = xr.where(dm_netwerk_noboezem_raster.notnull(), bodemhoogte_LHM_fijn, HWS_winterhoogte)
HWS_winterhoogte.to_netcdf(r'hoogte_HWS_winter.nc')

# Bepaal weerstand
HWS_weerstand = imod.prepare.rasterize(dm_netwerk, column='WEERSTAND', like=like_25m, all_touched=True)
HWS_weerstand = xr.where(HWS_weerstand == 0., 10, HWS_weerstand)
HWS_weerstand.to_netcdf('weerstand_HWS.nc')

celoppervlak = 25.*25.

primair_area = xr.open_dataarray('primair_area_raster_HWSoverlap.nc')
HWS_area = xr.where((dm_netwerk_raster.notnull() & primair_area.notnull()), primair_area, np.nan)
HWS_area = xr.where((dm_netwerk_raster.notnull() & primair_area.isnull()), celoppervlak, HWS_area) # Alle gebieden waar wel HWS, maar geen primair opp: vul aan met celoppervlak
HWS_area.to_netcdf('area_HWS.nc')

HWS_conductance = HWS_area / HWS_weerstand 
HWS_conductance.to_netcdf('conductance_HWS.nc')