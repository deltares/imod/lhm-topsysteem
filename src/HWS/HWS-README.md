Hoofdwatersysteem
=================

Doel van het script
-------------------

Peilen van het hoofdwatersysteem genereren.

Toelichting
-----------

Voor het hoofdwatersysteem worden maandelijks gemiddelde peilen opgelegd
die eerder zijn berekend met het Landelijk Sobek Model (LSM). De peilen
worden via regressie bepaald uit een langere Sobek-reeks, zodat ook
peilen worden toegekend aan rekenperioden waarvoor nog geen SOBEK
berekeningen zijn gerealiseerd.

Locatie van de scripts
----------------------

Deze actie bestaat uit meerdere scripts:

-   src/HWS/1_regress_dynamics.py (zie bijlage N)

-   src/HWS/2_dostatic.py (zie bijlage O)

-   src/HWS/3_pixel_fixer.py (zie bijlage P)

Invoer
------

+-----------------+-----------------+-----------+-----------------+
| Bestand of      | Eenheid         | Resolutie | Bron of locatie |
| dataset         |                 |           |                 |
+=================+=================+===========+=================+
| Historische     | Meter           | 250 meter | PeilH\_\*.idf   |
| (referentie)    |                 |           |                 |
| HWS peilen      |                 |           |                 |
+-----------------+-----------------+-----------+-----------------+
| DMdec           | M               | Tabel     |                 |
| adeafvoeren.csv | eter^3^/seconde |           |                 |
+-----------------+-----------------+-----------+-----------------+
| Issealevel.idf  | \-              | 250 meter |                 |
+-----------------+-----------------+-----------+-----------------+
| Invloed.idf     | \-              | 250 meter |                 |
+-----------------+-----------------+-----------+-----------------+
| Blank.idf       | \-              | 250 meter |                 |
+-----------------+-----------------+-----------+-----------------+
| Isstatic.idf    | \-              | 250 meter |                 |
+-----------------+-----------------+-----------+-----------------+
| Rec             | M               | Tabel     | <https://wat    |
| ent_rhine_meuse | eter^3^/seconde |           | erinfo.rws.nl/> |
|                 |                 |           | ,               |
| \_discharge.csv |                 |           |                 |
|                 |                 |           | <http://vo      |
|                 |                 |           | ies-hydraulique |
|                 |                 |           | s.wallonie.be/> |
+-----------------+-----------------+-----------+-----------------+

Uitvoer
-------

  Bestand of dataset   Eenheid   Resolutie   Locatie
  -------------------- --------- ----------- ---------
  Peil_HWS.idf         m         250 meter   

Beschrijving
------------

De scripts voeren de volgende acties achtereenvolgens uit:

-   Grid invloed.idf geeft aan voor welke modelcellen een dynamisch peil
    berekend moet worden. Het script maakt voor elk van deze cellen een
    QH-relatie op basis van maandelijkse resultaten van het Landelijk
    Sobek Model (LSM) van de periode 1980-2007

-   Voor elke tijdstap in Recent_rhine_meuse_discharge.csv wordt voor
    elke cel een bijpassend peil uit de QH relatie gehaald. Dit geeft de
    peilen voor de cellen waar een dynamisch peil berekend moet worden

-   Voor de cellen waar een statisch peil berekend moet worden
    (gedefinieerd in isstatic.idf) word het peil van de betreffende
    maand uit het jaar 2000 overgenomen uit de gemodereerde LSM
    resultaten.

-   Tenslotte worden enkele correcties uitgevoerd om fouten in de
    gebruikte LSM resultaten te repareren. Deze zijn beschreven in
    Veranderingsrapportage LHM 4.1.
