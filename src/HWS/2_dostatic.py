"""
Script overgenomen (en aangepast naar Snakemake etc.) van: https://gitlab.com/deltares/imod/LHM-automation/-/tree/master/1_model_setup/make-HWS-peilen 
"""
import datetime
import os
import shutil
from pathlib import Path

import raster_func3

"""
The regress_dynamics.py script only filled in the dynamic parts.
This script finishes the static part, that varies between the months but for
any given month, not between the years.
"""
params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

unknown_data_start = datetime.datetime.strptime(params["date_start"], "%Y-%m-%d").date()
unknown_data_end = datetime.datetime.strptime(params["date_end"], "%Y-%m-%d").date()

nodata = -9999.0
seaLevel = 0
setSealevel = False

isstatic = raster_func3.raster2arr(path_input["isstatic"]).arr.data == 1
issealevel = raster_func3.raster2arr(path_input["issealevel"]).arr.data == 1

for month in range(1, 12 + 1):
    srcidfname = path_input[f"PEILH_ref_2000{month:02d}01"]
    src = raster_func3.raster2arr(srcidfname)

    for year in range(unknown_data_start.year, unknown_data_end.year + 1):
        if unknown_data_start <= datetime.date(year, month, 1) <= unknown_data_end:
            print(year, month)
            dstidfname = path_input[f"peil_hws_{year}{month:02d}01"]
            dst = raster_func3.raster2arr(dstidfname)
            dst[isstatic] = src[isstatic]

            if setSealevel:
                dst[issealevel] = seaLevel

            idfname_done = path_output[f"peil_hws_{year}{month:02d}01"]
            print(idfname_done)
            dst.write(idfname_done)
