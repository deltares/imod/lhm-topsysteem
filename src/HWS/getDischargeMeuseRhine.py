import datetime
import os
import urllib
from pathlib import Path

import imod
import numpy as np
import pandas as pd
import xarray as xr
from bs4 import BeautifulSoup  # pip install beautifulsoup4
from netCDF4 import Dataset

## Settings

# we start at the beginning of this year
dateStart = datetime.datetime(datetime.date.today().year, 1, 1)
# we end yesterday
dateEnd = datetime.datetime.today() - datetime.timedelta(days=1)

# dateStart = datetime.datetime(2020,1,1)
# dateEnd = datetime.datetime(2020,4,20)

## magic starts here

print("Lobith")

# make array to store all data
riverDischarge = pd.DataFrame()

# download lobith. Read the readme.doc, becuase this has some complications
lobith = pd.read_csv(
    "https://waterinfo.rws.nl/api/Download/CSV?expertParameter=Debiet___20Oppervlaktewater___20m3___2Fs&locationSlug=Lobith(LOBI)&timehorizon=-2000,0",
    delimiter=";",
    decimal=",",
)

# fix date
lobith["day"] = pd.to_datetime(lobith["Datum"], format="%d-%m-%Y")

# avarage discharge per day
q = lobith.groupby("day").mean()
q = pd.DataFrame(q.loc[(q.index >= dateStart) & (q.index <= dateEnd)]["Meting"]).rename(
    columns={"Meting": "lobith"}
)

# save rhine discharge
riverDischarge = pd.concat([riverDischarge, q], axis=1, sort=False)

# muese settings (see readme.docx)
rivers = [
    {"name": "amay", "code": "7132"},
    {"name": "chaudfontaine", "code": "6228"},
    {"name": "sauheid", "code": "5826"},
]


# loop over years
for year in np.arange(dateStart.year, dateEnd.year + 1):
    # loop over rivers (see readme.docx)
    for river in rivers:
        print(river["name"])
        # get html
        html = (
            urllib.request.urlopen(
                f'http://voies-hydrauliques.wallonie.be/opencms/opencms/fr/hydro/Archive/annuaires/statjourtab.do?code={river["code"]}1002&annee={year}&xt=prt'
            )
            .read()
            .decode("utf-8")
        )
        # parse html
        soup = BeautifulSoup(html, "html.parser")
        # we need the 5th table, and all rows of that table
        tableRows = soup.find_all("table")[4].find_all("tr")
        # create space to store all values
        allData = []
        # loop over rows, but skip first
        for row in tableRows[1:32]:
            data = [x.encode_contents() for x in row.find_all("td")]
            allData.append(data[1:])
        # all data in aray -> reshape to one long list
        allDataList = np.reshape(np.array(allData), (1, 12 * 31), order="F")[0]
        # remove empty (31 april does not exist and gives a gap)
        allDataList = [float(x) for x in allDataList if x != b""]
        # get dates for this value. We assume the first value is jan 1st and there are no gaps
        days = pd.date_range(
            start=datetime.datetime(year, 1, 1), end=datetime.datetime(year, 12, 31)
        )[0 : len(allDataList)]
        # data management
        maas = pd.DataFrame(allDataList, index=days, columns=[river["name"]])
        riverDischarge = maas.combine_first(riverDischarge)
        # riverDischarge = pd.concat([riverDischarge, maas], axis=1, sort=False)

# calulate river discharge at monsin
print("Monsin")
riverDischarge["monsin"] = (
    riverDischarge["amay"] + riverDischarge["chaudfontaine"] + riverDischarge["sauheid"]
)

# trim to required date range
riverDischarge = riverDischarge.loc[
    (riverDischarge.index >= dateStart) & (riverDischarge.index <= dateEnd), :
]

# save data
riverDischarge.index.name = "date"
Path("3-output").mkdir(parents=True, exist_ok=True)
riverDischarge.loc[:, ["monsin", "lobith"]].round(3).to_csv(
    "3-output/maas-rijn.csv", date_format="%Y-%m-%d"
)
