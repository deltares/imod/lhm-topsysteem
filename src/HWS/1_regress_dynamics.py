"""
Script overgenomen (en aangepast naar Snakemake etc.) van: https://gitlab.com/deltares/imod/LHM-automation/-/tree/master/1_model_setup/make-HWS-peilen 
"""
import datetime
import os
import shutil
import sys
from pathlib import Path
from struct import pack, unpack

import pandas as pd
import raster_func3
from numpy import *

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# TODO: sensible dates
unknown_data_start = datetime.datetime.strptime(params["date_start"], "%Y-%m-%d").date()
unknown_data_end = datetime.datetime.strptime(params["date_end"], "%Y-%m-%d").date()

# The dates of the known data set are relates to the
# idfs in input/reference-data
kwown_data_start = datetime.date(1980, 1, 1)
kwown_data_end = datetime.date(2006, 12, 31)

nodata = -9999.0

# https://stackoverflow.com/a/51575528
def months(start_date, end_date, day=1):
    """Produce a date for every month from start until end"""
    start = start_date.year * 12 + (start_date.month - 1)
    if start_date.day > day:
        # already in this month, so start counting at the next
        start += 1
    end = end_date.year * 12 + (end_date.month - 1)
    if end_date.day > day:
        # end date is past the reference day, include the reference
        # date in the output
        end += 1
    # generate the months, just a range from start to end
    for ordinal in range(start, end):
        yield datetime.date(ordinal // 12, (ordinal % 12) + 1, day)


# get number of known data years
ndata = kwown_data_end.year - kwown_data_start.year + 1

# copy template IDF to target IDFs
for date in months(unknown_data_start, unknown_data_end):
    idfname = path_output[f'peil_hws_{date.strftime("%Y%m%d")}']
    shutil.copy2(path_input["blank_idf"], idfname)

# construct dicts of opened file handles
handledict = {}
for date in months(kwown_data_start, kwown_data_end):
    idfname = path_input[f'PEILH_ref_{date.strftime("%Y%m%d")}']
    handledict[idfname] = open(idfname, "rb")

# read input data
# this is the series for which we have grids based on Sobek LSM results
q_basis_daily = pd.read_csv(
    path_input["DMdecadeafvoeren"],
    index_col=0,
    parse_dates=True,
    #    dtype=float
)

# this is the series of Lobith & Monsin for which we would like to calculate the grids
q_new_daily = pd.read_csv(
    path_input["recent_rhine_meuse_discharge"],
    index_col=0,
    parse_dates=True,
    #    dtype=float
)

# resample to months, making sure the time label for Jan is 01-01
q_basis = q_basis_daily.resample("M", label="left", loffset="D").mean()
q_new = q_new_daily.resample("M", label="left", loffset="D").mean()

invloedidf = raster_func3.raster2arr(path_input["invloed_idf"])
invloed = invloedidf.arr.data
datamap = invloed != nodata  # 95014 data cells
ndatacells = datamap.sum()
dataidx = where(datamap)

nrow, ncol = datamap.shape

nactcells = 0

# loop over all data cells
# for each data cell, build a qh table,
# and interpolate into this table to find
# the unknown h based on known q for that year
cellcount = 0
for i, j in zip(*dataidx):
    if cellcount % 100 == 0:
        print(
            f"{round(cellcount/ndatacells*100)}% of pixels checked. Updated {nactcells} pixels."
        )
    # location of this cell in an IDF, in bytes
    idfpos = 52 + (4 * j) + (4 * i * ncol)

    if invloed[i, j] == 1:
        riv = "rijn"
    else:
        riv = "maas"

    for month in range(1, 12 + 1):

        # build lookup table based on available data
        q_data = q_basis.loc[q_basis.index.month == month, riv]
        q_nodata = q_new.loc[q_new.index.month == month, riv]
        h = zeros(ndata) + nodata
        c = 0
        assert len(q_data) == ndata

        for year in range(kwown_data_start.year, kwown_data_end.year + 1):
            idfname = path_input[f"PEILH_ref_{year}{month:02d}01"]
            f = handledict[idfname]
            f.seek(idfpos)
            h[c] = unpack("f", f.read(4))[0]
            c += 1

        # only those that actually matter
        if ptp(h) > 0.01:
            nactcells += 1

            qh = pd.Series(h, index=q_data)
            qh.sort_index(inplace=True)

            # find h for unknown years, using qh table
            new_index = qh.index | pd.Index(q_nodata)
            # interpolate the table to get h for new qs, if a new q is lowest, use the h of the next q with value
            qh_interp = (
                qh.reindex(new_index)
                .interpolate(method="index")
                .fillna(method="backfill")
            )
            qh_interp = qh_interp[~qh_interp.index.duplicated(keep="last")]
            for year in range(unknown_data_start.year, unknown_data_end.year + 1):
                if (
                    unknown_data_start
                    <= datetime.date(year, month, 1)
                    <= unknown_data_end
                ):
                    q_val = q_nodata[str(year)].values[0]
                    h_val = qh_interp[q_val]
                    idfname = path_output[f"peil_hws_{year}{month:02d}01"]
                    with open(idfname, "rb+") as f:
                        f.seek(idfpos)
                        f.write(pack("f", h_val))

    cellcount += 1

# close all file handles
for fname, handle in handledict.items():
    handle.close()

print(
    f"{round(cellcount/ndatacells*100)}% of pixels checked. Updated {nactcells} pixels."
)
