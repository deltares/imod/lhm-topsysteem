
'''
In dit script wordt per sloottype (primair, secundair, tertiair) een tabel aangemaakt 
met de benodigde parameters uit de Massop tabellen (drooglegging, breedte, waterdiepte en bodemdiepte)
'''
import pandas as pd 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

drooglegging = pd.read_csv(path_input['drooglegging_hydrotype'], index_col='hydrotype2')
drooglegging_GT = pd.read_csv(path_input['drooglegging_GTtype'], index_col='GT_num')
breedte = pd.read_csv(path_input['breedte_hydrotype'], index_col='hydrotype2')
bodemdiepte = pd.read_csv(path_input['bodemdiepte_hydrotype'], index_col='hydrotype2')
waterdiepte = pd.read_csv(path_input['waterdiepte_hydrotype'], index_col='hydrotype2')

primair_data = pd.concat([drooglegging['sloot36'],breedte['sloot36'],bodemdiepte['sloot36'],waterdiepte['sloot36']], axis=1)
primair_data.columns = ['drooglegging', 'breedte', 'bodemdiepte', 'waterdiepte']

primair_vlakken_data = pd.concat([drooglegging['sloot6'],bodemdiepte['sloot6'],waterdiepte['sloot6']], axis=1)
primair_vlakken_data.columns = ['drooglegging', 'bodemdiepte', 'waterdiepte']

secundair_data = pd.concat([drooglegging['sloot13'],breedte['sloot13'],bodemdiepte['sloot13'],waterdiepte['sloot13']], axis=1)
secundair_data.columns = ['drooglegging', 'breedte', 'bodemdiepte', 'waterdiepte']

tertiair_data = pd.concat([breedte['Greppel'],bodemdiepte['ontwatering']], axis=1)
tertiair_data.columns = ['breedte', 'bodemdiepte']

tertiair_data_GT = pd.concat([drooglegging_GT['ontwateringspeil']], axis=1)
tertiair_data_GT.columns = ['drooglegging']

primair_data.to_csv(path_output['primair_params_hydrotype'])
primair_vlakken_data.to_csv(path_output['primair_vlakken_params_hydrotype'])
secundair_data.to_csv(path_output['secundair_params_hydrotype'])
tertiair_data.to_csv(path_output['tertiair_params_hydrotype'])
tertiair_data_GT.to_csv(path_output['tertiair_params_GT'])







