"""
Buisdrainage
============
In dit script worden de locatie en de weerstand van de buisdrainage
bepaald.

Een uitgebreidere beschrijving staat in de documentatie.

De locatie van de buisdrainage wordt bepaald middels de volgende
stappen:
-   gebaseerd op buisdrainage_weerstand grid
    (buisdrainage_weerstand.asc);
-   Verwijderen uit natte natuur, landgebruik = 13

De bodemhoogte van de buisdrainage wordt bepaald middels de volgende
stappen:
-   Minimaal: AHN -- hoogste van landsdekkend winter- of zomerpeil + 10
    cm
-   Anders: buisdrainage_diepte grid (als deze diepte dus kleiner is)

De conductance van de buisdrainage wordt bepaald als:
-   Celoppervlak / weerstand_buisdrainage

"""
import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))


# make like_25m.nc
dx, xmin, xmax, dy, ymin, ymax = (
    params["celgrootte_klein"],
    0.0,
    300000.0,
    params["celgrootte_klein"] * -1,
    300000.0,
    625000.0,
)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

modus_2d_regridder = imod.prepare.Regridder(method="mode")

landgebruik = imod.rasterio.open(path_input["landgebruik"])
landgebruik_regrid = modus_2d_regridder.regrid(landgebruik, like= like_25m)

weerstand_buisdrainage = imod.rasterio.open(path_input["weerstand_buisdrainage"])
bodemdiepte_buisdrainage = np.divide(
    imod.rasterio.open(path_input["bodemdiepte_buisdrainage"]), 100.0
)  # Invoer is in cm
peil_landsdekkend_zomer = xr.open_dataarray(path_input["peil_landsdekkend_zomer"])
peil_landsdekkend_winter = xr.open_dataarray(path_input["peil_landsdekkend_winter"])
AHN = xr.open_dataarray(path_input["AHN"])


# Afmetingen buisdrainage grids zijn iets te klein, aanpassen:
modus_2d_regridder = imod.prepare.Regridder(method="mode")
weerstand_buisdrainage = modus_2d_regridder.regrid(weerstand_buisdrainage, like= like_25m)
bodemdiepte_buisdrainage = modus_2d_regridder.regrid(bodemdiepte_buisdrainage, like= like_25m)

# LOCATIE
is_buisdrainage = xr.where(
    (weerstand_buisdrainage > 0.0) & (landgebruik_regrid != 13), True, False
)

# BODEMHOOGTE
# Bodemdiepte van buisdrainage is minimaal AHN minus het hoogste vlakdekkende peil van winter- of zomerpeil + 10 cm
# De waarde uit de buisdrainageinvoer wordt gebruikt als deze diepte kleiner is.
bodemdiepte_buisdrainage_correctie = (
    AHN - (0.1 + np.fmax(peil_landsdekkend_winter, peil_landsdekkend_zomer))
    )  # gebruik fmax om te voorkomen dat er NaN uitkomt
bodemdiepte_buisdrainage_correctie = np.maximum(bodemdiepte_buisdrainage_correctie, 0.0)

bodemdiepte_buisdrainage_gecorrigeerd = xr.where(
    is_buisdrainage,
    np.fmin(
        bodemdiepte_buisdrainage_correctie, bodemdiepte_buisdrainage
    ), # gebruik fmin om te voorkomen dat er NaN uitkomt
    np.nan)

bodemhoogte_buisdrainage = AHN - bodemdiepte_buisdrainage_gecorrigeerd

bodemhoogte_buisdrainage.to_netcdf(path_output["bodemhoogte_buisdrainage"])

# CONDUCTANCE
conductance_buisdrainage = xr.where(
    is_buisdrainage, params["celoppervlak_klein"] / weerstand_buisdrainage, np.nan
)

conductance_buisdrainage.to_netcdf(path_output["conductance_buisdrainage"])
