"""
In dit script worden de rekenregels toegepast om de bodemhoogte per rastercell in het vrijafwaterende gebied te bepalen.

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

waterdiepte_primair = imod.idf.open(path_input['waterdiepte_primair'])
waterdiepte_secundair = imod.idf.open(path_input['waterdiepte_secundair'])

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

# Regridder aanmaken
mean_2d_regridder = imod.prepare.Regridder(method="mean")
waterdiepte_primair = mean_2d_regridder.regrid(waterdiepte_primair, like_25m)
mean_2d_regridder = imod.prepare.Regridder(method="mean")
waterdiepte_secundair = mean_2d_regridder.regrid(waterdiepte_secundair, like_25m)


#-------------
## Primair
#-------------
primair_zomerpeil = xr.open_dataarray(path_input['primair_peil_zomer_vrijafwater'])
primair_winterpeil = xr.open_dataarray(path_input['primair_peil_winter_vrijafwater'])
primair_peil = xr.where(primair_zomerpeil < primair_winterpeil, primair_zomerpeil, primair_winterpeil)

# zomer vrijafwater: bodh = peil - diepte
bodemhoogte_zomer_primair_afwatering = xr.where(primair_peil.notnull(), primair_peil - waterdiepte_primair, np.nan)
bodemhoogte_zomer_primair_afwatering.to_netcdf(path_output['primair_bodemhoogte_zomer_vrijafwater'])


# winter vrijafwater: bodh = peil - diepte
bodemhoogte_winter_primair_afwatering = xr.where(primair_peil.notnull(), primair_peil - waterdiepte_primair, np.nan)
bodemhoogte_winter_primair_afwatering.to_netcdf(path_output['primair_bodemhoogte_winter_vrijafwater'])


# -----
## Secundair
# -----
secundair_zomerpeil_afwatering = xr.open_dataarray(path_input['secundair_peil_zomer_vrijafwater_afwatering'])
secundair_winterpeil_afwatering = xr.open_dataarray(path_input['secundair_peil_winter_vrijafwater_afwatering'])
secundair_peil_afwatering = xr.where(secundair_zomerpeil_afwatering  < secundair_winterpeil_afwatering , secundair_zomerpeil_afwatering , secundair_winterpeil_afwatering )
secundair_zomerpeil_ontwatering = xr.open_dataarray(path_input['secundair_peil_zomer_vrijafwater_ontwatering'])
secundair_winterpeil_ontwatering = xr.open_dataarray(path_input['secundair_peil_winter_vrijafwater_ontwatering'])
secundair_peil_ontwatering = xr.where(secundair_zomerpeil_ontwatering < secundair_winterpeil_ontwatering, secundair_zomerpeil_ontwatering, secundair_winterpeil_ontwatering)

# zomer vrijafwater: bodh = peil - diepte
bodemhoogte_zomer_secundair_afwatering = xr.where(secundair_peil_afwatering.notnull(), secundair_peil_afwatering - waterdiepte_secundair, np.nan)
bodemhoogte_zomer_secundair_afwatering.to_netcdf(path_output['secundair_bodemhoogte_zomer_vrijafwater_afwatering'])

# winter vrijafwater: bodh = peil - diepte
bodemhoogte_winter_secundair_afwatering = xr.where(secundair_peil_afwatering.notnull(), secundair_peil_afwatering - waterdiepte_secundair, np.nan)
bodemhoogte_winter_secundair_afwatering.to_netcdf(path_output['secundair_bodemhoogte_winter_vrijafwater_afwatering'])

# zomer vrijafwater: bodh = peil - diepte
bodemhoogte_zomer_secundair_ontwatering = xr.where(secundair_peil_ontwatering.notnull(), secundair_peil_ontwatering - waterdiepte_secundair, np.nan)
bodemhoogte_zomer_secundair_ontwatering.to_netcdf(path_output['secundair_bodemhoogte_zomer_vrijafwater_ontwatering'])

# winter vrijafwater: bodh = peil - diepte
bodemhoogte_winter_secundair_ontwatering = xr.where(secundair_peil_ontwatering.notnull(), secundair_peil_ontwatering - waterdiepte_secundair, np.nan)
bodemhoogte_winter_secundair_ontwatering.to_netcdf(path_output['secundair_bodemhoogte_winter_vrijafwater_ontwatering'])
# -----
## Tertiair
# -----
tertiair_zomerpeil = xr.open_dataarray(path_input['tertiair_peil_zomer_vrijafwater'])
tertiair_winterpeil = xr.open_dataarray(path_input['tertiair_peil_winter_vrijafwater'])

# zomer vrijafwater: bodh = peil
tertiair_zomerpeil.to_netcdf(path_output['tertiair_bodemhoogte_zomer_vrijafwater'])

# winter vrijafwater: bodh = peil
tertiair_winterpeil.to_netcdf(path_output['tertiair_bodemhoogte_winter_vrijafwater'])

