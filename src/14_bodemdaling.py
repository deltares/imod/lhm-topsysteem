"""
Bodemdaling
===========

Toepassen bodemdaling op peilen en bodemhoogtes van het primaire,
secundaire en tertiair systeem en op de bodemhoogtes van buisdrainage en
SOF (Surface Overland Flow).

De bodemdaling- en landgebruikrasters zijn momenteel op 250 m resolutie
beschikbaar. Deze worden eerst teruggeschaald naar 25 m resolutie, zodat
ze gebruikt kunnen worden voor de aanpassing van de bodemhoogtes en
peilen.

Het maaiveld wordt verlaagd met de bodemdaling, net als de peilen en
bodemhoogtes van het primair-, secundair-, tertiair riviersysteem.

Vervolgens worden de peilen aangepast in de gebieden met natte natuur.
Natte natuur wordt bepaald aan de hand van de (scenarioafhankelijke)
landgebruikskaart. Natte natuur komt overeen met landgebruikswaarde 13.
Hierbij geldt dat in de natte natuurgebieden het peil maximaal 20 cm
onder het maaiveld mag staan. Wanneer het peil dieper dan 20 cm onder
het maaiveld ligt, wordt dit aangepast naar 20 cm onder maaiveld. Bij
deze berekening wordt dus het maaiveld gebruikt dat al is aangepast
volgens de bodemdaling.

Als laatste wordt de bodemdaling afgetrokken van de hoogtes van
buisdrainage en SOF.

"""

import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

bodemdaling = imod.rasterio.open(path_input["bodemdaling"])
landgebruik = imod.rasterio.open(path_input["landgebruik"])

# Bodemdaling & landgebruik grids zijn nu op 250 m, dit wordt eerst teruggeschaald naar 25 m.
# make like_25m.nc
dx, xmin, xmax, dy, ymin, ymax = (
    params["celgrootte_klein"],
    0.0,
    300000.0,
    params["celgrootte_klein"] * -1,
    300000.0,
    625000.0,
)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

mean_2d_regridder = imod.prepare.Regridder(method="mean")
modus_2d_regridder = imod.prepare.Regridder(method="mode")
bodemdaling_regrid = mean_2d_regridder.regrid(bodemdaling, like_25m)
landgebruik_regrid = modus_2d_regridder.regrid(landgebruik, like_25m)

# Verlaag maaiveld met bodemdaling:
maaiveld = xr.open_dataarray(path_input["AHN"])
maaiveld_bodemdaling = maaiveld - bodemdaling_regrid

# Verlaag peil & bodemhoogte van rivieren:
soort_rivieren = ["primair", "secundair", "tertiair"]
seizoenen = ["zomer", "winter"]

for soort in soort_rivieren:
    for seizoen in seizoenen:
        bodemhoogte = xr.open_dataarray(path_input[f"bodemhoogte_{soort}_{seizoen}"])
        bodemhoogte_bodemdaling = bodemhoogte - bodemdaling_regrid
        bodemhoogte_bodemdaling.to_netcdf(path_output[f"bodemhoogte_{soort}_{seizoen}"])

        peil = xr.open_dataarray(path_input[f"peil_{soort}_{seizoen}"])
        peil_bodemdaling = peil - bodemdaling_regrid

        # Pas het peil aan zodat in gebieden met natte natuur het peil minimaal
        # 20 cm onder maaiveld staat
        drooglegging = maaiveld_bodemdaling - peil_bodemdaling
        peil_bodemdaling = xr.where(
            (landgebruik_regrid == 13)
            & (drooglegging > params["peilcorrectie_natte_natuur"]),
            maaiveld_bodemdaling - params["peilcorrectie_natte_natuur"],
            peil_bodemdaling,
        )
        peil_bodemdaling.to_netcdf(path_output[f"peil_{soort}_{seizoen}"])


# Verlaag bodemhoogte van buisdrainage & SOF & greppels
for soort in ["SOF", "buisdrainage", "greppels"]:
    bodemhoogte = xr.open_dataarray(path_input[f"bodemhoogte_{soort}"])
    bodemhoogte_bodemdaling = bodemhoogte - bodemdaling_regrid
    bodemhoogte_bodemdaling.to_netcdf(path_output[f"bodemhoogte_{soort}"])
