"""
In dit script worden voor alle peilgestuurde (primaire, secundaire en tertiaire) sloten de benodigde peilgegevens
verzameld via een zonal aggregation.

Hierbij wordt per polygoon één waarde bepaald van de verschillende peilgegevens, 
bijvoorbeeld op basis van de meest voorkomende waarde binnen de polygoon

Dit zijn:
Peilvakkenpeil (zomer & winter)
Regiopeil (zomer & winter)
ahn
ahn-peil

Resultaat is csv met alle peil-parameters per sloot-polygoon 

!!! Note that the pc should have enough memory & CPU available
!!! Het script loopt dan niet vast, maar blijft wel hangen, het zal opvallen dat één van de parameters
veel langer bezig is en niet meer vooruit komt.
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd
from dask.diagnostics import ProgressBar
import scipy.stats 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# Put the coords of the submodel:
xmin_NL = 0.
xmax_NL = 300000.
ymin_NL = 300000.0
ymax_NL = 625000.0

xmin = xmin_NL
xmax = xmax_NL#xmax_NL / 2.
ymin = ymin_NL
ymax = ymax_NL#int(((ymax_NL-ymin_NL) / 2) + ymin_NL)

# Maak like van specifiek gebied:
dx, xmin, xmax, dy, ymin, ymax = (25.0, xmin, xmax, -25.0, ymin, ymax)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

for sloottype in ['tertiair', 'primair', 'secundair']:
    print(sloottype)
    pad_gebufferd = f'{path_input[f"{sloottype}_peilgestuurd"]}'

    polygonen = gpd.read_file(pad_gebufferd)
    polygonen['lokaalid'] = polygonen['lokaalid'].astype(int)
    polygonen = polygonen.set_index('lokaalid')
    
    if sloottype == 'tertiair':
        polygonen['waterdiepte'] = polygonen['bodemdiepte'].astype(float) - polygonen['drooglegging'].astype(float)
    
    # Peilvakkenkaart (polygonen):
    print('zomerpeil_peilvak')
    with ProgressBar():
        zomerpeil_peilvak = imod.prepare.spatial.zonal_aggregate_polygons(
            path_a=pad_gebufferd,
            column_a='lokaalid',
            path_b=path_input['peilgebieden'],
            column_b='ZOMERPEIL',
            like=like_25m,
            resolution=2.5,
            method=scipy.stats.mode,
            chunksize=7000
            )
    zomerpeil_peilvak['ZOMERPEIL']= zomerpeil_peilvak['ZOMERPEIL'].apply(lambda row: row[0][0])
    zomerpeil_peilvak = zomerpeil_peilvak.rename(columns={'ZOMERPEIL':'zomerpeil_peilvak'})

    print('winterpeil_peilvak')
    with ProgressBar():
        winterpeil_peilvak = imod.prepare.spatial.zonal_aggregate_polygons(
            path_a=pad_gebufferd,
            column_a='lokaalid',
            path_b=path_input['peilgebieden'],
            column_b='WINTERPEIL',
            like=like_25m,
            resolution=2.5,
            method=scipy.stats.mode,
            chunksize=7000
            )
    winterpeil_peilvak['WINTERPEIL']= winterpeil_peilvak['WINTERPEIL'].apply(lambda row: row[0][0]) # pd.Series.mode kan multiple modi vinden, selecteer standaard de eerste:
    winterpeil_peilvak = winterpeil_peilvak.rename(columns={'WINTERPEIL':'winterpeil_peilvak'})
    
    
    # Dan de regiopeilen (rasters):
    regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer']).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).compute()
    print('regiopeil_zomer')
    with ProgressBar():
        zomerpeil_regiopeil = imod.prepare.spatial.zonal_aggregate_raster(
            path = pad_gebufferd,
            column='lokaalid',
            raster=regiopeil_zomer,
            resolution=2.5,
            method=scipy.stats.mode,
            chunksize=7000
        ) 

    zomerpeil_regiopeil['peil_z_buf']= zomerpeil_regiopeil['peil_z_buf'].apply(lambda row: row[0][0]) # pd.Series.mode kan multiple modi vinden, selecteer standaard de eerste
    zomerpeil_regiopeil = zomerpeil_regiopeil.rename(columns={'peil_z_buf':'zomerpeil_regiopeil'})

    regiopeil_winter = xr.open_dataarray(path_input['regiopeil_winter']).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).compute()
    print('Regiopeil_winter')
    with ProgressBar():
        winterpeil_regiopeil = imod.prepare.spatial.zonal_aggregate_raster(
            path = pad_gebufferd,
            column='lokaalid',
            raster=regiopeil_winter,
            resolution=2.5,
            method=scipy.stats.mode,
            chunksize=7000
        ) 
    winterpeil_regiopeil['peil_w_buf']= winterpeil_regiopeil['peil_w_buf'].apply(lambda row: row[0][0]) # pd.Series.mode kan multiple modi vinden, selecteer standaard de eerste:
    winterpeil_regiopeil = winterpeil_regiopeil.rename(columns={'peil_w_buf':'winterpeil_regiopeil'})

    # Ook nog het AHN:
    ahn = xr.open_dataarray(path_input['ahn']).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).compute()
    ahn = ahn.rename('ahn')
    print('ahn')
    with ProgressBar():
        ahn_polygoon = imod.prepare.spatial.zonal_aggregate_raster(
            path = pad_gebufferd,
            column='lokaalid',
            raster=ahn,
            resolution=2.5,
            method='mean',
            chunksize=7000
        ) 
    
    
    # En AHN-peil, dit is een peilbestand dat gebaseerd is op het AHN
    peil_ahn = xr.open_dataarray(path_input['ahnpeil']).sel(x=slice(xmin, xmax), y=slice(ymax, ymin)).compute()
    peil_ahn = peil_ahn.rename('ahnpeil')

    # Fix eenheid
    peil_ahn = peil_ahn / 1000

    print('ahnpeil')
    with ProgressBar():
        ahnpeil = imod.prepare.spatial.zonal_aggregate_raster(
            path = pad_gebufferd,
            column='lokaalid',
            raster=peil_ahn,
            resolution=2.5,
            method='mean',
            chunksize=7000
        ) 
    

    # Due to the use of chunks, one polygon can be split into two
    # Sort by area and use the stage of the chunk with the largest area

    zomerpeil_regiopeil = zomerpeil_regiopeil.set_index('lokaalid')
    zomerpeil_regiopeil = zomerpeil_regiopeil.sort_values(by=['lokaalid', 'area'])
    zomerpeil_regiopeil = zomerpeil_regiopeil.loc[~zomerpeil_regiopeil.index.duplicated(keep='last')]

    winterpeil_regiopeil = winterpeil_regiopeil.set_index('lokaalid')
    winterpeil_regiopeil = winterpeil_regiopeil.sort_values(by=['lokaalid', 'area'])
    winterpeil_regiopeil = winterpeil_regiopeil.loc[~winterpeil_regiopeil.index.duplicated(keep='last')]

    zomerpeil_peilvak = zomerpeil_peilvak.set_index('lokaalid')
    zomerpeil_peilvak = zomerpeil_peilvak.sort_values(by=['lokaalid', 'area'])
    zomerpeil_peilvak = zomerpeil_peilvak.loc[~zomerpeil_peilvak.index.duplicated(keep='last')]

    winterpeil_peilvak = winterpeil_peilvak.set_index('lokaalid')
    winterpeil_peilvak = winterpeil_peilvak.sort_values(by=['lokaalid', 'area'])
    winterpeil_peilvak = winterpeil_peilvak.loc[~winterpeil_peilvak.index.duplicated(keep='last')]

    ahn_polygoon = ahn_polygoon.set_index('lokaalid')
    ahn_polygoon = ahn_polygoon.sort_values(by=['lokaalid', 'area'])
    ahn_polygoon = ahn_polygoon.loc[~ahn_polygoon.index.duplicated(keep='last')]

    ahnpeil = ahnpeil.set_index('lokaalid')
    ahnpeil = ahnpeil.sort_values(by=['lokaalid', 'area'])
    ahnpeil = ahnpeil.loc[~ahnpeil.index.duplicated(keep='last')]

    ahn_params = pd.concat([polygonen[['breedte', 'drooglegging', 'bodemdiepte']], ahn_polygoon['ahn']], axis=1)
    ahn_params['ahn_drooglegging'] = ahn_params['ahn'] - ahn_params['drooglegging'].astype(float)
    ahn_params['bodemhoogte'] =  ahn_params['ahn'] - ahn_params['bodemdiepte'].astype(float)
    
    peil = pd.concat([
        polygonen[['LSWtype', 'breedte', 'waterdiepte']], 
        zomerpeil_peilvak['zomerpeil_peilvak'], 
        winterpeil_peilvak['winterpeil_peilvak'], 
        zomerpeil_regiopeil['zomerpeil_regiopeil'], 
        winterpeil_regiopeil['winterpeil_regiopeil'], 
        ahnpeil['ahnpeil'],
        ahn_params[['ahn', 'ahn_drooglegging', 'bodemhoogte']]
        ],axis=1)
    

    peil.to_csv(f'{path_output[f"{sloottype}_zonal_agg_peil_peilgestuurd"]}')