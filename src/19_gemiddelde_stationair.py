# -*- coding: utf-8 -*-
"""
Created on Thu Dec  8 13:04:24 2022

@author: meeusen
Doel script: Gemiddelde RIV peil berekenen, nodig voor stationaire som 


Opmerkingen
    - 
"""

import os
import pandas as pd
import geopandas as gpd
import numpy as np
import xarray as xr
from tqdm import tqdm
import datetime
import matplotlib.pyplot as plt
import imod

##-------------------------------------------
## INVOER, PARAMETERS en UITVOER 
##-------------------------------------------


## PARAMETERS
var = ['PEIL','BODH','cond']
sys1 = ['P','S','T']
sys2 = ['p','s','t'] # want soms met kleine letters


## INVOER
params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

## Data inlezen en gemiddelde berekenen
data = {}
for v in var:
    data[v] = {}
    if v == 'cond':
        for s,s2 in zip(sys1,sys2):
            data[v][s] = xr.Dataset()
            data[v][s]['winter'] = imod.idf.open(path_input[f'{v}_{s2}_winter'],pattern='{name}')
            data[v][s]['zomer'] = imod.idf.open(path_input[f'{v}_{s2}_zomer'],pattern='{name}')
            data[v][s]['gemiddeld'] = data[v][s][['zomer','winter']].to_array(dim='new').mean('new')
            imod.idf.write(path_output[f'{v}_{s}_gemiddeld'], data[v][s]['gemiddeld'])
    else:
        for s in sys1:
            data[v][s] = xr.Dataset()
            try:  # omdat bodh_t1 niet bestaat
                data[v][s]['winter'] = imod.idf.open(path_input[f'{v}_{s}_winter'],pattern='{name}')
                data[v][s]['zomer'] = imod.idf.open(path_input[f'{v}_{s}_zomer'],pattern='{name}')
                data[v][s]['gemiddeld'] = data[v][s][['zomer','winter']].to_array(dim='new').mean('new')    
            except: continue
        
## Gemiddelde wegschrijven
for v in var:
    for s in sys1:
        try: # omdat bodh_t1 niet bestaat
            imod.idf.write(path_output[f'{v}_{s}_gemiddeld'],data[v][s]['gemiddeld'])
        except: continue

