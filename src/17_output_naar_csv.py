"""

De resultaten van voorgaande scripts moeten in een vijftal tabellen
samengevat worden. Deze tabellen zijn invoer voor de MOZART
programmatuur van het NHI.

**LSWattr.csv**

Deze tabel bevat informatie per LSW. Voor het genereren van deze
informatie is gebruikt gemaakt van de 250 meter grids.

De tabel heeft de volgende kolommen:

-   LSW peil: het gemiddelde peil per LSW. Hiervoor wordt per cel (250 x
    250 m) het winterpeil van het primaire systeem bepaald, aangevuld
    met de secundaire winterpeilen waar geen primair systeem aanwezig
    is. Van alle cellen die in één LSW vallen wordt het gemiddelde peil
    bepaald(d.m.v. de imod-python functie zonal_aggregate);

-   LSW area: het totale wateroppervlak per LSW. Dit wordt berekend door
    het totale wateroppervlak te sommeren per LSW;

-   LSW zout: de gemiddelde waarde uit het bestand saltnew.asc per LSW;

-   LSW urban: het totale oppervlak stedelijk gebied per LSW. Totale verhard gebied te sommeren per LSW

**WAattr.csv**

Deze tabel bevat informatie per weir area (WA). Voor het genereren van
deze informatie is gebruikt gemaakt van de 250 meter grids.

Dit bestand bevat de volgende kolommen:

-   LSW nummer;

-   WA nummer;

-   Peil in het weir area.

Om voor elke WA te bepalen in welke LSW de WA ligt, wordt de overlap
tussen de twee shapes bepaald. In theorie zou elke weir area in één LSW
moeten liggen. Dit is echter in de praktijk niet overal het geval
vanwege imperfecties in de shapefile. Om dit te corrigeren wordt elke WA alleen
gekoppeld aan de LSW waarmee het de grootste overlap heeft.

Het peil per WA wordt op dezelfde manier bepaald als het peil per LSW

**MFtoLSW.csv**

Deze tabel beschrijft voor elke MODFLOW-cel in welke LSW en WA deze
ligt.

Het bestand bevat de volgende kolommen:

-   icol: kolomnummer van de cel;

-   irow: regelnummer van de cel;

-   LSW nummer;

-   WA nummer;

-   winterpeilcorrectie.

De winterpeilcorrectie wordt als volgt bepaald:

-   De peilen per LSW en per WA (zoals berekend voor respectievelijk
    LSWattr.csv en WAattr.csv) worden omgezet naar een raster. Deze
    peilen worden gecombineerd tot één kaart. Hierbij geldt dat het WA
    peil overgenomen wordt; wanneer deze niet bekend is, wordt het LSW
    peil gebruikt. Dit betekent dat in het grootste deel van het
    vrijafwaterende gebied de WA peilen gebruikt worden, en in het
    peilbeheerste gebied de LSW peilen van toepassing zijn. Dit raster
    heet WALSW_peil.

-   Daarna wordt een raster gemaakt waarin de primaire, secundaire en
    tertiaire winterpeilen samengevoegd zijn. Dit raster bevat in
    hoofdzaak de primaire peilen, waar de secundaire peilen en tertiaire
    peilen als aanvulling gelden: daar waar geen primair peil aanwezig
    is, wordt het secundaire peil gebruikt, als deze ook niet aanwezig
    is wordt het tertiaire peil gebruikt. Dit raster wordt peil_winter
    genoemd.

-   De peilcorrectie is het verschil tussen peil_winter en het
    WALSW_peil.

Alleen voor de locaties waar de peilcorrectie een waarde heeft, wordt de
data weggeschreven naar MFtoLSW.csv.

**MFtoLSW_zomer.csv**

Dit bestand is nagenoeg gelijk aan MFtoLSW.csv, nu wordt er alleen een
peilcorrectie gemaakt voor de zomerpeilen. Hiervoor worden de
zomerpeilen van het primaire, secundaire tertiaire systeem gebruikt om
peil_zomer te maken. De peilcorrectie is het verschil tussen peil_zomer
en WALSW_peil.

Het bestand bevat de volgende kolommen:

-   icol: kolomnummer van de cel;

-   irow: regelnummer van de cel;

-   LSW nummer;

-   WA nummer;

-   zomerpeilcorrectie.

**PlottoLSW.csv**

In deze tabel worden de MetaSWAP ID's (gedefinieerd in een invoerraster)
gekoppeld aan het LSW nummer waar het MetaSWAP-gebied in ligt.

Deze tabel bevat de volgende kolommen:

-   Metaswap ID;

-   LSW nummer.

"""
import geopandas as gpd
import imod
import numpy as np
import pandas as pd
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

LSW_type = gpd.read_file(path_input["LSW_type"])
LSW_kaart = gpd.read_file(path_input["LSW_kaart"])
WA_kaart = gpd.read_file(path_input["WA_kaart"])
verhard_opp = imod.rasterio.open(path_input["verhard_opp"]).rename('stedelijk')
zout_water = imod.rasterio.open(path_input["zout_water"]).rename('zout_water')
wateroppervlak_totaal = xr.open_dataarray(path_input["wateroppervlak_totaal"]).rename('wateroppervlak_totaal')
ID_gridcel_metaswap = imod.rasterio.open(path_input["ID_gridcel_metaswap"]).rename('metaswap_ID')
like_groveschaal = xr.open_dataarray(path_input["like_groveschaal"])

lsw_grid = imod.prepare.rasterize(LSW_kaart, like=like_groveschaal, column="LSWFINAL")
wa_grid = imod.prepare.rasterize(WA_kaart, like=like_groveschaal, column="WACODE")
lsw_type_grid = imod.prepare.rasterize(LSW_type, like=like_groveschaal, column="TYPEint")

LSW_type = LSW_type.set_index('LSWFINAL')
LSW_type = LSW_type.sort_values(by=['LSWFINAL', 'AREA'])
LSW_type = LSW_type.loc[~LSW_type.index.duplicated(keep='last')]

peil_primair_winter = xr.open_dataarray(path_input["peil_primair_winter"])
peil_secundair_winter = xr.open_dataarray(path_input["peil_secundair_winter"])
peil_tertiair_winter = xr.open_dataarray(path_input["peil_tertiair_winter"])
peil_primair_zomer = xr.open_dataarray(path_input["peil_primair_zomer"])
peil_secundair_zomer = xr.open_dataarray(path_input["peil_secundair_zomer"])
peil_tertiair_zomer = xr.open_dataarray(path_input["peil_tertiair_zomer"])

lswpeil_winter = xr.where(
    peil_primair_winter.notnull(), peil_primair_winter, peil_secundair_winter
)
lswpeil_winter = lswpeil_winter.rename('peil_winter')

lswpeil_zomer = xr.where(
    peil_primair_zomer.notnull(), peil_primair_zomer, peil_secundair_zomer
)
lswpeil_zomer = lswpeil_zomer.rename('peil_zomer')

# Vind de waardes per LSW. Neem het gemiddelde voor zout & peil per LSW en
# de som van wateroppervlak en stedelijk oppervlak per LSW.

LSW_peil = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["LSW_kaart"],
        column="LSWFINAL",
        raster=lswpeil_winter,
        resolution=25,
        method='mean',
        chunksize=7000
    ) 
# Due to the use of chunks, one polygon can be split into two
# Sort by area and use the stage of the chunk with the largest area
LSW_peil = LSW_peil.set_index('LSWFINAL')
LSW_peil = LSW_peil.sort_values(by=['LSWFINAL', 'area'])
LSW_peil = LSW_peil.loc[~LSW_peil.index.duplicated(keep='last')]
LSW_peil = LSW_peil.rename(columns={'peil_winter':'LSW_peil'})

LSW_peil_zomer = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["LSW_kaart"],
        column="LSWFINAL",
        raster=lswpeil_zomer,
        resolution=25,
        method='mean',
        chunksize=7000
    ) 
# Due to the use of chunks, one polygon can be split into two
# Sort by area and use the stage of the chunk with the largest area
LSW_peil_zomer = LSW_peil_zomer.set_index('LSWFINAL')
LSW_peil_zomer = LSW_peil_zomer.sort_values(by=['LSWFINAL', 'area'])
LSW_peil_zomer = LSW_peil_zomer.loc[~LSW_peil_zomer.index.duplicated(keep='last')]
LSW_peil_zomer = LSW_peil_zomer.rename(columns={'peil_zomer':'LSW_peil_zomer'})

LSW_zout = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["LSW_kaart"],
        column="LSWFINAL",
        raster=zout_water,
        resolution=25,
        method='mean',
        chunksize=7000
    ) 
LSW_zout = LSW_zout.set_index('LSWFINAL')
LSW_zout = LSW_zout.sort_values(by=['LSWFINAL', 'area'])
LSW_zout = LSW_zout.loc[~LSW_zout.index.duplicated(keep='last')]

LSW_wateroppervlak = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["LSW_kaart"],
        column="LSWFINAL",
        raster=wateroppervlak_totaal,
        resolution=25,
        method='sum',
        chunksize=7000
    ) 
LSW_wateroppervlak = LSW_wateroppervlak.set_index('LSWFINAL')
LSW_wateroppervlak = LSW_wateroppervlak.sort_values(by=['LSWFINAL', 'area'])
LSW_wateroppervlak = LSW_wateroppervlak.loc[~LSW_wateroppervlak.index.duplicated(keep='last')]

LSW_stedelijk = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["LSW_kaart"],
        column="LSWFINAL",
        raster=verhard_opp,
        resolution=25,
        method='sum',
        chunksize=7000
    )
LSW_stedelijk = LSW_stedelijk.set_index('LSWFINAL')
LSW_stedelijk = LSW_stedelijk.sort_values(by=['LSWFINAL', 'area'])
LSW_stedelijk = LSW_stedelijk.loc[~LSW_stedelijk.index.duplicated(keep='last')]

LSW_data = pd.concat([
    LSW_peil['LSW_peil'],
    LSW_peil_zomer['LSW_peil_zomer'],
    LSW_zout['zout_water'],
    LSW_wateroppervlak['wateroppervlak_totaal'],
    LSW_stedelijk['stedelijk'],
    LSW_type['TYPEint']], axis='columns'
)
LSW_data['LSW_ID'] = LSW_data.index # LSWattr.csv bevat een extra kolom met de LSWcodes

WA_peil = imod.prepare.spatial.zonal_aggregate_raster(
        path = path_input["WA_kaart"],
        column="WACODE",
        raster=lswpeil_winter,
        resolution=25,
        method='mean',
        chunksize=7000
    )
WA_peil = WA_peil.set_index("WACODE")
WA_peil = WA_peil.sort_values(by=["WACODE", 'area'])
WA_peil = WA_peil.loc[~WA_peil.index.duplicated(keep='last')]
WA_peil = WA_peil.rename(columns={'peil_winter':'WA_peil'})

# Maak twee grids aan waarbij de LSW en WA peilen op de juiste locatie staan:
WA_peil_at_loc = xr.full_like(wa_grid, np.nan)
for WA in WA_kaart["WACODE"]:
    if WA in WA_peil.index:
        WA_peil_at_loc = xr.where(
            wa_grid == WA, WA_peil.at[WA, "WA_peil"], WA_peil_at_loc
        )

LSW_peil_at_loc = xr.full_like(lsw_grid, np.nan)
for LSW in LSW_kaart["LSWFINAL"]:
    if LSW in LSW_data.index:
        LSW_peil_at_loc = xr.where(
            lsw_grid == LSW, LSW_data.at[LSW, "LSW_peil"], LSW_peil_at_loc
        )

# Combineer de WA en LSW peilen tot 1 kaart:
WALSW_peil = xr.where((lsw_type_grid ==2) & (WA_peil_at_loc.notnull()), WA_peil_at_loc, LSW_peil_at_loc)

# Maak een peilcorrectiekaart, waar het verschil tussen de peilen van het regionale
# watersysteem (primair, secundair, tertiar) en de waarde van WALSW_peil bepaald wordt

peil_winter = xr.where(peil_primair_winter.notnull(), peil_primair_winter, np.nan)
peil_winter = peil_winter.where(peil_winter.notnull(), peil_secundair_winter)
fill_tertiair = imod.prepare.fill(peil_winter)
peil_winter = xr.where((peil_tertiair_winter.notnull())&(peil_winter.isnull()), fill_tertiair, peil_winter)
peil_correctie_winter = peil_winter - WALSW_peil

peil_zomer = xr.where(peil_primair_zomer.notnull(), peil_primair_zomer, np.nan)
peil_zomer = peil_zomer.where(peil_zomer.notnull(), peil_secundair_zomer)
fill_tertiair = imod.prepare.fill(peil_zomer)
peil_zomer = xr.where((peil_tertiair_zomer.notnull())&(peil_zomer.isnull()), fill_tertiair, peil_zomer)
peil_correctie_zomer = peil_zomer - WALSW_peil

# WALSW bevat een unieke combo van LSW & WA.
# Gebruik overlay (en niet sjoin) om nieuwe geometriën te maken van de overlappende shapes
# Doordat de kaarten niet perfect aansluiten bevat WALSW 114772 unieke combinaties
WALSW = gpd.overlay(WA_kaart, LSW_kaart, how="intersection")

# Daarom: drop_duplicates op basis van dubbel voorkomende WA's,
# zodat er altijd maar 1 combi van WA & LSW is
# Hiervoor wordt eerst het oppervlak bepaald, zodat het gebied met de grootste
# overlap overblijft.
WALSW["area"] = WALSW.geometry.area
WALSW = WALSW.sort_values(by=["WACODE", "area"], ascending=False)
WALSW = WALSW.drop_duplicates(subset=["WACODE"])

# Koppel de WALSW tabel aan de WA peilen.
# Alle WA's waarvoor geen peil bekend is, worden verwijderd.
WALSW = WALSW.rename(columns={"WACODE": "WA_ID"})
WALSW = WALSW.set_index("WA_ID")
WAattr = pd.concat([WALSW, WA_peil], axis="columns").dropna(subset = ['WA_peil'])

# Gebruik LSW ID als index, dit schrijft uiteindelijk makkelijk weg naar WAattr.csv
WAattr["WA_ID"] = WAattr.index.astype(int)
WAattr.index = WAattr["LSWFINAL"].astype(int)

# Schrijf de data weg naar csv's
# MFTOLSW.CSV
irow = xr.full_like(like_groveschaal, 0.).rename('row')
icol = xr.full_like(like_groveschaal, 0.).rename('col')
for rownr in range(0, 1300):
    irow[rownr] = rownr+1
icol[:] = list(range(1,1201))

lsw_grid = lsw_grid.rename('LSWNUM')
wa_grid = wa_grid.fillna(0)
wa_grid = wa_grid.rename('PV')
peil_correctie_winter = peil_correctie_winter.rename('oppw.correctie')
peil_correctie_zomer = peil_correctie_zomer.rename('oppz.correctie')

icol = icol.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
irow = irow.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
lsw = lsw_grid.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
wa = wa_grid.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
peil_correctie_winter = peil_correctie_winter.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
peil_correctie_zomer = peil_correctie_zomer.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')

mftolsw = pd.concat([icol,irow,lsw, wa,peil_correctie_winter],axis='columns')
mftolsw = mftolsw.dropna(subset=['LSWNUM']).fillna({"oppw.correctie": 0.0})
mftolsw[['col', 'row','LSWNUM', 'PV']] = mftolsw[['col', 'row','LSWNUM','PV']].astype(int)
mftolsw.to_csv(path_output["MFtoLSW"], index=False, columns=['col', 'row', 'LSWNUM', 'PV', 'oppw.correctie'])

# MFTOLSW_zomer.CSV
mftolsw_zomer = pd.concat([icol,irow,lsw, wa,peil_correctie_zomer],axis='columns')
mftolsw_zomer = mftolsw_zomer.dropna(subset=['LSWNUM']).fillna({"oppz.correctie": 0.0})
mftolsw_zomer[['col', 'row','LSWNUM', 'PV']] = mftolsw_zomer[['col', 'row','LSWNUM','PV']].astype(int)
mftolsw_zomer.to_csv(path_output["MFtoLSW_zomer"], index=False,columns=['col', 'row', 'LSWNUM', 'PV', 'oppz.correctie'])

# PLOTTOLSW.CSV
ID_gridcel_metaswap = ID_gridcel_metaswap.to_dataframe().reset_index().drop(['y', 'x'],axis='columns')
plottolsw = pd.concat([ID_gridcel_metaswap, lsw], axis='columns')
plottolsw = plottolsw.replace(0.0,np.nan).dropna()
plottolsw[['metaswap_ID','LSWNUM']] = plottolsw[['metaswap_ID','LSWNUM']].astype(int)
plottolsw.to_csv(path_output["PlottoLSW"], index=False, columns=['metaswap_ID', 'LSWNUM'], header=['ID', 'Majority'])

# LSWattr.CSV:
# LSW peil in dit bestand moet in integerwaardes, oftewel:
# *1000, zonder decimalen
LSW_data = LSW_data.dropna() # Sommige LSW's hebben geen peil, haal deze uit de lijst
LSW_data['peilcorrectie'] = LSW_data['LSW_peil_zomer'] - LSW_data['LSW_peil']
LSW_data['LSW_peil'] = LSW_data['LSW_peil'] * 1000.
LSW_data['LSW_peil_zomer'] = LSW_data['LSW_peil_zomer'] * 1000.


#correctie voor oppervlaktewisseling van 62500m2 naar 625m2 per cel 
LSW_data['wateroppervlak_totaal'] = LSW_data['wateroppervlak_totaal'] / 100.
LSW_data['stedelijk'] = LSW_data['stedelijk'] / 100

LSW_data[["LSW_peil", "wateroppervlak_totaal", "zout_water", "LSW_ID", "stedelijk"]] = LSW_data[["LSW_peil", "wateroppervlak_totaal", "zout_water", "LSW_ID", "stedelijk"]].astype(int)

LSW_data.to_csv(
    path_output["LSWattr"],
    header=False,
    columns=["LSW_peil", "wateroppervlak_totaal", "zout_water", "LSW_ID", "stedelijk"],
)

# WAattr.CSV:
# LSW peil in dit bestand moet in integerwaardes, oftewel:
# *1000, zonder decimalen
WAattr['WA_peil'] = (WAattr['WA_peil'] * 1000.).astype(int)
WAattr = WAattr.sort_values(by=["WA_ID"], ascending=True)

WAattr.to_csv(path_output["WAattr"], header=False, columns=["WA_ID", "WA_peil"])



# LSW_peilcorrectie
LSW_data_peilopzet = LSW_data[LSW_data['TYPEint'] == 1]
LSW_data_peilopzet.to_csv(
    path_output["LSW_peilopzet"],
    header=True,
    columns=["LSW_ID", "peilcorrectie"],
)