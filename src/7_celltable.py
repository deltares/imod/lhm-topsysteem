"""
In dit script worden de peilgestuurde rasters aangemaakt van de volgende parameters (primair, secundair, tertiair):
oppervlakte, breedte, lengte, zomer- & winterpeil, bodemhoogte

Er wordt gebruik gemaakt van een celltable, waarmee de data geaggregeerd kan worden wanneer er meerdere waterdelen in dezelfde cel liggen.
Dit wordt toegepast voor de paramters: oppervlakte, breedte, zomer- & winterpeil

De lengte wordt bepaald als oppervlak/breedte
De bodemhoogte wordt bepaald op basis van het peil

"""
import imod 
import geopandas as gpd 
import pandas as pd 
import numpy as np
import xarray as xr 
import scipy.stats
from dask.diagnostics import ProgressBar

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# Put the coords of the submodel:
xmin_NL = 0.
xmax_NL = 300000.
ymin_NL = 300000.0
ymax_NL = 625000.0

xmin = xmin_NL
xmax = xmax_NL
ymin = ymin_NL
ymax = ymax_NL

for sloottype in ['primair','secundair', 'tertiair']:  
    print(sloottype)

    pad_gpkg = path_input[f'{sloottype}_peilgestuurd']
    pad_peil = path_input[f'peil_bodemhoogte_{sloottype}']

    # Maak like van specifiek gebied:
    dx, xmin, xmax, dy, ymin, ymax = (25.0, xmin, xmax, -25.0, ymin, ymax)
    dims = ("y", "x")
    coords = {
        "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
        "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
    }
    nrow = coords["y"].size
    ncol = coords["x"].size
    like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

    print('Prepare celltable')
    with ProgressBar():
        celltable = imod.prepare.celltable(
                path=pad_gpkg,
                column="lokaalid",
                resolution=1.0,
                like=like_25m,
                chunksize=5000
            )
    celltable = celltable.set_index('lokaalid')
    celltable.to_csv(path_output[f'celltable_{sloottype}'])
    peil = pd.read_csv(pad_peil, index_col='lokaalid', usecols=['lokaalid','breedte','waterdiepte', 'peil_zomer', 'peil_winter'])
    
    print('Merge params')
    celltable = celltable.merge(peil, how='left', on='lokaalid')
    celltable = celltable.sort_values(by=['row_index', 'col_index'])
    if sloottype == 'primair':
        # Primair bevat een aantal polygonen die oorspronkelijk vlakken waren in Top10
        # De breedte van deze waterdelen is oppervlak / celoppervlak
        celltable['breedte'] = celltable['breedte'].where(celltable['breedte'].notnull(), (celltable['area']/25.))


    print('Groupby params')
    celltable_grouped = celltable.groupby(['row_index', 'col_index']).agg({
        "area": "sum",
        'waterdiepte':'mean',
        'breedte': scipy.stats.mode,
        'peil_zomer': scipy.stats.mode,
        'peil_winter': scipy.stats.mode
    })
    

    # pd.Series.mode kan multiple modi vinden, selecteer standaard de eerste:
    celltable_grouped['breedte']= celltable_grouped['breedte'].apply(lambda row: row[0][0])
    celltable_grouped['peil_zomer']= celltable_grouped['peil_zomer'].apply(lambda row: row[0][0])
    celltable_grouped['peil_winter']= celltable_grouped['peil_winter'].apply(lambda row: row[0][0])

    # Bepaal bodemhoogte op basis van de peilen (Zie documentatie voor rekenregels)
    if sloottype == 'primair' or sloottype == 'secundair':
        celltable_grouped['bodemhoogte_zomer'] = celltable_grouped['peil_zomer'] - celltable_grouped['waterdiepte']
        celltable_grouped['bodemhoogte_winter'] = celltable_grouped['peil_winter'] - celltable_grouped['waterdiepte']
    else:
        celltable_grouped['bodemhoogte_zomer'] = celltable_grouped['peil_zomer'] 
        celltable_grouped['bodemhoogte_winter'] = celltable_grouped['peil_winter']

    # Bepaal lengte als oppervlak/breedte
    celltable_grouped = celltable_grouped.reset_index()
    celltable_grouped['lengte'] = celltable_grouped['area'] / celltable_grouped['breedte']
    celltable_grouped.to_csv(path_output[f'celltable_grouped_{sloottype}'])
    
    for param in list(celltable_grouped.columns):
        if param == 'row_index' or param == 'col_index':
            continue
        print(f'write {sloottype} {param}')
        raster = imod.prepare.rasterize_celltable(
            like = like_25m,
            table = celltable_grouped,
            column = param
        )

        raster.to_netcdf(path_output[f'{sloottype}_{param}_peilgestuurd'])

