import subprocess
from pathlib import Path


input_files = snakemake.input
cwd = Path(snakemake.output[0]).parent

#%% Input files
arg_order = [
    "exe",
    "prm",
    "dw",
    "dwvalue",
    "lsw",
    "lswvalue",
    "ladvalue",
    "lswrouting",
    "uslsw",
    "uslswdem",
    "weirarea",
    "vadvalue",
    "vlvalue",
    "lswattr",
    "waattr",
]

args_ls = [input_files[arg] for arg in arg_order]

subprocess.run(args_ls, cwd=cwd)