'''
In dit script worden de volgende stappen gezet:
- split the dataset into primair, secundair, tertiair.
- Ken de Massop parameters ('drooglegging', 'breedte', 'bodemdiepte', 'waterdiepte') toe aan de lijn op basis van breedte klasse (pri, sec, tert) en hydrotype
- Buffer de lijnen met de breedte.
- Ken de Massop parameters toe aan de vlakken op basis van hydrotype
- Combineer de vlakken & primaire lijnen tot 1 bestand
- Splits de bestanden in peilgestuurd & vrijafwaterend

'''

import subprocess # Package to run command line
import os 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

# Split the waterdelen based on the TDNCODE into primary, secondary, tertiary
print('buffer sloten')

sloottypes = ['tertiair', 'secundair', 'primair']
tdncodes = ['600', '601', '602']

for nr in range(0,3):
    sloottype = sloottypes[nr]
    tdncode = tdncodes[nr]
    print(sloottype)
    process = subprocess.Popen(
        args=( 
            'ogr2ogr '
            #f'{sloottype}.gpkg -nln {sloottype} '# Uitvoer
            f'{path_output[f"{sloottype}"]} -nln {sloottype} '# Uitvoer
            f'{path_input["slootGebied_GT_hydrotype"]} slootGebied_GT_hydrotype '# Invoer
            '-dialect sqlite '
            f'-sql "SELECT slootGebied_GT_hydrotype.* from slootGebied_GT_hydrotype WHERE slootGebied_GT_hydrotype.tdncode = {tdncode}"'
        ),
    )
    stdout, stderr = process.communicate()
     
    # Add params (breedte, drooglegging, bodemdiepte) based on hydrotype, make sure that the hydrotype numbers are both FLOATS in csv & gpkg
    process = subprocess.Popen(
        args=( 
            'sqlite3 '
            f'{path_output[f"{sloottype}"]} '
            #f'--cmd ".mode csv" ".import ../{sloottype}_params_hydrotype.csv {sloottype}_params_hydrotype"'
            f'--cmd ".mode csv" ".import {path_input[f"params_{sloottype}"]} {sloottype}_params_hydrotype"'
        ),
    )
    stdout, stderr = process.communicate()
    
    process = subprocess.Popen(
        args=( 
            'ogr2ogr -f GPKG '
            f'{path_output[f"{sloottype}_params"]} -nln {sloottype}_params '# Uitvoer
            f'{path_output[f"{sloottype}"]} '# Invoer
            f'-sql "SELECT shp.*, csv.* FROM {sloottype} shp JOIN {sloottype}_params_hydrotype csv ON shp.hydrotype2 = csv.hydrotype2"'
        ),
    )
    stdout, stderr = process.communicate()
    
    # Add params (drooglegging, scheidingsteken) based on GT, make sure that the hydrotype numbers are both FLOATS in csv & gpkg
    if nr == 0:
        print('GT')
        process = subprocess.Popen(
            args=( 
                'sqlite3 '
                f'{path_output[f"{sloottype}_params"]} '
                #f'--cmd ".mode csv" ".import ../{sloottype}_params_hydrotype.csv {sloottype}_params_hydrotype"'
                f'--cmd ".mode csv" ".import {path_input[f"params_{sloottype}_GT"]} {sloottype}_params_GT "'
                ),
            )
        stdout, stderr = process.communicate()
    
        process = subprocess.Popen(
            args=( 
                'ogr2ogr -f GPKG '
                 f'{path_output[f"{sloottype}_params_all"]} -nln {sloottype}_params_all '# Uitvoer
                 f'{path_output[f"{sloottype}_params"]} '# Invoer
                 f'-sql "SELECT shp.*, csv.* FROM {sloottype}_params shp JOIN {sloottype}_params_GT csv ON shp.GT_num = csv.GT_num"'
            ),
        )
        stdout, stderr = process.communicate()

        # buffer de lijnen met de breedte    
        process = subprocess.Popen(
            args=( 
                'ogr2ogr '
                f'{path_output[f"{sloottype}_buffer"]} -nln {sloottype}_buffer '# Uitvoer
                f'{path_output[f"{sloottype}_params_all"]} {sloottype}_params_all '# Invoer
                '-dialect sqlite '
                f'-sql "select ST_buffer(geom, breedte * 0.5) as geom, lokaalid, LSWtype, breedte, drooglegging, bodemdiepte FROM {sloottype}_params_all"'
                ),
            )
        stdout, stderr = process.communicate()
        print('buffer done')
        
        
    else:
        process = subprocess.Popen(
            args=( 
                'ogr2ogr '
                f'{path_output[f"{sloottype}_buffer"]} -nln {sloottype}_buffer '# Uitvoer
                f'{path_output[f"{sloottype}_params"]} {sloottype}_params '# Invoer
                '-dialect sqlite '
                f'-sql "select ST_buffer(geom, breedte * 0.5) as geom, lokaalid, LSWtype, breedte, drooglegging, bodemdiepte, waterdiepte FROM {sloottype}_params"'
                ),
            )
        stdout, stderr = process.communicate() 

## Vlakken
print('Hydrotype vlakken')

process = subprocess.Popen(
    args=( 
        'ogr2ogr '
        f'{path_output["primair_vlakken"]} -nln primair_vlakken '# Uitvoer
        f'{path_input["slootGebied_vlakken_hydrotype"]} slootGebied_vlakken_hydrotype '# Invoer
        '-dialect sqlite '
        f'-sql "SELECT slootGebied_vlakken_hydrotype.* from slootGebied_vlakken_hydrotype WHERE slootGebied_vlakken_hydrotype.tdncode = 611"'
    ),
)
stdout, stderr = process.communicate()

# Add params (breedte, drooglegging, bodemdiepte) based on hydrotype, make sure that the hydrotype numbers are both FLOATS in csv & gpkg
process = subprocess.Popen(
    args=( 
        'sqlite3 '
        f'{path_output["primair_vlakken"]} '
        f'--cmd ".mode csv" ".import {path_input["params_primair_vlakken"]} primair_vlakken_params_hydrotype"'
    ),
)
stdout, stderr = process.communicate()

process = subprocess.Popen(
    args=( 
        'ogr2ogr -f GPKG '
        f'{path_output["primair_vlakken_params"]} -nln primair_vlakken_params '# Uitvoer
        f'{path_output["primair_vlakken"]}  '# Invoer
        '-sql "SELECT shp.*, csv.* FROM primair_vlakken shp JOIN primair_vlakken_params_hydrotype csv ON shp.hydrotype2 = csv.hydrotype2"'
    ),
)
stdout, stderr = process.communicate()

# Add vlakken to primair:
print('Merge vlakken & primair')
process = subprocess.Popen(
    args=(
        'ogr2ogr '
        f'{path_output["primair_buffer"]} -append -nln primair_buffer '
        f'{path_output["primair_vlakken_params"]} primair_vlakken_params'
    ),
)
stdout, stderr = process.communicate()

print('knip peilgestuurd en vrijafwaterend')
#Knip op in peilgstuurd en vrijafwaterend:
for sloottype in ['primair', 'secundair', 'tertiair']:
    process = subprocess.Popen(
        args=(
            'ogr2ogr '
            #f'{sloottype}_peilgestuurd.gpkg -nln {sloottype}_peilgestuurd '
            f'{path_output[f"{sloottype}_peilgestuurd"]} -nln {sloottype}_peilgestuurd '
            f'{path_output[f"{sloottype}_buffer"]} {sloottype}_buffer '
            '-dialect sqlite '
            f'-sql "SELECT {sloottype}_buffer.* from {sloottype}_buffer WHERE {sloottype}_buffer.LSWtype != 2.0"'
        ),
    )
    stdout, stderr = process.communicate()

    process = subprocess.Popen(
        args=(
        'ogr2ogr '
        f'{path_output[f"{sloottype}_vrijafwater"]} -nln {sloottype}_vrijafwater '
        f'{path_output[f"{sloottype}_buffer"]} {sloottype}_buffer '
        '-dialect sqlite '
        f'-sql "SELECT {sloottype}_buffer.* from {sloottype}_buffer WHERE {sloottype}_buffer.LSWtype = 2.0"'
        ),
    )
    stdout, stderr = process.communicate()

