"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

modus_2d_regridder = imod.prepare.Regridder(method="mode")

drooglegging_tabel_gt = pd.read_csv(path_input['drooglegging_GT_type'], index_col='GT_num')
GT_type = imod.idf.open(path_input['GT_karteerkaart'])
GT_model = imod.idf.open(path_input['GT_modelkaart'])
GT_model = modus_2d_regridder.regrid(GT_model, like=GT_type) 
GT_type = GT_type.where(GT_type.notnull(), GT_model)


GT_ontwatering = GT_type
for x in range(1,20):
    GT_ontwatering = xr.where(GT_ontwatering == x, drooglegging_tabel_gt.iloc[x-1, drooglegging_tabel_gt.columns.get_loc('ontwateringspeil')],GT_ontwatering)   
drooglegging_ontwatering = modus_2d_regridder.regrid(GT_ontwatering, like=like_25m)
drooglegging_ontwatering.to_netcdf(path_output['drooglegging_ontwatering'])

GT_zomer = GT_type
for x in range(1,20):
    GT_zomer = xr.where(GT_zomer == x, drooglegging_tabel_gt.iloc[x-1, drooglegging_tabel_gt.columns.get_loc('zomerpeil')],GT_zomer)   
drooglegging_zomerpeil = modus_2d_regridder.regrid(GT_zomer, like=like_25m)
drooglegging_zomerpeil.to_netcdf(path_output['drooglegging_afwatering_zomer'])

GT_winter = GT_type
for x in range(1,20):
    GT_winter = xr.where(GT_winter == x, drooglegging_tabel_gt.iloc[x-1, drooglegging_tabel_gt.columns.get_loc('winterpeil')],GT_winter)   
drooglegging_winterpeil = modus_2d_regridder.regrid(GT_winter, like=like_25m)
drooglegging_winterpeil.to_netcdf(path_output['drooglegging_afwatering_winter'])


hydrotypes = gpd.read_file(path_input['hydrotypen'])
bodemdiepte_tabel = pd.read_csv(path_input['bodemdiepte_hydrotype'], index_col='hydrotype2')
hydrotypes = hydrotypes.dissolve(by='hydrotype2')
bodemdiepte = pd.concat([hydrotypes, bodemdiepte_tabel], axis=1)
bodemdiepte_ontwatering = imod.prepare.rasterize(bodemdiepte, column='ontwatering', like=like_25m)
bodemdiepte_afwatering = imod.prepare.rasterize(bodemdiepte, column='afwatering', like=like_25m)

waterdiepte_afwatering_zomer = bodemdiepte_afwatering - drooglegging_zomerpeil
waterdiepte_afwatering_zomer = waterdiepte_afwatering_zomer.where(waterdiepte_afwatering_zomer>=0,0)
waterdiepte_afwatering_zomer.to_netcdf(path_output['waterdiepte_afwatering_zomer'])

waterdiepte_afwatering_winter = bodemdiepte_afwatering - drooglegging_winterpeil
waterdiepte_afwatering_winter = waterdiepte_afwatering_winter.where(waterdiepte_afwatering_winter>=0,0)
waterdiepte_afwatering_winter.to_netcdf(path_output['waterdiepte_afwatering_winter'])

waterdiepte_ontwatering = bodemdiepte_ontwatering - drooglegging_ontwatering
waterdiepte_ontwatering = waterdiepte_ontwatering.where(waterdiepte_ontwatering>=0,0)
waterdiepte_ontwatering.to_netcdf(path_output['waterdiepte_ontwatering'])