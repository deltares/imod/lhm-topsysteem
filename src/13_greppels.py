"""
De aanpak om de bodemhoogte en weerstand van de greppels te bepalen, is overgenomen uit
LHM veranderingsrapportage 3.2

De greppelafstand is 40m in het Hollands-Utrechtse veenweidegebied en verder 15m
Het invoerbestand (greppel shapefile) bevat de locaties en het percentage maaiveldgreppels. 

Dat wordt vermenigvuldigd met de greppeldichtheid (celbreedte / greppelafstand)
En wordt veremnigvuldigd met het greppeloppervlak (greppelbreedte * cellengte)

De greppelweerstand is 1 dag

De greppelhoogte is 20cm onder maaiveld
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd
import os

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

greppels = gpd.read_file(path_input['greppels'])
veengebied_holland = gpd.read_file(path_input['veengebied_holland'])
ahn = xr.open_dataarray(path_input['AHN'])

dx, xmin, xmax, dy, ymin, ymax = (25.0, 0.0, 300000.0, -25.0, 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

# Benodigde parameters (uit veranderingsrapportage LHM3.2)
# Let op, verschil tussen greppelafstand (Dgreppel) in hollandsutrechtse veenweidegebieden en de rest van NL
Bcell = params['celgrootte_klein'] # cel breedte
Dgreppel_NL = params['greppelafstand_NL']
Dgreppel_veenHolland = params['greppelafstand_veenholland']
greppel_breedte = params['greppel_breedte']
greppel_lengte = params['celgrootte_klein'] #cel lengte

greppel_area_NL = (Bcell / Dgreppel_NL) * greppel_breedte * greppel_lengte
greppel_area_holland = (Bcell / Dgreppel_veenHolland) * greppel_breedte * greppel_lengte

# Vermenigvuldig area met het greppelpercentage (uit greppelshape)
greppels_percentage = imod.prepare.rasterize(greppels, column = 'percmvgrep' , like=like_25m)
veengebied_raster = imod.prepare.rasterize(veengebied_holland, like=like_25m)

greppels_percentage_NL = xr.where(veengebied_raster.isnull(),greppels_percentage, np.nan)
greppels_percentage_holland = xr.where(veengebied_raster.notnull(),greppels_percentage, np.nan)

greppel_oppervlak_NL = greppels_percentage_NL / 100. * greppel_area_NL
greppels_oppervlak_holland = greppels_percentage_holland / 100. * greppel_area_holland


greppels_oppervlak = greppel_oppervlak_NL.where(greppel_oppervlak_NL.notnull(), greppels_oppervlak_holland)
greppels_oppervlak.to_netcdf(path_output['oppervlak_greppels'])

# Greppel weerstand is 30 dagen, dus conductance == oppervlak/30
greppels_conductance = xr.where(greppels_oppervlak.notnull(), greppels_oppervlak / params["greppels_weerstand"], np.nan)
greppels_conductance.to_netcdf(path_output['conductance_greppels'])

# Greppelhoogte is altijd 20 cm - mv
greppels_hoogte = xr.where(greppels_oppervlak.notnull(), (ahn - 0.2), np.nan)
greppels_hoogte.to_netcdf(path_output['bodemhoogte_greppels'])  
