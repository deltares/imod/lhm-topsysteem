
'''
In dit script wordt per sloottype (primair, secundair, tertiair) een tabel aangemaakt 
met de benodigde parameters uit de Massop tabellen (drooglegging, breedte, waterdiepte en bodemdiepte)
'''
import pandas as pd 

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

drooglegging = pd.read_csv(path_input['drooglegging_GTtype'], index_col='GT_num')

tertiair_data = pd.concat([drooglegging['ontwateringspeil']], axis=1)
tertiair_data.columns = ['drooglegging']

tertiair_data.to_csv(path_output['ontwatering_params_GT'])
