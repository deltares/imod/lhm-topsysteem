"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ahn = xr.open_dataarray(path_input['AHN'])
ahnpeil = xr.open_dataarray(path_input['ahnpeil'])
regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer'])
regiopeil_winter = xr.open_dataarray(path_input['regiopeil_winter'])
peilvakken = gpd.read_file(path_input['peilgebieden'])
hydrotypes = gpd.read_file(path_input['hydrotypen'])
peilvakkenpeil_zomer = imod.prepare.rasterize(peilvakken, column='ZOMERPEIL', like=regiopeil_zomer)
peilvakkenpeil_winter = imod.prepare.rasterize(peilvakken, column='WINTERPEIL', like=regiopeil_zomer)

ahnpeil = ahnpeil / 1000. 

hydrotypes = hydrotypes.dissolve(by='hydrotype2')#.set_index('hydrotype2')
drooglegging_tabel = pd.read_csv(path_input['drooglegging_hydrotype'], index_col='hydrotype2')
drooglegging = pd.concat([hydrotypes, drooglegging_tabel], axis=1)
drooglegging_secundair = imod.prepare.rasterize(drooglegging, column='sloot13', like=regiopeil_zomer)
drooglegging_primair = imod.prepare.rasterize(drooglegging, column='sloot36', like=regiopeil_zomer)

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)



#-------------
## Primair
#-------------
primair_vrijafwater = imod.prepare.gdal_rasterize(path_input['primair_vrijafwater'], column='lokaalid', like=like_25m)
ahn_drooglegging_primair = ahn - drooglegging_primair

### Zomerpeil
primair_zomerpeil = xr.full_like(primair_vrijafwater,np.nan)
primair_zomerpeil = regiopeil_zomer.where(regiopeil_zomer.notnull())
primair_zomerpeil = peilvakkenpeil_zomer.where(primair_zomerpeil.isnull(), primair_zomerpeil)
primair_zomerpeil = np.fmax(primair_zomerpeil, ahn_drooglegging_primair) # Gebruik fmax om goed om te gaan met NaN

primair_zomerpeil = primair_zomerpeil.where(primair_vrijafwater.notnull()) # Houd alleen de primair vrijafwater sloten over
primair_zomerpeil.to_netcdf(path_output['primair_peil_zomer_vrijafwater'])

#### Winterpeil
primair_winterpeil = xr.full_like(primair_vrijafwater,np.nan)
primair_winterpeil = regiopeil_winter.where(regiopeil_winter.notnull())
primair_winterpeil = peilvakkenpeil_winter.where(primair_winterpeil.isnull(), primair_winterpeil) 
primair_winterpeil = np.fmax(primair_winterpeil, ahn_drooglegging_primair) # Gebruik fmax om goed om te gaan met NaN

primair_winterpeil = primair_winterpeil.where(primair_vrijafwater.notnull()) # Houd alleen de primair vrijafwater sloten over
primair_winterpeil.to_netcdf(path_output['primair_peil_winter_vrijafwater'])

# -----
## Secundair
# -----
secundair_vrijafwater = imod.prepare.gdal_rasterize(path_input['secundair_vrijafwater'], column='lokaalid', like=like_25m)

ahn_drooglegging_secundair = ahn - drooglegging_secundair

#Zomerpeil
secundair_zomerpeil = np.fmax(peilvakkenpeil_zomer, ahn_drooglegging_secundair)
secundair_zomerpeil = np.fmax(secundair_zomerpeil, ahnpeil)

secundair_zomerpeil = secundair_zomerpeil.where(secundair_vrijafwater.notnull()) # Houd alleen de secundair vrijafwater sloten over
secundair_zomerpeil.to_netcdf(path_output['secundair_peil_zomer_vrijafwater'])

#Winterpeil
secundair_winterpeil = np.fmax(peilvakkenpeil_winter, ahn_drooglegging_secundair)
secundair_winterpeil = np.fmax(secundair_winterpeil, ahnpeil)

secundair_winterpeil = secundair_winterpeil.where(secundair_vrijafwater.notnull()) # Houd alleen de secundair vrijafwater sloten over
secundair_winterpeil.to_netcdf(path_output['secundair_peil_winter_vrijafwater'])

