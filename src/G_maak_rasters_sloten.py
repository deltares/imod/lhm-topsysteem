"""
In dit script worden de rasters voor alle sloten (primair, secundair, tertiair) aangemaakt, 
waarbij het peil & bodemhoogte van vrijafwaterende en peilgestuurde sloten weer worden 
samengevoegd tot één raster. 

Daarvoor worden de rasters van het vrijafwaterende gebied voor de breedte en areaal aangemaakt dmv een celtable van de gebufferde sloten.

Voor de primaire sloten wordt op basis van de DMnetwerk shape 
de HWS waterdelen uit de primaire sloten rasters verwijderd

Als laatste stap wordt het totale wateroppervlak bepaald door de arealen te sommeren. Het oppervlak van HWS wordt bepaald door de 
DM netwerk shape te verrasteren.


"""

import imod 
import geopandas as gpd 
import pandas as pd 
import numpy as np
import xarray as xr 
import scipy.stats
from dask.diagnostics import ProgressBar

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))


# Put the coords of the submodel:
xmin_NL = 0.
xmax_NL = 300000.
ymin_NL = 300000.0
ymax_NL = 625000.0

xmin = xmin_NL
xmax = xmax_NL
ymin = ymin_NL
ymax = ymax_NL

# Maak like van specifiek gebied:
dx, xmin, xmax, dy, ymin, ymax = (25.0, xmin, xmax, -25.0, ymin, ymax)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

# De DMnetwerk shape wordt gebruikt om te bepalen waar HWS aanwezig is:
dm_netwerk = gpd.read_file(path_input['dm_netwerk'])
dm_netwerk_raster = imod.prepare.rasterize(dm_netwerk, like=like_25m, all_touched=True)

for sloottype in ['primair','secundair', 'tertiair']:  
    print(sloottype)
    for param in ['lengte', 'area']:
        raster_totaal = imod.rasterio.open(path_input[f'{sloottype}_{param}'])
        if sloottype == 'primair':
            #raster.to_netcdf(path_output[f'{sloottype}_{param}_HWSoverlap'])
            raster_totaal = xr.where(dm_netwerk_raster.isnull(), raster_totaal, np.nan)

        raster_totaal.to_netcdf(path_output[f'{sloottype}_{param}'])
     
    # Peil en bodemhoogte van peilgestuurd & vrijafwaterend wordt gecombineerd:
    for param in ['bodemhoogte', 'peil']:
        for seizoen in ['zomer', 'winter']:
            if sloottype == 'secundair':
                raster_peilgestuurd_afwatering = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_peilgestuurd_afwatering'])
                raster_vrijafwater_afwatering = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_vrijafwater_afwatering'])
                raster_peilgestuurd_ontwatering = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_peilgestuurd_ontwatering'])
                raster_vrijafwater_ontwatering = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_vrijafwater_ontwatering'])
                raster_peilgestuurd = xr.where(raster_peilgestuurd_afwatering.notnull(), raster_peilgestuurd_afwatering, raster_peilgestuurd_ontwatering)
                raster_vrijafwater = xr.where(raster_vrijafwater_afwatering.notnull(), raster_vrijafwater_afwatering, raster_vrijafwater_ontwatering)
            else:
                raster_peilgestuurd = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_peilgestuurd'])
                raster_vrijafwater = xr.open_dataarray(path_input[f'{sloottype}_{param}_{seizoen}_vrijafwater'])
            raster_totaal = xr.where(raster_vrijafwater.notnull(), raster_vrijafwater, raster_peilgestuurd)
            
            if sloottype == 'primair':
                raster_totaal = xr.where(dm_netwerk_raster.isnull(), raster_totaal, np.nan)

            raster_totaal.to_netcdf(path_output[f'{sloottype}_{param}_{seizoen}'])


# Maak totaal areaal aan door alle arealen per cel te sommeren
areaal_primair = xr.open_dataarray(path_output['primair_area']).fillna(0.)
areaal_secundair = xr.open_dataarray(path_output['secundair_area']).fillna(0.)
areaal_tertiair = xr.open_dataarray(path_output['tertiair_area']).fillna(0.)

# Primair (uit top10) en HWS overlappen gedeeltelijk
# Daar waar geen top10: vul celoppervlak in 
HWS_area_part = xr.where((dm_netwerk_raster.notnull() & areaal_primair.isnull()), params['celoppervlak_klein'], 0.) 

areaal_totaal = areaal_primair + areaal_secundair + areaal_tertiair + HWS_area_part
areaal_totaal = xr.where(areaal_totaal>params['celoppervlak_klein'], params['celoppervlak_klein'], areaal_totaal)

areaal_totaal.to_netcdf(path_output['totaal_areaal'])

