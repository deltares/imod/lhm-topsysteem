"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ahn = xr.open_dataarray(path_input['AHN'])
ahnpeil = xr.open_dataarray(path_input['ahnpeil'])
regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer'])
regiopeil_winter = xr.open_dataarray(path_input['regiopeil_winter'])
peilvakken = gpd.read_file(path_input['peilgebieden'])
peilvakkenpeil_zomer = imod.prepare.rasterize(peilvakken, column='ZOMERPEIL', like=regiopeil_zomer)
peilvakkenpeil_winter = imod.prepare.rasterize(peilvakken, column='WINTERPEIL', like=regiopeil_zomer)
drooglegging_ontwatering = xr.open_dataarray(path_input['drooglegging_ontwatering'])

ahnpeil = ahnpeil / 1000. 
modus_2d_regridder = imod.prepare.Regridder(method="mode")



dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)


# -----
## TERTIAIR PEILGESTUURD
# -----
tertiair_peilgestuurd = xr.open_dataarray(path_input['tertiair_peilgestuurd'])

ahn_drooglegging_ontwatering = ahn - drooglegging_ontwatering



### Zomerpeil
tertiair_zomerpeil = xr.full_like(tertiair_peilgestuurd,np.nan)
tertiair_zomerpeil = np.fmax(peilvakkenpeil_zomer, ahn_drooglegging_ontwatering)
tertiair_zomerpeil = tertiair_zomerpeil.where(tertiair_zomerpeil < ahn, ahn-0.5)
tertiair_zomerpeil = tertiair_zomerpeil.where(tertiair_peilgestuurd.notnull()) # Houd alleen de tertiair peilgestuurd sloten over
tertiair_zomerpeil.to_netcdf(path_output['tertiair_peil_zomer_peilgestuurd'])

#### Winterpeil
tertiair_winterpeil = xr.full_like(tertiair_peilgestuurd,np.nan)
tertiair_winterpeil = np.fmax(peilvakkenpeil_winter, ahn_drooglegging_ontwatering)
tertiair_winterpeil = tertiair_winterpeil.where(tertiair_winterpeil < ahn, ahn-0.5)
tertiair_winterpeil = tertiair_winterpeil.where(tertiair_peilgestuurd.notnull()) # Houd alleen de tertiair peilgestuurd sloten over
tertiair_winterpeil.to_netcdf(path_output['tertiair_peil_winter_peilgestuurd'])



# -----
## TERTIAIR VRIJAFWATER
# -----
tertiair_vrijafwater = tertiair_vrijafwater = xr.open_dataarray(path_input['tertiair_vrijafwater'])

#Zomerpeil
tertiair_zomerpeil = xr.full_like(tertiair_vrijafwater,np.nan)
tertiair_zomerpeil = np.fmax(peilvakkenpeil_zomer, ahn_drooglegging_ontwatering)
tertiair_zomerpeil = tertiair_zomerpeil.where(tertiair_zomerpeil < ahn, ahn-0.5)
tertiair_zomerpeil = tertiair_zomerpeil.where(tertiair_vrijafwater.notnull()) # Houd alleen de tertiair vrijafwater sloten over
tertiair_zomerpeil.to_netcdf(path_output['tertiair_peil_zomer_vrijafwater'])

#Winterpeil
tertiair_winterpeil = xr.full_like(tertiair_vrijafwater,np.nan)
tertiair_winterpeil = np.fmax(peilvakkenpeil_zomer, ahn_drooglegging_ontwatering)
tertiair_winterpeil = tertiair_winterpeil.where(tertiair_winterpeil < ahn, ahn-0.5)
tertiair_winterpeil = tertiair_winterpeil.where(tertiair_vrijafwater.notnull()) # Houd alleen de tertiair vrijafwater sloten over
tertiair_winterpeil.to_netcdf(path_output['tertiair_peil_winter_vrijafwater'])



