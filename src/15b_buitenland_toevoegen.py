# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 13:38:44 2022

@author: meeusen

In dit script worden de kalibratiefactoren die zijn gebruikt om de conductance aan te passen, vermenigvuldigd met de conducatance kaarten

"""

from os import path
import geopandas as gpd
import imod
import numpy as np
import xarray as xr


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ibound = imod.rasterio.open(path_input['ibound'])

# Maak lege array met de grove celgroottes om mee te regridden
dx, xmin, xmax, dy, ymin, ymax = (
    params['celgrootte_groot'],
    0.0,
    300000.0,
   params['celgrootte_groot'] * -1,
    300000.0,
    625000.0,
)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_groveschaal = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)

# Regridder aanmaken
mean_2d_regridder = imod.prepare.Regridder(method="mean")
sum_2d_regridder = imod.prepare.Regridder(method="sum")
mode_2d_regridder = imod.prepare.Regridder(method="mode")

#Ibrahym data peil
ib_peil_primair = imod.idf.open(path_input['IB_peil_gemiddeld'])
ib_peil_secundair = imod.idf.open(path_input['IB_peil_secundair'])
ib_peil_drain = imod.idf.open(path_input['IB_peil_drain'])

ib_peil_kaart = ib_peil_primair.where(
    ib_peil_primair.notnull(),ib_peil_secundair.where(
        ib_peil_secundair.notnull(), ib_peil_drain))
ib_peil_invoer = mean_2d_regridder.regrid(ib_peil_kaart, like_groveschaal)

#Ibrahym bodemhoogte data
ib_bodh_primair = imod.idf.open(path_input['IB_bottom_winter'])
ib_bodh_kaart = ib_bodh_primair.where(
    ib_bodh_primair.notnull(),ib_peil_secundair.where(
        ib_peil_secundair.notnull(), ib_peil_drain))
ib_bodh_invoer = mean_2d_regridder.regrid(ib_bodh_kaart, like_groveschaal)

#Brabant model invoerdata peil
BM_invoer_bodem = imod.rasterio.open(path_input['BM_bodem'])
invalid_data = xr.where(np.isnan(BM_invoer_bodem)|(BM_invoer_bodem<1000),0,1)
BM_invoer_bodem = xr.where(BM_invoer_bodem==0,np.nan,BM_invoer_bodem)
BM_invoer_bodem = imod.prepare.fill(BM_invoer_bodem, invalid=invalid_data)
BM_invoer_bodem = mode_2d_regridder.regrid(BM_invoer_bodem, like_groveschaal)

#Peil berekening, in overlap deel heeft Ibrahym preferentie:
for soort in ['primair', 'secundair','tertiair']:
        for sys in ["zomer", "winter"]: 
            stage = xr.open_dataarray(path_input[f'peil_{soort}_{sys}_herschaald'])
            if soort == 'primair':
                BM_invoer = imod.rasterio.open(path_input[f'BM_peil_{sys}'])
                BM_invoer = xr.where(BM_invoer==0,np.nan,BM_invoer)
                BM_invoer = imod.prepare.fill(BM_invoer, invalid=invalid_data)
                BM_invoer = mode_2d_regridder.regrid(BM_invoer, like_groveschaal)
                
                buitenland_invoer = ib_peil_invoer.where(ib_peil_invoer.notnull(),BM_invoer)
                stage_buitenland = xr.where(ibound != 1, buitenland_invoer, np.nan)
                stage_aangevuld = xr.where(stage.notnull(),stage,stage_buitenland)
                stage_aangevuld.to_netcdf(path_output[f'peil_{soort}_{sys}_buitenland']) 
                
            else:
                stage.to_netcdf(path_output[f'peil_{soort}_{sys}_buitenland'])


#Bodemhoogte toevoeging buitenland data
for soort in ['primair', 'secundair','tertiair']:
        for sys in ["zomer", "winter"]: 
            bodh = xr.open_dataarray(path_input[f'bodemhoogte_{soort}_{sys}_herschaald'])
            if soort == 'primair':
                bodh_buitenland = ib_bodh_invoer.where(ib_bodh_invoer.notnull(), BM_invoer_bodem)
                bodh_aangevuld_2 = xr.where(ibound != 1, bodh_buitenland, np.nan)
                bodh_aangevuld = xr.where(bodh.notnull(),bodh,bodh_aangevuld_2)
                bodh_aangevuld.to_netcdf(path_output[f'bodemhoogte_{soort}_{sys}_buitenland']) 
            else:
                bodh.to_netcdf(path_output[f'bodemhoogte_{soort}_{sys}_buitenland'])
                

ib_cond_gemiddeld = imod.idf.open(path_input['IB_cond_gemiddeld'])
ib_cond_secundair = imod.idf.open(path_input['IB_cond_secundair'])
ib_cond_drain = imod.idf.open(path_input['IB_cond_drain'])

ib_cond = ib_cond_gemiddeld.where(
    ib_cond_gemiddeld.notnull(),ib_cond_secundair.where(
        ib_cond_secundair.notnull(), ib_cond_drain))
ib_cond = sum_2d_regridder.regrid(ib_cond, like_groveschaal)

BM_cond = imod.rasterio.open(path_input['BM_cond'])
BM_cond = xr.where(BM_cond==0,np.nan,BM_cond)
BM_cond = mode_2d_regridder.regrid(BM_cond, like_groveschaal)

### conductance aanvullen
for soort in ['primair', 'secundair','tertiair']:
    cond = imod.idf.open(path_input[f'cond_leakage_{soort}'])
    if soort == 'primair':
        buitenland_cond = ib_cond.where(ib_cond.notnull(),BM_cond)
        cond_aangevuld_2 = xr.where(ibound != 1, buitenland_cond, np.nan)
        cond_aangevuld = xr.where(cond.notnull(),cond,cond_aangevuld_2)
        imod.idf.write(path_output[f'cond_leakage_{soort}_buitenland'], cond_aangevuld) 
        
    else:
        imod.idf.write(path_output[f'cond_leakage_{soort}_buitenland'], cond)
  
       
####SOF buitenland toevoegen
for soort in ['buisdrainage', 'greppels','SOF']:
    bodh_sof = xr.open_dataarray(path_input[f'bodemhoogte_{soort}_herschaald'])
    cond_sof = xr.open_dataarray(path_input[f'conductance_{soort}_herschaald'] )
    if soort == 'SOF':
        bodh_sof_buitenland = imod.idf.open(path_input['bodh_LHM4_0'])
        bodh_sof_aangevuld_2 = xr.where(ibound != 1, bodh_sof_buitenland, np.nan)
        bodh_sof_aangevuld = xr.where(bodh_sof_aangevuld_2.isnull(),bodh_sof,bodh_sof_aangevuld_2)
        bodh_sof_aangevuld.to_netcdf(path_output[f'bodemhoogte_{soort}_buitenland'])
        

        cond_sof_buitenland = imod.idf.open(path_input['cond_LHM4_0'])
        cond_sof_aangevuld_2 = xr.where((ibound != 1) & (cond_sof_buitenland.notnull()), 62500/30, np.nan)
        cond_sof_aangevuld = xr.where(cond_sof_aangevuld_2.isnull(),cond_sof,cond_sof_aangevuld_2)
        cond_sof_aangevuld.to_netcdf(path_output[f'conductance_{soort}_buitenland'])
    else:
        bodh_sof.to_netcdf(path_output[f'bodemhoogte_{soort}_buitenland'])
        cond_sof.to_netcdf(path_output[f'conductance_{soort}_buitenland'])
