"""
In dit script worden de rekenregels toegepast om het peil per rastercel in het peilgestuurdende gebied te bepalen.


Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd


params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ahn = xr.open_dataarray(path_input['AHN'])
ahnpeil = xr.open_dataarray(path_input['ahnpeil'])
regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer'])
regiopeil_winter = xr.open_dataarray(path_input['regiopeil_winter'])
peilvakken = gpd.read_file(path_input['peilgebieden'])
hydrotypes = gpd.read_file(path_input['hydrotypen'])
peilvakkenpeil_zomer = imod.prepare.rasterize(peilvakken, column='ZOMERPEIL', like=regiopeil_zomer)
peilvakkenpeil_winter = imod.prepare.rasterize(peilvakken, column='WINTERPEIL', like=regiopeil_zomer)
drooglegging_afwatering_zomer = xr.open_dataarray(path_input['drooglegging_afwatering_zomer'])
drooglegging_afwatering_winter = xr.open_dataarray(path_input['drooglegging_afwatering_winter'])
drooglegging_ontwatering = xr.open_dataarray(path_input['drooglegging_ontwatering'])
ahnpeil = ahnpeil / 1000. 

hydrotypes = hydrotypes.dissolve(by='hydrotype2')#.set_index('hydrotype2')
scheiding_tabel = pd.read_csv(path_input['scheiding_hydrotype'], index_col='hydrotype2')
scheiding = pd.concat([hydrotypes, scheiding_tabel], axis=1)
scheidingsraster = imod.prepare.rasterize(scheiding, column='scheidingsnummer', like=regiopeil_zomer)

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)



#-------------
## Primair
#-------------
primair_peilgestuurd = xr.open_dataarray(path_input['primair_peilgestuurd'])
ahn_drooglegging_afwatering_zomer = ahn - drooglegging_afwatering_zomer
ahn_drooglegging_afwatering_winter = ahn - drooglegging_afwatering_winter

### Zomerpeil
primair_zomerpeil = xr.full_like(primair_peilgestuurd,np.nan)
primair_zomerpeil = peilvakkenpeil_zomer.where(peilvakkenpeil_zomer.notnull())
primair_zomerpeil = regiopeil_zomer.where(primair_zomerpeil.isnull(), primair_zomerpeil)
primair_zomerpeil = ahn_drooglegging_afwatering_zomer.where(primair_zomerpeil.isnull(), primair_zomerpeil)
primair_zomerpeil = primair_zomerpeil.where(primair_zomerpeil < ahn, ahn-0.5)
primair_zomerpeil = primair_zomerpeil.where(primair_peilgestuurd.notnull()) # Houd alleen de primair peilgestuurd sloten over
primair_zomerpeil.to_netcdf(path_output['primair_peil_zomer_peilgestuurd'])

#### Winterpeil
primair_winterpeil = xr.full_like(primair_peilgestuurd,np.nan)
primair_winterpeil = peilvakkenpeil_winter.where(peilvakkenpeil_winter.notnull())
primair_winterpeil = regiopeil_winter.where(primair_winterpeil.isnull(), primair_winterpeil)
primair_winterpeil = ahn_drooglegging_afwatering_winter.where(primair_winterpeil.isnull(), primair_winterpeil)
primair_winterpeil = primair_winterpeil.where(primair_winterpeil < ahn, ahn-0.5)
primair_winterpeil = primair_winterpeil.where(primair_peilgestuurd.notnull()) # Houd alleen de primair peilgestuurd sloten over
primair_winterpeil.to_netcdf(path_output['primair_peil_winter_peilgestuurd'])
 
# -----
## Secundair
# -----
secundair_peilgestuurd = xr.open_dataarray(path_input['secundair_peilgestuurd'])
ahn_drooglegging_ontwatering = ahn - drooglegging_ontwatering
secundair_peilgestuurd_ontwatering = secundair_peilgestuurd.where(scheidingsraster == 2, np.nan)
secundair_peilgestuurd_afwatering = secundair_peilgestuurd.where(scheidingsraster == 1, np.nan)

# Gebruik fmax om goed om te gaan met NaN

### Zomerpeil afwatering
secundair_zomerpeil = xr.full_like(secundair_peilgestuurd_afwatering,np.nan)
secundair_zomerpeil = peilvakkenpeil_zomer.where(peilvakkenpeil_zomer.notnull())
secundair_zomerpeil = regiopeil_zomer.where(secundair_zomerpeil.isnull(), secundair_zomerpeil)
secundair_zomerpeil = ahn_drooglegging_afwatering_zomer.where(secundair_zomerpeil.isnull(), secundair_zomerpeil)
secundair_zomerpeil = secundair_zomerpeil.where(secundair_zomerpeil < ahn, ahn-0.5)
secundair_zomerpeil = secundair_zomerpeil.where(secundair_peilgestuurd_afwatering.notnull()) # Houd alleen de secundair peilgestuurd sloten over
secundair_zomerpeil.to_netcdf(path_output['secundair_peil_zomer_peilgestuurd_afwatering'])

#### Winterpeil afwatering
secundair_winterpeil = xr.full_like(secundair_peilgestuurd_afwatering,np.nan)
secundair_winterpeil = peilvakkenpeil_winter.where(peilvakkenpeil_winter.notnull())
secundair_winterpeil = regiopeil_winter.where(secundair_winterpeil.isnull(), secundair_winterpeil)
secundair_winterpeil = ahn_drooglegging_afwatering_winter.where(secundair_winterpeil.isnull(), secundair_winterpeil)
secundair_winterpeil = secundair_winterpeil.where(secundair_winterpeil < ahn, ahn-0.5)
secundair_winterpeil = secundair_winterpeil.where(secundair_peilgestuurd_afwatering.notnull()) # Houd alleen de secundair peilgestuurd sloten over
secundair_winterpeil.to_netcdf(path_output['secundair_peil_winter_peilgestuurd_afwatering'])

### Zomerpeil ontwatering
secundair_zomerpeil = xr.full_like(secundair_peilgestuurd_ontwatering,np.nan)
secundair_zomerpeil = peilvakkenpeil_zomer.where(peilvakkenpeil_zomer.notnull())
secundair_zomerpeil = regiopeil_zomer.where(secundair_zomerpeil.isnull(), secundair_zomerpeil)
secundair_zomerpeil = ahn_drooglegging_ontwatering.where(secundair_zomerpeil.isnull(), secundair_zomerpeil)
secundair_zomerpeil = secundair_zomerpeil.where(secundair_zomerpeil < ahn, ahn-0.5)
secundair_zomerpeil = secundair_zomerpeil.where(secundair_peilgestuurd_ontwatering.notnull()) # Houd alleen de secundair peilgestuurd sloten over
secundair_zomerpeil.to_netcdf(path_output['secundair_peil_zomer_peilgestuurd_ontwatering'])

#### Winterpeil ontwatering
secundair_winterpeil = xr.full_like(secundair_peilgestuurd_ontwatering,np.nan)
secundair_winterpeil = peilvakkenpeil_winter.where(peilvakkenpeil_winter.notnull())
secundair_winterpeil = regiopeil_winter.where(secundair_winterpeil.isnull(), secundair_winterpeil)
secundair_winterpeil = ahn_drooglegging_ontwatering.where(secundair_winterpeil.isnull(), secundair_winterpeil)
secundair_winterpeil = secundair_winterpeil.where(secundair_winterpeil < ahn, ahn-0.5)
secundair_winterpeil = secundair_winterpeil.where(secundair_peilgestuurd_ontwatering.notnull()) # Houd alleen de secundair peilgestuurd sloten over
secundair_winterpeil.to_netcdf(path_output['secundair_peil_winter_peilgestuurd_ontwatering'])

