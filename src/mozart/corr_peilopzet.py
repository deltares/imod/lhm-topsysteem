""" 
Script to correct WM records in uslswdem to reflect lsw level control derived from summer/winter level maps (derived in topsysteem)
"""
import pandas as pd
import mozart as mz
from pathlib import Path

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

path_lsw = path_input["lsw"]
path_uslswdem = path_input["uslswdem"]
# path_uslsw = "uslsw.dik" # wordt nieuw gemaakt obv uslswdem
path_peilopzet = path_input["LSW_peilopzet"]
path_uslswdem_new = path_output["uslswdem_new"]
path_uslsw_new = path_output["uslsw_new"]

# read files
lsw = mz.read_mzlsw(path_lsw)
uslswdem = mz.read_mzuslswdemand(path_uslswdem)
uslswdem_cols = uslswdem.columns
peilopzet = pd.read_csv(path_peilopzet, index_col=0)["peilcorrectie"]
# only for P lsws, ignore rest
lsws_p = lsw.loc[lsw["local_surface_water_code_type"] == "P", "local_surface_water_code"]
peilopzet = peilopzet.loc[peilopzet.index.isin(lsws_p)]

# split uslswdem wm / rest 
uslswdem_wm = uslswdem.loc[(uslswdem["usercode"] == "WM") & (uslswdem["local_surface_water_code"].isin(peilopzet.index))]
uslswdem_rest = uslswdem.loc[~uslswdem.index.isin(uslswdem_wm.index)]

# what is current level_rtc?
lsws_levrtc = uslswdem_wm.groupby("local_surface_water_code").max()["level_rtc"]
 
# if already level_rtc, scale to new level_rtc
# NOTE: this leaves the current spreading of level_rtc over multiple decades intact
lsws_withlevrtc = lsws_levrtc.loc[lsws_levrtc > 0.]
lsws_levrtcfact = pd.merge(lsws_withlevrtc, peilopzet, how="inner", left_index=True, right_index=True)
lsws_levrtcfact["factor"] = lsws_levrtcfact["peilcorrectie"] / lsws_levrtcfact["level_rtc"] # factor is new_peilopzet / old_peilopzet
uslswdem_withlevrtc = uslswdem_wm.loc[uslswdem_wm["local_surface_water_code"].isin(lsws_withlevrtc.index)]
uslswdem_withlevrtc = uslswdem_withlevrtc.merge(lsws_levrtcfact["factor"], how="inner", left_on="local_surface_water_code", right_index=True)
uslswdem_withlevrtc.loc[:, "level_rtc"] = uslswdem_withlevrtc.loc[:, "level_rtc"] * uslswdem_withlevrtc.loc[:, "factor"]
uslswdem_withlevrtc = uslswdem_withlevrtc[uslswdem_cols]

# not already a level_rtc? create new uslswdem records to split in winter and summer period
uslswdem_nolevrtc = uslswdem_wm.loc[~uslswdem_wm.index.isin(uslswdem_withlevrtc.index)]
# make sure only 1 current record
uslswdem_nolevrtc = uslswdem_nolevrtc.groupby("local_surface_water_code").first().reset_index()

# set record to 1-jan - 1apr, and create two additional records (apr - oct, oct - dec)
uslswdem_nolevrtc1 = uslswdem_nolevrtc.copy()
uslswdem_nolevrtc2 = uslswdem_nolevrtc.copy()
uslswdem_nolevrtc3 = uslswdem_nolevrtc.copy()
uslswdem_nolevrtc1["time_end"] = uslswdem_nolevrtc1["time_start"] + 300
uslswdem_nolevrtc2["time_end"] = uslswdem_nolevrtc2["time_start"] + 900
uslswdem_nolevrtc3["time_end"] = uslswdem_nolevrtc3["time_start"] + 10000
uslswdem_nolevrtc2["time_start"] = uslswdem_nolevrtc2["time_start"] + 300
uslswdem_nolevrtc3["time_start"] = uslswdem_nolevrtc3["time_start"] + 900

# set level_rtc in 2nd record apr-oct)
uslswdem_nolevrtc2 = uslswdem_nolevrtc2.merge(peilopzet, how="inner", left_on="local_surface_water_code", right_index=True)
uslswdem_nolevrtc2.loc[:,"level_rtc"] = uslswdem_nolevrtc2.loc[:,"peilcorrectie"]
uslswdem_nolevrtc2 = uslswdem_nolevrtc2[uslswdem_cols]

# merge all uslswdem records and sort (lsw, usercode, time)
uslswdem_new = pd.concat([uslswdem_withlevrtc,uslswdem_nolevrtc1,uslswdem_nolevrtc2,uslswdem_nolevrtc3,uslswdem_rest], axis=0, ignore_index=True)
uslswdem_new = uslswdem_new.sort_values(["local_surface_water_code", "usercode", "time_start"]).reset_index(drop=True)

# save new uslswdem and uslsw
mz.write_mzuslswdemand(path_uslswdem_new, uslswdem_new)
uslsw_new = uslswdem_new.groupby(["local_surface_water_code","usercode"]).first().reset_index()[["local_surface_water_code", "usercode"]]
mz.write_mzuslsw(path_uslsw_new, uslsw_new)