"""Script to change LHM schematization on Zeeuwse eilanden"""
import mozart as mz
import numpy as np
import pandas as pd
import geopandas as gpd
from pathlib import Path

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))


path_lsw_shp = path_input["lsws"]  #TODO: is dit meest recente?
path_lbwl_shp = path_input["lbwl_voorzgebied"]
districten_nowateraanvoer = [80, 135, 791, 950, 951, 78, 500] # districten zonder wateraanvoer, 78 is Wadden
districten_lbwl = [791, 950]

# MOZART schematization

path_lsw = path_input["lsw"]
path_ladvalue = path_input["ladvalue"]
path_vadvalue = path_input["vadvalue"]
path_vlvalue = path_input["vlvalue"]
path_weirarea = path_input["weirarea"]
 
path_uslswdem = path_input["uslswdem"]
path_uslsw = path_input["uslsw"]
path_lswattr = path_input["lswattr"]
path_waattr = path_input["waattr"]

# koppelingsbestanden
path_mftolsw = path_input["MFtoLSW"]
# Which lsw's?
# provincie = gpd.read_file(path_provincie)
# zeeland = provincie.loc[provincie.provincien == "Zeeland"]
lbwl = gpd.read_file(path_lbwl_shp)
# dw_shp = gpd.read_file(path_dw_shp)
lsw_shp = gpd.read_file(path_lsw_shp)
# lsws_zeeland = lsw_shp.overlay(zeeland, how="intersection")


# Load Mozart schem
lsw = mz.read_mzlsw(path_lsw)
ladvalue = mz.read_mzladvalue(path_ladvalue)
vadvalue = mz.read_mzvadvalue(path_vadvalue)
vlvalue = mz.read_mzvlvalue(path_vlvalue)
weirarea = mz.read_mzweirarea(path_weirarea)

uslswdem = mz.read_mzuslswdemand(path_uslswdem)
uslsw = mz.read_mzuslsw(path_uslsw)
lswattr = mz.read_mzlswattr(path_lswattr)
waattr = mz.read_mzwaattr(path_waattr)
mftolsw = pd.read_csv(path_mftolsw)

# Change all P lsw's within districten_nowateraanvoer to type V
lswsel = lsw.loc[lsw["districtwatercode"].isin(districten_nowateraanvoer)]
lswsel = lswsel.loc[lswsel["local_surface_water_code_type"] == "P"]

# Zak van Beveland: make them 'W' LSW's, set priority for agriculture to high and WM to low?
lsws_lbwl = gpd.overlay(lsw_shp, lbwl, how="intersection")["LSWFINAL"]
lsws_lbwl = lsw.loc[lsw["local_surface_water_code"].isin(lsws_lbwl) & lsw["districtwatercode"].isin(districten_lbwl)]

# How?
# --> read ladvalue for lsw, output vadvalue and vlvalue, assume one weirarea per lsw
def get_target_lev(lswnr, lsw, lswattr, uslswdem):
    if lswnr in lswattr["local_surface_water_code"].values:
        target_lev = float(lswattr.loc[lswattr.local_surface_water_code == lswnr, "level"]) / 1000.
    else:
        target_lev = float(lsw.loc[lsw.local_surface_water_code == lswnr, "target_level"])
    uslswdemsel = uslswdem.loc[(uslswdem.local_surface_water_code == lswnr) & (uslswdem.usercode == "WM")]
    if len(uslswdemsel):
        target_lev += uslswdemsel["level_rtc"].max()
    return target_lev

def get_wacode(lswnr, weirarea):
    if lswnr not in weirarea["weir_area_code"]:
        return lswnr
    elif lswnr < 1e5 and (lswnr * 10 + 1) not in weirarea["weir_area_code"]:
        return lswnr * 10 + 1
    else:
        # continue numbering from highest wacode...
        return weirarea["weir_area_code"].max() + 1

def corr_ladvlswattr(ladv, lswnr, lsw, lswattr):
    """   dLevelCorr = pLsw->dLswTargetLvl - dNewLevel;
   for(pLADV = pLsw->pLADValueFirst; pLADV != NULL; pLADV = pLADV->next)
   {
      pLADV->dLevel -= dLevelCorr;
   }"""
    targetlev = float(lsw.loc[lsw.local_surface_water_code == lswnr, "target_level"])
    levcorr = 0.
    if lswnr in lswattr["local_surface_water_code"].values:
        newlev = float(lswattr.loc[lswattr.local_surface_water_code == lswnr, "level"]) / 1000.
        levcorr = targetlev - newlev
    ladv = ladv.copy()
    ladv["level"] -= levcorr
    return ladv

def vadv_vlv_from_ladv(ladv, target_lev, wa_code):
    # TODO Maybe: VADV with sort of 'pit' to allow lsw to run dry and stop infiltration/evaporation before water is asked from DW? TEST!
    # 3/3: mozartin stalls on successive Q=0 records
    
    # ensure correct sorting
    ladv = ladv.sort_values("level")
    
    # if not exists: add a record for target_level, interpolate area
    if target_lev not in ladv["level"].values:
        ladv_target = ladv.iloc[-1:].copy()
        ladv_target["level"] = target_lev
        ladv_target["area"] = np.interp(x=target_lev, xp=ladv["level"], fp=ladv["area"])
        ladv = pd.concat((ladv,ladv_target), axis=0, ignore_index=True)
        ladv = ladv.sort_values("level").reset_index(drop=True)
    
    # calculate volume
    dlev = ladv["level"]-ladv["level"].shift(1)
    ladv["volume"] = (dlev * 0.5*ladv["area"]+0.5*ladv["area"].shift(1)).fillna(0).cumsum()
    # ensure no duplicate volumes
    ladv = ladv.drop_duplicates(subset=["volume"])

    # below and up to target_level: convert to vadvalue / vlvalue
    # NO: only take target level value and a zero record below
    # BECAUSE MozartIn can't handle this, it assumes second record is weir level
    # vadv = ladv.loc[ladv["level"] <= target_lev].copy()
    vadv = pd.concat((ladv.iloc[:1].copy(), ladv.loc[ladv["level"] == target_lev].copy()),axis=0,ignore_index=True)
    vadv.loc[0, ["area","discharge","volume"]] = [0.,0.,0.]

    # make sure that level of first rec != level of second rec
    if vadv.loc[0,"level"] == vadv.loc[1,"level"]:
        vadv.loc[0,"level"] = vadv.loc[1,"level"] - 0.1

    # 3/3: add small amount of Q (0.01 mm/d) to not trip Mozartin
    # vadv.loc[:,"discharge"] = vadv["area"].copy() * 0.01 / 1000. / 86400.
    vadv.loc[:,"discharge"] = np.linspace(0.,vadv["area"].max() * 0.01 / 1000. / 86400., len(vadv) )
    
    # add record with high discharge
    highQ = vadv.iloc[-1:].copy()  #-1: to keep a df, not a series
    highQ["discharge"] = 1e6
    highQ["level"] += 1e-3
    highQ["volume"] += 10.
    vadv = pd.concat((vadv,highQ), axis=0, ignore_index=True)
    
    vlv = vadv.rename(columns={"volume":"volume_lsw", "level":"weir_level"})
    vlv["weir_area_code"] = wa_code
    vlv["weir_level_slope"] = 0.
    vlv = vlv[["local_surface_water_code", "weir_area_code", "volume_lsw", "weir_level", "weir_level_slope"]]
    vadv = vadv[["local_surface_water_code", "volume", "area", "discharge"]]
 
    return vadv, vlv

# Loop through all to-be-changed lsws:
for lswnr in lswsel["local_surface_water_code"]:
    print(lswnr)
    # break
    # change lsw to V
    target_lev = get_target_lev(lswnr, lsw, lswattr, uslswdem)
    assert(target_lev is not None)
    wa_code = get_wacode(lswnr, weirarea)
    ladv = ladvalue.loc[ladvalue["local_surface_water_code"] == lswnr]
    ladv = corr_ladvlswattr(ladv, lswnr, lsw, lswattr)
    vadv, vlv = vadv_vlv_from_ladv(ladv, target_lev, wa_code)
    
    # break
    # append weirarea, vadvalue, vlvalue, delete from ladvalue
    vadvalue = pd.concat((vadvalue,vadv), axis=0, ignore_index=True)
    vlvalue = pd.concat((vlvalue,vlv), axis=0, ignore_index=True)
    wa = pd.DataFrame(data=[[wa_code, lswnr]], columns=weirarea.columns)
    weirarea = pd.concat((weirarea,wa), axis=0, ignore_index=True)
    #try:
    #    attrlev = lswattr.loc[lswattr["local_surface_water_code"]==lswnr,"level"].iloc[0]
    #except IndexError:
    attrlev = int(target_lev * 1000)
    wat = pd.DataFrame(data=[[wa_code, lswnr, attrlev]], columns=waattr.columns)
    waattr = pd.concat((waattr,wat), axis=0, ignore_index=True)
    ladvalue = ladvalue.loc[ladvalue["local_surface_water_code"] != lswnr]

    # change type to V
    if lswnr not in lsws_lbwl["local_surface_water_code"].values:
        # not LBWL, type V:
        lsw.loc[lsw["local_surface_water_code"] == lswnr, "local_surface_water_code_type"] = "V"
        # if V: delete records from uslsw and uslswdem (excl A)
        uslsw = uslsw.loc[(uslsw["local_surface_water_code"] != lswnr) | (uslsw["usercode"] == "A")]
        uslswdem = uslswdem.loc[(uslswdem["local_surface_water_code"] != lswnr) | (uslswdem["usercode"] == "A")]
    else:
        # in LBWL, type W
        lsw.loc[lsw["local_surface_water_code"] == lswnr, "local_surface_water_code_type"] = "W"
        # if W: set prio WM to low records from uslsw and uslswdem (excl A), delete flushing
        uslsw = uslsw.loc[(uslsw["local_surface_water_code"] != lswnr) | (uslsw["usercode"].isin(["A","WM"]))]
        uslswdem = uslswdem.loc[(uslswdem["local_surface_water_code"] != lswnr) | (uslswdem["usercode"].isin(["A","WM"]))]
        uslswdem.loc[(uslswdem["local_surface_water_code"] == lswnr) & (uslswdem["usercode"] == "WM"), "priority"] = 9

    # change coupling file: add PV
    mftolsw.loc[mftolsw["LSWNUM"]==lswnr, "PV"] = wa_code

# Done!
path_lsw_out = path_output["lsw_out"]
path_ladvalue_out = path_output["ladvalue_out"]
path_vadvalue_out = path_output["vadvalue_out"]
path_vlvalue_out = path_output["vlvalue_out"]
path_weirarea_out = path_output["weirarea_out"]
path_uslswdem_out = path_output["uslswdem_out"]
path_uslsw_out = path_output["uslsw_out"]

# Write files
mz.write_mzlsw(path_lsw_out, lsw)
mz.write_mzladvalue(path_ladvalue_out, ladvalue)
mz.write_mzvadvalue(path_vadvalue_out, vadvalue)
mz.write_mzvlvalue(path_vlvalue_out, vlvalue)
mz.write_mzweirarea(path_weirarea_out, weirarea)
mz.write_mzuslswdemand(path_uslswdem_out, uslswdem)
mz.write_mzuslsw(path_uslsw_out, uslsw)
waattr.to_csv(path_output["waattr"], index=False, header=False)
mftolsw.to_csv(path_output["MFtoLSW"], index=False)

print(f"LBWL lsw's present in districts: {lsws_lbwl['districtwatercode'].unique()}, alter DM schematisation to allow water allocation")
