#Script part 3 (Vervolg op overzichtstabel_prioriteit_LSW.py
"""
Create Mozart input from prio-tables
author: Joost Delsman
"""

#Importeer benodigde packages
import pandas as pd
import mozart as mz
from pathlib import Path

#Snakemake paden
path_priocsv = snakemake.input.totaal_tabel
path_uslswdem_in = snakemake.input.uslswdem_dik
path_uslsw_out = Path(snakemake.output.uslsw_new)
path_uslswdem_out = snakemake.output.uslswdem_new

#Lees Excel in
priodf = pd.read_csv(path_priocsv, index_col=0)

# mozart file layout
uslsw_cols = [l[1] for l in mz.mzfile_description["uslsw"]]
uslswdem_cols = [l[1] for l in mz.mzfile_description["uslswdemand"]]

# select highest priority for each MZ user
# NEW: already present in 'Hoogste prioritering' column
users = ["WM","A","WF","WFR"]
# prio_cols = {"WM":['Prio peilbeheer natuur', 'Prio peilbeheer veen',
#        'Prio peilbeheer kapitaalintensief', 'Prio peilbeheer glastuinbouw',
#        'Prio peilbeheer overig', 'Hoogste prioritering peilbeheer'],
#              "A":['Prio beregening kapitaalintensief', 'Prio beregening glastuinbouw',
#        'Prio beregening akkerbouw', 'Prio beregening overig', 'Hoogste prioritering beregening'],
#              "WF":['Prio doorspoeling kapitaalintensief',
#        'Prio doorspoeling glastuinbouw', 'Prio doorspoeling stedelijk',
#        'Prio doorspoeling akkerbouw', 'Prio doorspoeling overig', 'Hoogste prioritering doorspoeling']}
prio_cols = {"WM":"Hoogste prioritering peilbeheer",
             "A":"Hoogste prioritering beregening",
             "WF":"Hoogste prioritering doorspoeling"}
prio_cols["WFR"] = prio_cols["WF"]  #flushing return
for user in users:
    priodf[f"prio_{user}"] = priodf[prio_cols[user]]#.max(axis=1) "hoogste prioritering" is already the maximum
    # CHECK: is there a priority assigned to this user for this LSW?
    # if flushing, that is allowed (if no flushing)
    b = priodf[f"prio_{user}"].isnull()
    assert (b.sum() == 0) or (user in ["WF","WFR"]), f"{b.sum()} LSWs do not have records for user {user}"

# 1. create USLSW records
# NOTE: superfluous, better to create consistent with uslswdem (below)
# uslsw = pd.DataFrame(columns=uslsw_cols)
# for user in users:
#     # list lsws with this user
#     sel = priodf[f"prio_{user}"].dropna().copy()
#     sel = sel.reset_index()
#     sel = sel.rename(columns={"lswfinal":uslsw_cols[0]})
#     sel["usercode"] = user
#     uslsw = pd.concat((uslsw,sel[uslsw_cols]),axis=0,ignore_index=True)

# 2: create USLSWDEM records
# FIRST: read existing USLSWDEM to determine flushing demand, level management
uslswdem = mz.read_mzuslswdemand(path_uslswdem_in)

# THEN: join to priodf on lswcode
joined = pd.merge(uslswdem, priodf, left_on="local_surface_water_code", right_index=True)# how="inner")
# AND assign new priorities
for user in users:
    b = (joined["usercode"]==user)&(joined[f"prio_{user}"].notnull())
    joined.loc[b, "priority"] = joined.loc[b, f"prio_{user}"].astype(int)
      
# CHECK: what existing lsw user demands are not assigned a new priority?
# TODO: HOW TO HANDLE THESE? STOP WITH ERROR? For now just warn and output
for user in users:
    b = (joined["usercode"]==user)&(joined[f"prio_{user}"].isnull())
    if b.sum():
        print(f"{b.sum()} original uslswdem {user} records not assigned a new priority")
        joined.loc[b].to_csv(f"uslswdem_noprio_user{user}.csv")
    else:
        print(f"All original uslswdem {user} records are assigned a new priority")

# FINALLY: save uslsw and uslswdem
if not path_uslsw_out.parent.exists():
    path_uslsw_out.parent.mkdir(exist_ok=True,parents=True)
    
mz.write_mzuslswdemand(path_uslswdem_out, joined[uslswdem_cols])
uslsw_from_joined = joined[uslsw_cols].drop_duplicates()
mz.write_mzuslsw(path_uslsw_out, uslsw_from_joined)