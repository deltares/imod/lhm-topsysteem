#Script part 2 (Vervolg op prioriteit_LSW.py)
# author: Esmee mes

#Importeer benodigde packages
import pandas as pd
import pathlib
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

#Snakemake paden
doorspoeling_path = snakemake.input.doorspoeling_tabel
beregening_path = snakemake.input.beregening_tabel
peilbeheer_path = snakemake.input.peilbeheer_tabel
lsw_path = snakemake.input.lsw_RDO
lsw_type_path = snakemake.input.lsw
path_totaal_tabel = snakemake.output.totaal_tabel

#Lees Excel bestanden in waarin ja/nee kolommen zijn opgenomen voor de cutoffs per LSW
doorspoeling = pd.read_csv(doorspoeling_path)
doorspoeling2 = doorspoeling[['lswfinal','Area kapitaalintensief groter dan 250.000', 'Area glastuinbouw groter dan 100.000', 'Percentage stedelijk groter dan 25', 'Percentage akkerbouw groter dan 10', 'doorspoeling overig aanwezig']]
beregening = pd.read_csv(beregening_path)
beregening2 = beregening[['lswfinal','Area kapitaalintensief groter dan 250.000', 'Area glastuinbouw groter dan 100.000', 'Percentage akkerbouw groter dan 10', 'Beregening overig aanwezig']]
peilbeheer = pd.read_csv(peilbeheer_path)
peilbeheer2 = peilbeheer[['lswfinal','Area natuur groter dan 10.000', 'Area veen groter dan 10.000m2 en Percentage veen groter dan 1%', 'Area kapitaalintensief groter dan 250.000', 'Area glastuinbouw groter dan 100.000', 'peilbeheer overig aanwezig']]
lsw = gpd.read_file(lsw_path)
lsw_type = gpd.read_file(lsw_type_path)

#LSWs kunnen toebedeeld zijn aan meerdere RDO's. Deze omzetten dat deze worden toebedeeld aan RDO Noord
lsw["RDO"] = lsw['RDO'].replace(to_replace="RDO-Twentekanalen + RDO-Noord", value="RDO-Noord")
lsw["RDO"] = lsw['RDO'].replace(to_replace="RDO-Gelderland + RDO-Noord", value="RDO-Noord")
lsw["RDO"] = lsw['RDO'].replace(to_replace="RDO-West-Midden + RDO-Noord", value="RDO-Noord")
lsw = lsw.drop_duplicates(subset=["lswfinal"])

#Creëer een lege dataframe waarin alle ja/nee categorieën later worden toegevoegd
overzichtstabel = pd.DataFrame()
overzichtstabel[['lswfinal','RDO']] = lsw[['lswfinal','RDO']]
lsw_type2 = lsw_type.iloc[:, [0,4]]
overzichtstabel = overzichtstabel.merge(lsw_type2, on='lswfinal')

#Voeg ja/nee kolommen toe aan overzichtstabel
overzichtstabel = overzichtstabel.merge(doorspoeling2, on='lswfinal', how='outer')
overzichtstabel = overzichtstabel.merge(beregening2, on='lswfinal', how='outer')
overzichtstabel = overzichtstabel.merge(peilbeheer2, on='lswfinal', how='outer')
#Verander kolomnamen om verwarring tussen categorieën te voorkomen
overzichtstabel.rename(columns={
    'Area kapitaalintensief groter dan 250.000_x':'Doorspoeling Area kapitaalintensief groter dan 250.000',
    'Area glastuinbouw groter dan 100.000_x': 'Doorspoeling Area glastuinbouw groter dan 100.000',
    'Percentage stedelijk groter dan 25': 'Doorspoeling Percentage stedelijk groter dan 25',
    'Percentage akkerbouw groter dan 10_x': 'Doorspoeling Percentage akkerbouw groter dan 10',
    'doorspoeling overig aanwezig': 'Doorspoeling overig aanwezig',
    'Area kapitaalintensief groter dan 250.000_y': 'Beregening Area kapitaalintensief groter dan 250.000',
    'Area glastuinbouw groter dan 100.000_y': 'Beregening Area glastuinbouw groter dan 100.000',
    'Percentage akkerbouw groter dan 10_y': 'Beregening Percentage akkerbouw groter dan 10',
    'Area natuur groter dan 10.000': 'Peilbeheer Area natuur groter dan 10.000',
    'Area veen groter dan 10.000m2 en Percentage veen groter dan 1%': 'Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%',
    'Area kapitaalintensief groter dan 250.000': 'Peilbeheer Area kapitaalintensief groter dan 250.000',
    'Area glastuinbouw groter dan 100.000': 'Peilbeheer Area glastuinbouw groter dan 100.000',
    'peilbeheer overig aanwezig': 'Peilbeheer overig aanwezig'
    }, inplace=True)

#Voeg lege dataframes toe waaraan priortering per categorie kan worden toegevoegd
overzichtstabel['Prio doorspoeling kapitaalintensief'] = '' 
overzichtstabel['Prio doorspoeling glastuinbouw'] = ''
overzichtstabel['Prio doorspoeling stedelijk'] = ''
overzichtstabel['Prio doorspoeling akkerbouw'] = ''
overzichtstabel['Prio doorspoeling overig'] = ''
overzichtstabel['Hoogste prioritering doorspoeling'] = ''
overzichtstabel['Prio beregening kapitaalintensief'] = ''
overzichtstabel['Prio beregening glastuinbouw'] = ''
overzichtstabel['Prio beregening akkerbouw'] = ''
overzichtstabel['Prio beregening overig'] = ''
overzichtstabel['Hoogste prioritering beregening'] = ''
overzichtstabel['Prio peilbeheer natuur'] = ''
overzichtstabel['Prio peilbeheer veen'] = ''
overzichtstabel['Prio peilbeheer kapitaalintensief'] = ''
overzichtstabel['Prio peilbeheer glastuinbouw'] = ''
overzichtstabel['Prio peilbeheer overig'] = ''
overzichtstabel['Hoogste prioritering peilbeheer'] = ''
overzichtstabel['Totale prioritering'] = ''

#Change type from object to numeric
overzichtstabel['Prio doorspoeling kapitaalintensief'] = overzichtstabel['Prio doorspoeling kapitaalintensief'].apply(pd.to_numeric)
overzichtstabel['Prio doorspoeling glastuinbouw'] = overzichtstabel['Prio doorspoeling glastuinbouw'].apply(pd.to_numeric)
overzichtstabel['Prio doorspoeling stedelijk'] = overzichtstabel['Prio doorspoeling stedelijk'].apply(pd.to_numeric)
overzichtstabel['Prio doorspoeling akkerbouw'] = overzichtstabel['Prio doorspoeling akkerbouw'].apply(pd.to_numeric)
overzichtstabel['Prio doorspoeling overig'] = overzichtstabel['Prio doorspoeling overig'].apply(pd.to_numeric)
overzichtstabel['Hoogste prioritering doorspoeling'] = overzichtstabel['Hoogste prioritering doorspoeling'].apply(pd.to_numeric)
overzichtstabel['Prio beregening kapitaalintensief'] = overzichtstabel['Prio beregening kapitaalintensief'].apply(pd.to_numeric)
overzichtstabel['Prio beregening glastuinbouw'] = overzichtstabel['Prio beregening glastuinbouw'].apply(pd.to_numeric)
overzichtstabel['Prio beregening akkerbouw'] = overzichtstabel['Prio beregening akkerbouw'].apply(pd.to_numeric)
overzichtstabel['Prio beregening overig'] = overzichtstabel['Prio beregening overig'].apply(pd.to_numeric)
overzichtstabel['Hoogste prioritering beregening'] = overzichtstabel['Hoogste prioritering beregening'].apply(pd.to_numeric)
overzichtstabel['Prio peilbeheer natuur'] = overzichtstabel['Prio peilbeheer natuur'].apply(pd.to_numeric)
overzichtstabel['Prio peilbeheer veen'] = overzichtstabel['Prio peilbeheer veen'].apply(pd.to_numeric)
overzichtstabel['Prio peilbeheer kapitaalintensief'] = overzichtstabel['Prio peilbeheer kapitaalintensief'].apply(pd.to_numeric)
overzichtstabel['Prio peilbeheer glastuinbouw'] = overzichtstabel['Prio peilbeheer glastuinbouw'].apply(pd.to_numeric)
overzichtstabel['Prio peilbeheer overig'] = overzichtstabel['Prio peilbeheer overig'].apply(pd.to_numeric)
overzichtstabel['Hoogste prioritering peilbeheer'] = overzichtstabel['Hoogste prioritering peilbeheer'].apply(pd.to_numeric)
overzichtstabel['Totale prioritering'] = overzichtstabel['Totale prioritering'].apply(pd.to_numeric)

##HANTEER DEZE PRIORITERING IN HUIDIGE MOZART WAARBIJ ALLEEN PRIORITERING T/M 9 MOGELIJK IS
#Invullen prioritering wanneer LSW in RDO-Noord gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Twentekanalen gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Gelderland gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-West-Midden gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Zuid-Oost gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Zuid-West gelokaliseerd is
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Voeg hoogste prioritering toe voor doorspoeling, beregening, en peilbeheer
overzichtstabel2 = overzichtstabel.iloc[:, 17:23]
overzichtstabel['Hoogste prioritering doorspoeling'] = overzichtstabel2.min(axis=1)
overzichtstabel3 = overzichtstabel.iloc[:, 23:28]
overzichtstabel['Hoogste prioritering beregening'] = overzichtstabel3.min(axis=1)
overzichtstabel4 = overzichtstabel.iloc[:, 28:34]
overzichtstabel['Hoogste prioritering peilbeheer'] = overzichtstabel4.min(axis=1)

#Geef LSW's zonder prioritering een standaard prioritering voor doorspoeling, beregening en peilbeheer )
#Doorspoeling (default categorie = doorspoeling overig)
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
#Beregening (default categorie = beregening overig)
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
#Peilbeheer (default categorie = peilbeheer overig)
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7



##HANTEER DEZE PRIORITERING IN TOEKOMSTIGE MOZART WAARBIJ PRIORITERING T/M 10 MOGELIJK IS

#Invullen prioritering wanneer LSW in RDO-Noord gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Twentekanalen gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Gelderland gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-West-Midden gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Zuid-Oost gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Invullen prioritering wanneer LSW in RDO-Zuid-West gelokaliseerd is
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio doorspoeling kapitaalintensief"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Area glastuinbouw groter dan 100.000"] == 'True'), "Prio doorspoeling glastuinbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Percentage stedelijk groter dan 25"] == 'True'), "Prio doorspoeling stedelijk"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling Percentage akkerbouw groter dan 10"] == 'True'), "Prio doorspoeling akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Doorspoeling overig aanwezig"] == True), "Prio doorspoeling overig"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio beregening kapitaalintensief"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Area glastuinbouw groter dan 100.000"] == 'True'), "Prio beregening glastuinbouw"] = 6
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening Percentage akkerbouw groter dan 10"] == 'True'), "Prio beregening akkerbouw"] = 8
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Beregening overig aanwezig"] == True), "Prio beregening overig"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area natuur groter dan 10.000"] == 'True'), "Prio peilbeheer natuur"] = 3
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area veen groter dan 10.000m2 en Percentage veen groter dan 1%"] == 'True'), "Prio peilbeheer veen"] = 2
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area kapitaalintensief groter dan 250.000"] == 'True'), "Prio peilbeheer kapitaalintensief"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer Area glastuinbouw groter dan 100.000"] == 'True'), "Prio peilbeheer glastuinbouw"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Peilbeheer overig aanwezig"] == True), "Prio peilbeheer overig"] = 7

#Voeg hoogste prioritering toe voor doorspoeling, beregening, en peilbeheer
#overzichtstabel2 = overzichtstabel.iloc[:, 17:23]
#overzichtstabel['Hoogste prioritering doorspoeling'] = overzichtstabel2.min(axis=1)
#overzichtstabel3 = overzichtstabel.iloc[:, 23:28]
#overzichtstabel['Hoogste prioritering beregening'] = overzichtstabel3.min(axis=1)
#overzichtstabel4 = overzichtstabel.iloc[:, 28:34]
#overzichtstabel['Hoogste prioritering peilbeheer'] = overzichtstabel4.min(axis=1)

#Geef LSW's zonder prioritering een standaard prioritering voor doorspoeling, beregening en peilbeheer )
#Doorspoeling (default categorie = doorspoeling overig)
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 10
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering doorspoeling"].isnull()), "Hoogste prioritering doorspoeling"] = 7
#Beregening (default categorie = beregening overig)
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 9
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering beregening"].isnull()), "Hoogste prioritering beregening"] = 7
#Peilbeheer (default categorie = peilbeheer overig)
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Noord') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Twentekanalen') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-West-Midden') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 3)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 1)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-West (Deelgebied 2)') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Gelderland') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7
#overzichtstabel.loc[(overzichtstabel["RDO"] == 'RDO-Zuid-Oost') & (overzichtstabel["Hoogste prioritering peilbeheer"].isnull()), "Hoogste prioritering peilbeheer"] = 7

#Check of alle LSW's een prioritering hebben voor doorspoeling, beregening en peilbeheer
#Doorspoeling
assert (
        (overzichtstabel['Hoogste prioritering doorspoeling'].isna().sum() == 0)
    ), "Er zijn nog lsw's aanwezig zonder prioritering voor doorspoeling"
#Beregening
assert (
        (overzichtstabel['Hoogste prioritering beregening'].isna().sum() == 0)
    ), "Er zijn nog lsw's aanwezig zonder prioritering voor beregening"
#Peilbeheer
assert (
        (overzichtstabel['Hoogste prioritering peilbeheer'].isna().sum() == 0)
    ), "Er zijn nog lsw's aanwezig zonder prioritering voor peilbeheer"

#Hoogste prioritering toevoegen in laatste kolom
overzichtstabel5 = overzichtstabel.iloc[:, 17:34]
overzichtstabel['Totale prioritering'] = overzichtstabel5.min(axis=1)

#Exporteer als csv
overzichtstabel.to_csv(path_totaal_tabel, header=True, index=False, encoding='utf-8')