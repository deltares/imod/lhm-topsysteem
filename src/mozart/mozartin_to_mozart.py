# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 07:40:26 2023

@author: Meeusen_R
"""

import mozart as mz
import pandas as pd

path_uslswdem = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\uslswdem.dik"
path_uslswdem_new = 

uslswdem = mz.read_mzuslswdemand(path_uslswdem)

year_start = 2009
year_end = 2020
year_org = uslswdem.iloc[0]["time_start"] // 10000  # year_start in original uslswdem

uslswdem_new = []
for year in range(year_start, year_end+1):
    uslswdem_y = uslswdem.copy()
    uslswdem_y["time_start"] += (year-year_org) * 10000
    uslswdem_y["time_end"] += (year-year_org) * 10000
    uslswdem_new.append(uslswdem_y)
uslswdem_new = pd.concat(uslswdem_new, axis=0, ignore_index=True)

# sort on lsw, user, time
uslswdem_new = uslswdem_new.sort_values(["local_surface_water_code","usercode","time_start"])

mz.write_mzuslswdemand(path_uslswdem_new, uslswdem_new)



######### LSWvalue, dwvalue en wavalue
path_dwvalue = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\dwvalue.dik"
path_wavalue = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\wavalue.dik"
path_lswvalue = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\lswvalue.dik"
path_dwvalue_new = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\uslswdem.dik"
path_wavalue_new = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\uslswdem.dik"
path_lswvalue_new = r"g:\Projecten\2023\LHM_setup\data\4_output\runs\LHM_standaard2\mozart\uslswdem.dik"

dwvalue = mz.read_mzdwvalue(path_dwvalue)
wavalue = mz.read_mzwavalue(path_wavalue)
lswvalue = mz.read_mzlswvalue(path_lswvalue)

year_new = year_start-1

year_org_dw = dwvalue.iloc[0]["time_start"] // 10000  # year_start in original uslswdem
dwvalue_new = dwvalue.copy
dwvalue_new["time_start"] += (year_new-year_org) * 10000
dwvalue_new["time_end"] += (year_new-year_org) * 10000
mz.write_mzdwvalue(path_dwvalue_new, dwvalue_new)

year_org_wa = wavalue.iloc[0]["time_start"] // 10000  # year_start in original uslswdem
wavalue_new = wavalue.copy
wavalue_new["time_start"] += (year_new-year_org) * 10000
wavalue_new["time_end"] += (year_new-year_org) * 10000
mz.write_mzwavalue(path_wavalue_new, wavalue_new)

year_org_lsw = lswvalue.iloc[0]["time_start"] // 10000  # year_start in original uslswdem
lswvalue_new = lswvalue.copy
lswvalue_new["time_start"] += (year_new-year_org) * 10000
lswvalue_new["time_end"] += (year_new-year_org) * 10000
mz.write_mzlswvalue(path_lswvalue_new, lswvalue_new)
