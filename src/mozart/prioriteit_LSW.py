#Script part 1 (Gevolgd door overzichtstabel_prioriteit_LSW.py)
#author: Esmée Mes

#Importeer benodigde packages
from cmath import nan
import pathlib
from pickle import TRUE
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import xarray as xr
import rasterio as rio
from rasterio.crs import CRS
from osgeo import ogr
from osgeo import gdal
from shapely.geometry import shape
from tabulate import tabulate

#Snakemake paden
bodem_path = snakemake.input.bodem
natuur_path = snakemake.input.natuur
doorspoel_path = snakemake.input.doorspoel
beregening_path = snakemake.input.beregening
lsw_path = snakemake.input.lsw
landgebruik_path = snakemake.input.landgebruik
path_peilbeheer_tabel = snakemake.output.peilbeheer_tabel
path_doorspoeling_tabel = snakemake.output.doorspoeling_tabel
path_beregening_tabel = snakemake.output.beregening_tabel

#Laad de data in
landgebruik = gpd.read_file(landgebruik_path)
bodem = gpd.read_file(bodem_path)
doorspoeling = gpd.read_file(doorspoel_path)
beregening = gpd.read_file(beregening_path)
natuur = gpd.read_file(natuur_path)
lsw = gpd.read_file(lsw_path)

#Creëer een lege dataframe voor de gebruiker peilbeheer en voeg de LSW id's toe. Later kunnen hier kolommen aan worden toegevoegd.
peilbeheer = gpd.GeoDataFrame()
peilbeheer['lswfinal'] = lsw['lswfinal']

###PEILBEHEERST NATUUR###
#Gebieden van lsw die overlappen met natuur, waarbij lsw id bewaard blijft
intersection_lsw_natuur = gpd.overlay(lsw, natuur, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_lsw_natuur2 = intersection_lsw_natuur.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_lsw_natuur3 = intersection_lsw_natuur2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor natuur per LSW
intersection_lsw_natuur3['area_natuur_per_LSW'] = intersection_lsw_natuur3.area
#Voeg area toe van de totale LSW polygonen
lsw['area_LSW_totaal'] = lsw.area
#Voeg totale areaal van de LSWs toe aan de dataframe van de gebruiker
peilbeheer['LSW area'] = lsw['area_LSW_totaal']

#Merge LSW shapefile en natuur shapefile op basis van lswfinal
lsw2 = lsw.merge(intersection_lsw_natuur3, on='lswfinal')
lsw2['Percentage natuur van LSW'] = lsw2['area_natuur_per_LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 1 ha/10.000 m2
lsw2.loc[lsw2["area_natuur_per_LSW"] >= 10000, 'Area natuur groter dan 10.000'] = 'True'
lsw2.loc[lsw2["area_natuur_per_LSW"] < 10000, 'Area natuur groter dan 10.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan peilbeheertabel
natuur_peilbeheer = lsw2.iloc[:, [0,51,53]]
peilbeheer = peilbeheer.merge(natuur_peilbeheer, on='lswfinal', how='outer')

###PEILBEHEERST BODEM### 
#Filter de veengebieden uit de bodemlaag (id tussen 1001 en 1018)
mask = (bodem['BOFEK2020'] >= 1001)  & (bodem['BOFEK2020'] <= 1018) 
bodem_veen = bodem.loc[mask]

#Gebieden van lsw die overlappen met veengebieden, waarbij lsw id bewaard blijft
intersection_lsw_veen = gpd.overlay(lsw, bodem_veen, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_lsw_veen2 = intersection_lsw_veen.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_lsw_veen3 = intersection_lsw_veen2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor veengebieden per LSW
intersection_lsw_veen3['area_veen_per_LSW'] = intersection_lsw_veen3.area

#Merge LSW shapefile en veengebieden shapefile op basis van lswfinal
lsw2 = lsw.merge(intersection_lsw_veen3, on='lswfinal')
lsw2['Percentage veen van LSW'] = lsw2['area_veen_per_LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 1 ha/10.000 m2 & 1%
lsw2.loc[(lsw2["area_veen_per_LSW"] >= 10000) & (lsw2["Percentage veen van LSW"] >= 1), 'Area veen groter dan 10.000m2 en Percentage veen groter dan 1%'] = 'True'
lsw2.loc[(lsw2["area_veen_per_LSW"] < 10000) & (lsw2["Percentage veen van LSW"] < 1), 'Area veen groter dan 10.000m2 en Percentage veen groter dan 1%'] = 'False'

#Behoud interessante kolommen voor toevoegen aan peilbeheertabel
veen_peilbeheer = lsw2.iloc[:, [0,51,52,53]]
peilbeheer = peilbeheer.merge(veen_peilbeheer, on='lswfinal', how='outer')

###WELKE LSWS HEBBEN DOORSPOELING
#Leidt af welke LSWs een doorspoelfunctie hebben
LSWs_met_doorspoeling = doorspoeling.dropna()
LSWs_met_doorspoeling.rename(columns={'LSWFINAL': 'lswfinal'}, inplace=True)
#Voeg de dubbele LSWs in de file samen
LSWs_met_doorspoeling2 = LSWs_met_doorspoeling.dissolve(by='lswfinal', aggfunc='mean', as_index=False)
#Voeg areaal toe van de polygonen in LSW doorspoeling
LSWs_met_doorspoeling2['area_LSW_totaal'] = LSWs_met_doorspoeling2.area
#Maak dataframe voor doorspoeling
doorspoeling_tabel = gpd.GeoDataFrame()
doorspoeling_tabel['lswfinal'] = LSWs_met_doorspoeling2['lswfinal']
doorspoeling_tabel['LSW area'] = LSWs_met_doorspoeling2['area_LSW_totaal']

###WELKE LSWS HEBBEN BEREGENING
#Leidt af welke LSWs beregening hebben
beregening2 = beregening.loc[(beregening['DN'] == 1) | (beregening['DN'] == 2)]
intersection_lsw_beregening = gpd.overlay(beregening2, lsw, how = 'intersection')
intersection_lsw_beregening2 = intersection_lsw_beregening.dissolve(by='lswfinal', aggfunc='sum').reset_index()
#Hoe groot is het areaal aan beregening per LSW?
intersection_lsw_beregening2['area_beregening_per_LSW'] = intersection_lsw_beregening2.area
#Behoud alleen lswfinal en geometry van lsw file
lsw2 = lsw.loc[:, ['lswfinal', "geometry"]]
#Behoud gehele lsw shapefile wanneer deze beregening heeft
lsw3 = lsw2.merge(intersection_lsw_beregening2, on='lswfinal', how='outer')
lsw4 = lsw3.dropna(subset=["geometry_y"])
lsw5 = lsw4.iloc[:, [0,1]]
lsw6 = gpd.GeoDataFrame(lsw5, geometry='geometry_x')
lsw7 = lsw6.dissolve(by='lswfinal', aggfunc='sum').reset_index()
lsw7['area_beregening_per_LSW'] = lsw7.area
#Maak dataframe voor beregening
beregening_tabel = gpd.GeoDataFrame()
beregening_tabel['lswfinal'] = lsw7['lswfinal']
beregening_tabel['Area beregening per LSW'] = intersection_lsw_beregening2['area_beregening_per_LSW']


###KAPITAALINTENSIEVE GEWASSEN

#Lees LSW opnieuw in, want originele versie is hierboven aangepast
lsw = gpd.read_file(lsw_path)
#Voeg areaal toe van de totale LSW polygonen
lsw['area_LSW_totaal'] = lsw.area

#Behoud data met kapitaalintensieve gewassen uit landbouwkaart --> Kapitaalintensieve gewassen bestaan uit: overige landbouwgewassen (6); boomteelt (7); boomgaard (9); bollen (10); fruitkwekerijen (21); glastuinbouw (8)
mask = (landgebruik['DN'] == 6) | (landgebruik['DN'] == 7) | (landgebruik['DN'] == 9) | (landgebruik['DN'] == 10) | (landgebruik['DN'] == 21) | (landgebruik['DN'] == 8) 
kapitaalintensieve_gewassen = landgebruik.loc[mask]

###Kapitaalintensieve gewassen peilbeheerst
#Gebieden van lsw die overlappen met kapitaalintensieve gewassen, waarbij lsw id bewaard blijft
intersection_lsw_kapitaalintensief = gpd.overlay(lsw, kapitaalintensieve_gewassen, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_lsw_kapitaalintensief2 = intersection_lsw_kapitaalintensief.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_lsw_kapitaalintensief3 = intersection_lsw_kapitaalintensief2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor kapitaalintensieve gewassen per LSW
intersection_lsw_kapitaalintensief3['area_kapitaalintensief_per_LSW'] = intersection_lsw_kapitaalintensief3.area

#Voeg LSW shapefile en kapitaalintensieve gewassen shapefile samen op basis van lswfinal
lsw2 = lsw.merge(intersection_lsw_kapitaalintensief3, on='lswfinal')
lsw2['Percentage kapitaalintensief van LSW'] = lsw2['area_kapitaalintensief_per_LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 25 ha/250.000 m2
lsw2.loc[lsw2["area_kapitaalintensief_per_LSW"] >= 250000, 'Area kapitaalintensief groter dan 250.000'] = 'True'
lsw2.loc[lsw2["area_kapitaalintensief_per_LSW"] < 250000, 'Area kapitaalintensief groter dan 250.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan peilbeheertabel
kapint_peilbeheer = lsw2.iloc[:, [0,51,53]]
peilbeheer = peilbeheer.merge(kapint_peilbeheer, on='lswfinal', how='outer')

#Kapitaalintensieve gewassen doorspoeling
#Gebieden van doorgespoelde lsws die overlappen met kapitaalintensieve gewassen, waarbij lsw id bewaard blijft
intersection_doorspoeling_kapitaalintensief = gpd.overlay(LSWs_met_doorspoeling2, kapitaalintensieve_gewassen, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_doorspoeling_kapitaalintensief2 = intersection_doorspoeling_kapitaalintensief.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_doorspoeling_kapitaalintensief3 = intersection_doorspoeling_kapitaalintensief2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor kapitaalintensief per LSW
intersection_doorspoeling_kapitaalintensief3['area_kapitaalintensief_per_doorgespoelde LSW'] = intersection_doorspoeling_kapitaalintensief3.area

#Voeg LSW shapefile en kapitaalintensief shapefile samen op basis van lswfinal
lsw2 = LSWs_met_doorspoeling2.merge(intersection_doorspoeling_kapitaalintensief3, on='lswfinal')
lsw2['Percentage kapitaalintensief van LSW'] = lsw2['area_kapitaalintensief_per_doorgespoelde LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 25 ha/250.000 m2
lsw2.loc[lsw2["area_kapitaalintensief_per_doorgespoelde LSW"] >= 250000, 'Area kapitaalintensief groter dan 250.000'] = 'True'
lsw2.loc[lsw2["area_kapitaalintensief_per_doorgespoelde LSW"] < 250000, 'Area kapitaalintensief groter dan 250.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan doorspoelingtabel
kapint_doorspoeling = lsw2.iloc[:, [0,9,11]]
doorspoeling_tabel = doorspoeling_tabel.merge(kapint_doorspoeling, on='lswfinal', how='outer')

#Kapitaalintensieve gewassen beregening
LSWs_met_beregening2 = intersection_lsw_beregening2   
#Gebieden van doorgespoelde lsws die overlappen met kapitaalintensieve gewassen, waarbij lsw id bewaard blijft
intersection_beregening_kapitaalintensief = gpd.overlay(LSWs_met_beregening2, kapitaalintensieve_gewassen, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_beregening_kapitaalintensief2 = intersection_beregening_kapitaalintensief.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_beregening_kapitaalintensief3 = intersection_beregening_kapitaalintensief2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor kapitaalintensief per LSW
intersection_beregening_kapitaalintensief3['area_kapitaalintensief_per_beregende LSW'] = intersection_beregening_kapitaalintensief3.area

#Voeg LSW shapefile en kapitaalintensief shapefile samen op basis van lswfinal
lsw2 = LSWs_met_beregening2.merge(intersection_beregening_kapitaalintensief3, on='lswfinal')
lsw2['Percentage kapitaalintensief van LSW'] = lsw2['area_kapitaalintensief_per_beregende LSW'] / lsw2['area_beregening_per_LSW']*100
#Cut-off is gezet op 25 ha/250.000 m2
lsw2.loc[lsw2["area_kapitaalintensief_per_beregende LSW"] >= 250000, 'Area kapitaalintensief groter dan 250.000'] = 'True'
lsw2.loc[lsw2["area_kapitaalintensief_per_beregende LSW"] < 250000, 'Area kapitaalintensief groter dan 250.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan beregeningstabel
kapint_beregening = lsw2.iloc[:, [0,26,27,28]]
beregening_tabel = beregening_tabel.merge(kapint_beregening, on='lswfinal', how='outer')

###GLASTUINBOUW###
#Lees LSW opnieuw in
lsw = gpd.read_file(lsw_path)
#Voeg areaal toe van de totale LSW polygonen
lsw['area_LSW_totaal'] = lsw.area

#Behoud data met glastuinbouw uit landbouwkaart
mask = (landgebruik['DN'] == 8)
glastuinbouw = landgebruik.loc[mask]

#Glastuinbouw peilbeheerst
#Gebieden van lsw die overlappen met glastuinbouw, waarbij lsw id bewaard blijft
intersection_peilbeheer_glastuinbouw = gpd.overlay(lsw, glastuinbouw, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_peilbeheer_glastuinbouw2 = intersection_peilbeheer_glastuinbouw.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_peilbeheer_glastuinbouw3 = intersection_peilbeheer_glastuinbouw2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor glastuinbouw per LSW
intersection_peilbeheer_glastuinbouw3['area_glastuinbouw_per_LSW'] = intersection_peilbeheer_glastuinbouw3.area

#Voeg LSW shapefile en glastuinbouw shapefile samen op basis van lswfinal
lsw2 = lsw.merge(intersection_peilbeheer_glastuinbouw3, on='lswfinal')
lsw2['Percentage glastuinbouw van LSW'] = lsw2['area_glastuinbouw_per_LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 10 ha/100.000 m2
lsw2.loc[lsw2["area_glastuinbouw_per_LSW"] >= 100000, 'Area glastuinbouw groter dan 100.000'] = 'True'
lsw2.loc[lsw2["area_glastuinbouw_per_LSW"] < 100000, 'Area glastuinbouw groter dan 100.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan peilbeheertabel
glastuinbouw_peilbeheer = lsw2.iloc[:, [0,51,53]]
peilbeheer = peilbeheer.merge(glastuinbouw_peilbeheer, on='lswfinal', how='outer')

#Glastuinbouw doorspoeling
#Gebieden van doorgespoelde lsw die overlappen met glastuinbouw, waarbij lsw id bewaard blijft
intersection_doorspoeling_glastuinbouw = gpd.overlay(LSWs_met_doorspoeling2, glastuinbouw, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_doorspoeling_glastuinbouw2 = intersection_doorspoeling_glastuinbouw.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_doorspoeling_glastuinbouw3 = intersection_doorspoeling_glastuinbouw2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor glastuinbouw per LSW
intersection_doorspoeling_glastuinbouw3['area_glastuinbouw_per_doorgespoelde LSW'] = intersection_doorspoeling_glastuinbouw3.area

#Voeg LSW shapefile en glastuinbouw shapefile samen op basis van lswfinal
lsw2 = LSWs_met_doorspoeling2.merge(intersection_doorspoeling_glastuinbouw3, on='lswfinal')
lsw2['Percentage glastuinbouw van LSW'] = lsw2['area_glastuinbouw_per_doorgespoelde LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is put on 10 ha/100.000 m2
lsw2.loc[lsw2["area_glastuinbouw_per_doorgespoelde LSW"] >= 100000, 'Area glastuinbouw groter dan 100.000'] = 'True'
lsw2.loc[lsw2["area_glastuinbouw_per_doorgespoelde LSW"] < 100000, 'Area glastuinbouw groter dan 100.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan doorspoelingtabel
glastuinbouw_doorspoeling = lsw2.iloc[:, [0,9,11]]
doorspoeling_tabel = doorspoeling_tabel.merge(glastuinbouw_doorspoeling, on='lswfinal', how='outer')

        #Glastuinbouw beregend
#Gebieden van beregende lsw die overlappen met glastuinbouw, waarbij lsw id bewaard blijft
intersection_beregening_glastuinbouw = gpd.overlay(LSWs_met_beregening2, glastuinbouw, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_beregening_glastuinbouw2 = intersection_beregening_glastuinbouw.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_beregening_glastuinbouw3 = intersection_beregening_glastuinbouw2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor glastuinbouw per LSW
intersection_beregening_glastuinbouw3['area_glastuinbouw_per_beregende LSW'] = intersection_beregening_glastuinbouw3.area

#Voeg LSW shapefile en glastuinbouw shapefile samen op basis van lswfinal
lsw2 = LSWs_met_beregening2.merge(intersection_beregening_glastuinbouw3, on='lswfinal')
lsw2['Percentage glastuinbouw van LSW'] = lsw2['area_glastuinbouw_per_beregende LSW'] / lsw2['area_beregening_per_LSW']*100
#Cut-off is gezet op 10 ha/100.000 m2
lsw2.loc[lsw2["area_glastuinbouw_per_beregende LSW"] >= 100000, 'Area glastuinbouw groter dan 100.000'] = 'True'
lsw2.loc[lsw2["area_glastuinbouw_per_beregende LSW"] < 100000, 'Area glastuinbouw groter dan 100.000'] = 'False'

#Behoud interessante kolommen voor toevoegen aan beregeningtabel
glastuinbouw_beregening = lsw2.iloc[:, [0,26,27,28]]
beregening_tabel = beregening_tabel.merge(glastuinbouw_beregening, on='lswfinal', how='outer')

###STEDELIJK GEBIED
#Doorspoeling stedelijk gebied
#Behoud data met stedelijk gebied uit landbouwkaart
mask = (landgebruik['DN'] == 22) | (landgebruik['DN'] == 18)
stedelijk = landgebruik.loc[mask]

#Gebieden van lsw die overlappen met stedelijk gebied, waarbij lsw id bewaard blijft
intersection_doorspoeling_stedelijk = gpd.overlay(LSWs_met_doorspoeling2, stedelijk, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_doorspoeling_stedelijk2 = intersection_doorspoeling_stedelijk.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_doorspoeling_stedelijk3 = intersection_doorspoeling_stedelijk2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor stedelijk gebied per LSW
intersection_doorspoeling_stedelijk3['area_stedelijk_per_doorgespoelde LSW'] = intersection_doorspoeling_stedelijk3.area

#Voeg LSW shapefile en stedelijk shapefile samen op basis van lswfinal
lsw2 = LSWs_met_doorspoeling2.merge(intersection_doorspoeling_stedelijk3, on='lswfinal')
lsw2['Percentage stedelijk van LSW'] = lsw2['area_stedelijk_per_doorgespoelde LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 25%
lsw2.loc[lsw2["Percentage stedelijk van LSW"] >= 25, 'Percentage stedelijk groter dan 25'] = 'True'
lsw2.loc[lsw2["Percentage stedelijk van LSW"] < 25, 'Percentage stedelijk groter dan 25'] = 'False'

#Behoud interessante kolommen voor toevoegen aan doorspoelingtabel
stedelijk_doorspoeling = lsw2.iloc[:, [0,9,10,11]]
doorspoeling_tabel = doorspoeling_tabel.merge(stedelijk_doorspoeling, on='lswfinal', how='outer')

###AKKERBOUW
#Behoud data met akkerbouw uit landbouwkaart 
mask = (landgebruik['DN'] == 3) | (landgebruik['DN'] == 4) | (landgebruik['DN'] == 5) | (landgebruik['DN'] == 6) | (landgebruik['DN'] == 25) | (landgebruik['DN'] == 7) | (landgebruik['DN'] == 9) | (landgebruik['DN'] == 10) | (landgebruik['DN'] == 21)
akkerbouw = landgebruik.loc[mask]

#Doorspoeling akkerbouw
#Gebieden van lsw die overlappen met akkerbouw gebied, waarbij lsw id bewaard blijft
intersection_doorspoeling_akkerbouw = gpd.overlay(LSWs_met_doorspoeling2, akkerbouw, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_doorspoeling_akkerbouw2 = intersection_doorspoeling_akkerbouw.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_doorspoeling_akkerbouw3 = intersection_doorspoeling_akkerbouw2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor akkerbouw gebied per LSW
intersection_doorspoeling_akkerbouw3['area_akkerbouw_per_doorgespoelde LSW'] = intersection_doorspoeling_akkerbouw3.area

#Voeg LSW shapefile en akkerbouw shapefile samen op basis van lswfinal
lsw2 = LSWs_met_doorspoeling2.merge(intersection_doorspoeling_akkerbouw3, on='lswfinal')
lsw2['Percentage akkerbouw van LSW'] = lsw2['area_akkerbouw_per_doorgespoelde LSW'] / lsw2['area_LSW_totaal']*100
#Cut-off is gezet op 10%
lsw2.loc[lsw2["Percentage akkerbouw van LSW"] >= 10, 'Percentage akkerbouw groter dan 10'] = 'True'
lsw2.loc[lsw2["Percentage akkerbouw van LSW"] < 10, 'Percentage akkerbouw groter dan 10'] = 'False'

#Behoud interessante kolommen voor toevoegen aan doorspoelingtabel
akkerbouw_doorspoeling = lsw2.iloc[:, [0,9,10,11]]
doorspoeling_tabel = doorspoeling_tabel.merge(akkerbouw_doorspoeling, on='lswfinal', how='outer')

#Beregening akkerbouw
#Gebieden van doorgespoelde lsws die overlappen met akkerbouw, waarbij lsw id bewaard blijft
intersection_beregening_akkerbouw = gpd.overlay(LSWs_met_beregening2, akkerbouw, how = 'intersection')
#Gebieden in dezelfde lws-id worden samengenomen
intersection_beregening_akkerbouw2 = intersection_beregening_akkerbouw.dissolve(by='lswfinal', aggfunc='sum')
#Bewaar alleen lsw id en polygoon
intersection_beregening_akkerbouw3 = intersection_beregening_akkerbouw2.iloc[:,:1].reset_index()
#Voeg areaal toe van de polygonen voor akkerbouw per LSW
intersection_beregening_akkerbouw3['area_akkerbouw_per_beregende LSW'] = intersection_beregening_akkerbouw3.area

#Merge LSW shapefile en akkerbouw shapefile op basis van lswfinal
lsw2 = LSWs_met_beregening2.merge(intersection_beregening_akkerbouw3, on='lswfinal')
lsw2['Percentage akkerbouw van LSW'] = lsw2['area_akkerbouw_per_beregende LSW'] / lsw2['area_beregening_per_LSW']*100
#Cut-off is put on 10%
lsw2.loc[lsw2["Percentage akkerbouw van LSW"] >= 10, 'Percentage akkerbouw groter dan 10'] = 'True'
lsw2.loc[lsw2["Percentage akkerbouw van LSW"] < 10, 'Percentage akkerbouw groter dan 10'] = 'False'

#Behoud interessante kolommen voor toevoegen aan beregeningstabel
akkerbouw_beregening = lsw2.iloc[:, [0,26,27,28]]
beregening_tabel = beregening_tabel.merge(akkerbouw_beregening, on='lswfinal', how='outer')

###OVERIG
#Peilbeheerst overig
#Zet Nan naar 0
peilbeheer.fillna(0, inplace=True)
#Peilbeheerst overig
peilbeheer['Area overig peilbeheer'] = peilbeheer['LSW area'] - peilbeheer['area_natuur_per_LSW'] - peilbeheer['area_veen_per_LSW'] - peilbeheer['area_kapitaalintensief_per_LSW'] - peilbeheer['area_glastuinbouw_per_LSW']
peilbeheer.loc[peilbeheer["Area overig peilbeheer"] > 0, 'peilbeheer overig aanwezig'] = 'True'
peilbeheer.loc[peilbeheer["Area overig peilbeheer"] == 0, 'peilbeheer overig aanwezig'] = 'False'

#Doorspoeling overig
#Zet Nan naar 0 in tabel doorspoeling
doorspoeling_tabel.fillna(0, inplace=True)
#Bepaal areaal voor doorspoeling overig en voeg toe aan tabel doorspoeling
doorspoeling_tabel['Area overig doorspoeling'] = doorspoeling_tabel['LSW area'] - doorspoeling_tabel['area_glastuinbouw_per_doorgespoelde LSW'] - doorspoeling_tabel['area_stedelijk_per_doorgespoelde LSW'] - doorspoeling_tabel['area_akkerbouw_per_doorgespoelde LSW']
doorspoeling_tabel.loc[doorspoeling_tabel["Area overig doorspoeling"] > 0, 'doorspoeling overig aanwezig'] = 'True'
doorspoeling_tabel.loc[doorspoeling_tabel["Area overig doorspoeling"] == 0, 'doorspoeling overig aanwezig'] = 'False'
doorspoeling_tabel['Percentage overig van doorspoelde LSW'] = doorspoeling_tabel['Area overig doorspoeling'] / doorspoeling_tabel['LSW area']*100

#Beregening overig (Gras en mais)
#Zet Nan naar 0
beregening_tabel.fillna(0, inplace=True)
#Bepaal areaal voor beregening overig en voeg toe aan tabel beregening
beregening_tabel['Area overig beregening'] = beregening_tabel['Area beregening per LSW'] - beregening_tabel['area_glastuinbouw_per_beregende LSW'] - beregening_tabel['area_akkerbouw_per_beregende LSW']
beregening_tabel.loc[beregening_tabel["Area overig beregening"] > 0, 'Beregening overig aanwezig'] = 'True'
beregening_tabel.loc[beregening_tabel["Area overig beregening"] == 0, 'Beregening overig aanwezig'] = 'False'
beregening_tabel['Percentage overig van beregende LSW'] = beregening_tabel['Area overig beregening'] / beregening_tabel['Area beregening per LSW']*100


###DEFINITIEVE TABELLEN

#Peilbeheer
peilbeheer
#Assert error wanneer arealen samen niet gelijk zijn aan het LSW areaal, controleer dit voor meerdere rijen
test1 = (peilbeheer["LSW area"][1] - (peilbeheer["area_natuur_per_LSW"][1] + peilbeheer["area_veen_per_LSW"][1] + peilbeheer["area_kapitaalintensief_per_LSW"][1] + peilbeheer["area_glastuinbouw_per_LSW"][1] + peilbeheer["Area overig peilbeheer"][1]))
test2 = (peilbeheer["LSW area"][500] - (peilbeheer["area_natuur_per_LSW"][500] + peilbeheer["area_veen_per_LSW"][500] + peilbeheer["area_kapitaalintensief_per_LSW"][500] + peilbeheer["area_glastuinbouw_per_LSW"][500] + peilbeheer["Area overig peilbeheer"][500]))
test3 = (peilbeheer["LSW area"][1000] - (peilbeheer["area_natuur_per_LSW"][1000] + peilbeheer["area_veen_per_LSW"][1000] + peilbeheer["area_kapitaalintensief_per_LSW"][1000] + peilbeheer["area_glastuinbouw_per_LSW"][1000] + peilbeheer["Area overig peilbeheer"][1000]))
assert (
        -0.01 < test1 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test2 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test3 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"

    #Doorspoeling
doorspoeling_tabel
#Assert error wanneer arealen samen niet gelijk zijn aan het LSW areaal, controleer dit voor meerdere rijen
test1 = (doorspoeling_tabel["LSW area"][1] - (doorspoeling_tabel["area_glastuinbouw_per_doorgespoelde LSW"][1] + doorspoeling_tabel["area_akkerbouw_per_doorgespoelde LSW"][1] + doorspoeling_tabel["area_stedelijk_per_doorgespoelde LSW"][1] + doorspoeling_tabel["Area overig doorspoeling"][1]))
test2 = (doorspoeling_tabel["LSW area"][50] - (doorspoeling_tabel["area_glastuinbouw_per_doorgespoelde LSW"][50] + doorspoeling_tabel["area_akkerbouw_per_doorgespoelde LSW"][50] + doorspoeling_tabel["area_stedelijk_per_doorgespoelde LSW"][50] + doorspoeling_tabel["Area overig doorspoeling"][50]))
test3 = (doorspoeling_tabel["LSW area"][200] - (doorspoeling_tabel["area_glastuinbouw_per_doorgespoelde LSW"][200] + doorspoeling_tabel["area_akkerbouw_per_doorgespoelde LSW"][200] + doorspoeling_tabel["area_stedelijk_per_doorgespoelde LSW"][200] + doorspoeling_tabel["Area overig doorspoeling"][200]))
assert (
        -0.01 < test1 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test2 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test3 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"

    #Beregening
beregening_tabel
#Assert error wanneer arealen samen niet gelijk zijn aan het LSW areaal, controleer dit voor meerdere rijen
test1 = (beregening_tabel["Area beregening per LSW"][1] - (beregening_tabel["area_glastuinbouw_per_beregende LSW"][1] + beregening_tabel["area_akkerbouw_per_beregende LSW"][1] + beregening_tabel["Area overig beregening"][1]))
test2 = (beregening_tabel["Area beregening per LSW"][500] - (beregening_tabel["area_glastuinbouw_per_beregende LSW"][500] + beregening_tabel["area_akkerbouw_per_beregende LSW"][500] + beregening_tabel["Area overig beregening"][500]))
test3 = (beregening_tabel["Area beregening per LSW"][1000] - (beregening_tabel["area_glastuinbouw_per_beregende LSW"][1000] + beregening_tabel["area_akkerbouw_per_beregende LSW"][1000] + beregening_tabel["Area overig beregening"][1000]))
assert (
        -0.01 < test1 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test2 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"
assert (
        -0.01 < test3 < 0.01
    ), "Areaal verschillende gebruikers samen niet even groot als totale LSW areaal"

#Exporteer tables als Excel tabellen
peilbeheer.to_csv(path_peilbeheer_tabel, header=True, index=False, encoding='utf-8')
doorspoeling_tabel.to_csv(path_doorspoeling_tabel, header=True, index=False, encoding='utf-8')
beregening_tabel.to_csv(path_beregening_tabel, header=True, index=False, encoding='utf-8')
