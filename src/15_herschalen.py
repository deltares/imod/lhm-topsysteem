"""
Herschalen
==========
Uitvoerbestanden van voorgaande script schalen van 25 meter resolutie
naar 250 meter resolutie.

Voor verschillende grids worden verschillende methodes gebruikt. Voor peil en bodemhoogte wordt
de gemiddelde waarde berekend. Voor conductance, lengte en areaal worden
de waardes in een opgeschaalde cel gesommeerd. 
"""
import geopandas as gpd
import imod
import numpy as np
import xarray as xr

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ibound = imod.idf.open(path_input["ibound"]).isel(layer=0)
ibound = ibound.drop_vars('layer')
 # removes layer dimension from Ibound

# Maak lege array met de grove celgroottes om mee te regridden
dx, xmin, xmax, dy, ymin, ymax = (
    params["celgrootte_groot"],
    0.0,
    300000.0,
    -params["celgrootte_groot"],
    300000.0,
    625000.0,
)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_groveschaal = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)
like_groveschaal.to_netcdf(path_output["like_groveschaal"])

# Regridder aanmaken
mean_2d_regridder = imod.prepare.Regridder(method="mean")
sum_2d_regridder = imod.prepare.Regridder(method="sum")
mode_2d_regridder = imod.prepare.Regridder(method="mode")


def herschalen_gemiddelde_waarde(path_input_fijneschaal, path_output_groveschaal):
    # Functie voor het herschalen en opslaan van een invoerbestand dmv som van alle waardes
    input_fijneschaal = xr.open_dataarray(path_input_fijneschaal)
    output_groveschaal = mean_2d_regridder.regrid(input_fijneschaal, like_groveschaal)
    output_groveschaal = xr.where(ibound == 1, output_groveschaal, np.nan)
    output_groveschaal.to_netcdf(path_output_groveschaal)

def herschalen_som_waardes(path_input_fijneschaal, path_output_groveschaal):
    # Functie voor het herschalen en opslaan van een invoerbestand dmv som van alle waardes
    input_fijneschaal = xr.open_dataarray(path_input_fijneschaal)
    output_groveschaal = sum_2d_regridder.regrid(input_fijneschaal, like_groveschaal)
    output_groveschaal = xr.where(ibound == 1, output_groveschaal, np.nan)
    output_groveschaal.to_netcdf(path_output_groveschaal)


def herschalen_modus_waardes(path_input_fijneschaal, path_output_groveschaal):
    # Functie voor het herschalen en opslaan van een invoerbestand dmv modus van alle waardes
    # Dit betekent dat de meest voorkomende waarde gebruikt zal worden.
    input_fijneschaal = xr.open_dataarray(path_input_fijneschaal)
    output_groveschaal = mode_2d_regridder.regrid(input_fijneschaal, like_groveschaal)
    output_groveschaal = xr.where(ibound == 1, output_groveschaal, np.nan)
    output_groveschaal.to_netcdf(path_output_groveschaal)


# Eerst voor de riviersystemen:
for soort in ["primair", "secundair", "tertiair"]:
    herschalen_som_waardes(
        path_input[f"{soort}_lengte"], path_output[f"lengte_{soort}_herschaald"]
    )
#    herschalen_som_waardes(
#        path_input[f"{soort}_breedte"], path_output[f"breedte_{soort}_herschaald"]
#    )
    herschalen_som_waardes(
        path_input[f"{soort}_area"], path_output[f"area_{soort}_herschaald"]
    )
    # herschalen_modus_waardes(
    #     path_input[f"infiltratie_{soort}_modflow"],
    #     path_output[f"infiltratie_{soort}_modflow_herschaald"],
    # )
    # herschalen_modus_waardes(
    #     path_input[f"infiltratie_{soort}_mozart"],
    #     path_output[f"infiltratie_{soort}_mozart_herschaald"],
    # )
    for seizoen in ["zomer", "winter"]:
        herschalen_gemiddelde_waarde(
            path_input[f"peil_{soort}_{seizoen}"],
            path_output[f"peil_{soort}_{seizoen}_herschaald"],
        )
        herschalen_gemiddelde_waarde(
            path_input[f"bodemhoogte_{soort}_{seizoen}"],
            path_output[f"bodemhoogte_{soort}_{seizoen}_herschaald"],
        )

# Dan voor buisdrainage & SOF & greppels
for soort in ["SOF", "buisdrainage", "greppels"]:
    herschalen_gemiddelde_waarde(
        path_input[f"bodemhoogte_{soort}"],
        path_output[f"bodemhoogte_{soort}_herschaald"],
    )
    herschalen_som_waardes(
        path_input[f"conductance_{soort}"],
        path_output[f"conductance_{soort}_herschaald"],
    )

# Ook nog het totale wateroppervlak
herschalen_som_waardes(
    path_input["totaal_areaal"], path_output["totaal_areaal_herschaald"]
)
