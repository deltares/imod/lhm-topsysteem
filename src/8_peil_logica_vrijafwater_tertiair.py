"""
In dit script worden de rekenregels toegepast om het peil per polygoon in het vrijafwaterende gebied te bepalen.
In tegenstelling tot het peilgestuurde gebied, wordt in dit gebied aangenomen dat het peil kan verschillen over de lengte van de sloot.
Daarom wordt voor het vrijafwaterende gebied de peilberekening per rastercel uitgevoerd, in plaats van per oppervlaktewaterpolygoon.
Ook wordt de bodemhoogte vastgesteld

Zie de documentatie voor de rekenregels
"""
import imod
import xarray as xr 
import numpy as np
import pandas as pd
import geopandas as gpd

params = dict(zip(snakemake.params.keys(), snakemake.params))
path_input = dict(zip(snakemake.input.keys(), snakemake.input))
path_output = dict(zip(snakemake.output.keys(), snakemake.output))

ahn = xr.open_dataarray(path_input['AHN'])
ahnpeil = xr.open_dataarray(path_input['ahnpeil'])
regiopeil_zomer = xr.open_dataarray(path_input['regiopeil_zomer'])
regiopeil_winter = xr.open_dataarray(path_input['regiopeil_winter'])
peilvakken = gpd.read_file(path_input['peilgebieden'])
peilvakkenpeil_zomer = imod.prepare.rasterize(peilvakken, column='ZOMERPEIL', like=regiopeil_zomer)
peilvakkenpeil_winter = imod.prepare.rasterize(peilvakken, column='WINTERPEIL', like=regiopeil_zomer)

ahnpeil = ahnpeil / 1000. 
modus_2d_regridder = imod.prepare.Regridder(method="mode")

drooglegging_tabel_gt = pd.read_csv(path_input['drooglegging_GTtype'], index_col='GT_num')
GT_type = imod.idf.open(path_input['GT_karteerkaart'])
GT_model = imod.idf.open(path_input['GT_modelkaart'])
GT_model = modus_2d_regridder.regrid(GT_model, like=GT_type) 

GT_type = GT_type.where(GT_type.notnull(), GT_model)

for x in range(1,20):
    GT_type = xr.where(GT_type == x, drooglegging_tabel_gt.iloc[x-1, drooglegging_tabel_gt.columns.get_loc('tertiairspeil')],GT_type)
    
drooglegging_tertiair = modus_2d_regridder.regrid(GT_type, like=regiopeil_zomer)

dx, xmin, xmax, dy, ymin, ymax = (params["celgrootte_klein"], 0.0, 300000.0, -params["celgrootte_klein"], 300000.0, 625000.0)
dims = ("y", "x")
coords = {
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
}
nrow = coords["y"].size
ncol = coords["x"].size
like_25m = xr.DataArray(np.full((nrow, ncol), np.nan), coords, dims)


# -----
## tertiair
# -----
tertiair_vrijafwater = imod.prepare.gdal_rasterize(path_input['tertiair_vrijafwater'], column='lokaalid', like=like_25m)

ahn_drooglegging_tertiair = ahn - drooglegging_tertiair

#Zomerpeil
tertiair_zomerpeil = xr.ufuncs.fmax(peilvakkenpeil_zomer, ahn_drooglegging_tertiair)
tertiair_zomerpeil = xr.ufuncs.fmax(tertiair_zomerpeil, ahnpeil)

tertiair_zomerpeil = tertiair_zomerpeil.where(tertiair_vrijafwater.notnull()) # Houd alleen de tertiair vrijafwater sloten over
tertiair_zomerpeil.to_netcdf(path_output['tertiair_peil_zomer_vrijafwater'])

#Winterpeil
tertiair_winterpeil = xr.ufuncs.fmax(peilvakkenpeil_winter, ahn_drooglegging_tertiair)
tertiair_winterpeil = xr.ufuncs.fmax(tertiair_winterpeil, ahnpeil)

tertiair_winterpeil = tertiair_winterpeil.where(tertiair_vrijafwater.notnull()) # Houd alleen de tertiair vrijafwater sloten over
tertiair_winterpeil.to_netcdf(path_output['tertiair_peil_winter_vrijafwater'])



