# LHM Topsysteem

Sinds 2008 is in het LHM voor het aanmaken van invoer voor het bovenste deel van
het hydrologische systeem, het zogenaamde topsysteem, gebruik gemaakt van het
script topsysteem.py, dat gebruikt maakte van (een verouderde versie van) de
ArcGIS Python scripting API. Dit script was verouderd en bovendien niet
gedocumenteerd. Het script is in het kader van beheer en onderhoud van LHM
geanalyseerd, beschreven, en op basis van de beschrijving is een nieuw script
ontwikkeld met dezelfde functionaliteiten als het oude script. 

Deze repository bevat de recentste versie van dat ArcGIS script als
vergelijkingsmateriaal: `topsysteemv67.py`

# Opzet topsysteem programmatuur 

De vernieuwde topsysteem-programmatuur geschreven in Python 3. In de nieuwe
scripts is voor een aantal onderdelen gebruik gemaakt van bestaande
programmatuur; de iMOD-Python package, die gemaakt is om grondwatermodel
invoer te kunnen genereren en ook is toegepast in LHM zoet-zout. Zie
https://imod.xyz voor meer informatie over deze package.

De nieuwe topsysteem programmatuur bestaat uit een collectie pythonscripts die
los van elkaar werken en gebruikt kunnen worden, maar wel volledig op elkaar
zijn afgestemd. 

# Gebruik van Snakemake

Elk pythonscript heeft een exact gedefinieerde lijst met invoer-  en
uitvoerbestanden. Een specifiek bestand kan uitvoer zijn van script A en invoer
zijn van script B. 

In een zogenoemde Snakefile is per script aangegeven wat de invoer- en
uitvoer-bestanden zijn. Middels Snakemake kunnen vervolgens alle scripts, of een
deel daarvan aangeroepen en uitgevoerd worden. Wanneer script B aangeroepen
wordt, ziet Snakemake dat een specifiek invoerbestand van script B gemaakt wordt
door script A. Wanneer dat invoerbestand nog niet aangemaakt is, zal Snakemake
eerst script A uitvoeren. Deze opzet maakt het mogelijk om de topsysteem
programmatuur zowel in haar geheel, als in losse onderdelen gemakkelijk aan te
roepen.

# Module docstrings
De huidige module docstring zijn gegenereerd door met [pandoc](https://pandoc.org/),
van een word rapport naar markdown, met het volgende commando:

```
pandoc -o rapport.md rapport.docx
```

En vervolgens handmatig aan het begin van elke module geplakt.

Het is aan te raden om vooral aanpassing van tabellen in word te doen, en daarna
met pandoc te converteren. Tabellen in markdown aanpassen is ronduit ellendig,
terwijl pandoc genereert verrassend leesbare resultaten.
