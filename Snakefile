'''
How does one run a snakemake rule?

* open this folder in your cmd (anaconda prompt)
* snakemake {rulename} --cores 1

(without the curly braces, obviously)
'''
import itertools
from typing import Any, Dict, List
import pandas as pd
from pathlib import Path


celgrootte_klein = 25. 
celgrootte_groot = 250. 
celoppervlak_klein = celgrootte_klein*celgrootte_klein
celoppervlak_groot = celgrootte_groot*celgrootte_groot

scenario = 'ref' 

pst = ['primair','secundair','tertiair']
ps = ['primair','secundair']
pt = ['primair','tertiair']
ao = ['afwatering','ontwatering']
zw = ['zomer','winter']
sw = ['SUMMER','WINTER']
bsg = ['buisdrainage', 'SOF', 'greppels']
bsgnum = ['1', '2', '3']
pstnum = ['3', '4', '5']

date_start_ref = '1980-01-01' # start van referentie HWS peilen
date_end_ref = '2007-02-01' # eind van referentie HWS peilen
date_start = '2019-01-01' # start van gewenste HWS uitvoer
date_end = '2019-06-30' # eind van gewenste HWS uitvoer

datelist_ref = pd.date_range(date_start_ref, date_end_ref, freq='MS').strftime('%Y%m%d').tolist()
datelist_out = pd.date_range(date_start, date_end, freq='MS').strftime('%Y%m%d').tolist()

script_dir = r'e:\LHM_master\Scripts\1_Preprocessing\Topsysteem\lhm-topsysteem\src'
input_dir = r'e:\LHM_master\Data\1_To_Preprocess\Topsysteem\1-input'
interim_dir = r'e:\Topsysteem\2-interim'
output_dir = r'e:\Topsysteem\3-output'
lhmmaster_dir = r'e:\LHM_master\Data\2_Model_Input'

def multi(path: str, name: str, parameters: List[str]) -> Dict[str, str]:
    '''
    Generate all possible combinations ('cartesian product') of the provided
    parameters, return a dictionary of keyword arguments for a snakemake rule.
    '''
    result = {}
    combinations = list(itertools.product(*parameters))
    for combination in combinations:
        result[name.format(*combination)] = path.format(*combination)
    return result

# Note: double asterisk (**) is the 'unpack' operator
# This is equivalent:
# f(a=1, b=2, c=3)
# f(**{'a': 1, 'b': 2, 'c': 3})

rule hydrotype_tabel:
    input:
        script = f'{script_dir}/0_hydrotype_GT_tabellen.py',
        drooglegging_hydrotype = f'{input_dir}/massop_tabellen/drooglegging_hydrotype.csv',
        drooglegging_GTtype = f'{input_dir}/massop_tabellen/GT_drooglegging_tabel.csv',
        breedte_hydrotype = f'{input_dir}/massop_tabellen/breedte_hydrotype.csv',
        bodemdiepte_hydrotype = f'{input_dir}/massop_tabellen/bodemdiepte_hydrotype_ontwatering.csv',
        waterdiepte_hydrotype = f'{input_dir}/massop_tabellen/waterdiepte_hydrotype.csv',
    output:
        **multi(f'{interim_dir}'+ '/{}_params_hydrotype.csv','{}_params_hydrotype', [pst]),
        primair_vlakken_params_hydrotype = f'{interim_dir}/primair_vlakken_params_hydrotype.csv',
        tertiair_params_GT = f'{interim_dir}/tertiair_params_GT.csv',
    script:
        f'{script_dir}/0_hydrotype_GT_tabellen.py'

'''

!!!!!!!script 1 t/m 9 staan op pauze zolang de top10 data uit het oude topsysteem(top10 uit 2006 aangepast) gebruikt wordt!!!!!! 
rule centroids_sloten:
    input:
        script = f'{script_dir}/1_centroids_sloten.py', 
        top10nl_Waterdeel = f'{input_dir}/Basisdata/top10nl_Waterdeel.gpkg'
    output:
        centroidGebiedcsv = f'{interim_dir}/ogr/centroidGebied.csv',
        slootGebied = f'{interim_dir}/ogr/slootGebied.gpkg',
        centroidGebiedgpkg = f'{interim_dir}/ogr/centroidGebied.gpkg', 
        centroidGebied_vlakkencsv = f'{interim_dir}/ogr/centroidGebied_vlakken.csv',
        slootGebied_vlakken = f'{interim_dir}/ogr/slootGebied_vlakken.gpkg',
        centroidGebied_vlakkengpkg = f'{interim_dir}/ogr/centroidGebied_vlakken.gpkg'
    script: 
        f'{script_dir}/1_centroids_sloten.py'


rule find_GT_hydrotype:
    input:
        script = f'{script_dir}/2_find_GT_hydrotype.py', 
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        LSW_type = f'{input_dir}/Basisdata/LSW_type.shp',
        GT_type = f'{input_dir}/Basisdata/GT_kaart_combined.IDF',  # gevulde gekarteerde kaart 
        centroidGebiedcsv = f'{interim_dir}/ogr/centroidGebied.csv',
        centroidGebied_vlakkencsv = f'{interim_dir}/ogr/centroidGebied_vlakken.csv',
    output:
        GThydrotypesGebied = f'{interim_dir}/ogr/GThydrotypesGebied.csv',
        hydrotypesGebied_vlakken = f'{interim_dir}/ogr/hydrotypesGebied_vlakken.csv'
    script: 
        f'{script_dir}/2_find_GT_hydrotype.py'


rule assign_GT_hydrotype:
    input:
        script = f'{script_dir}/3_assign_GT_hydrotype.py', 
        GThydrotypesGebied = f'{interim_dir}/ogr/GThydrotypesGebied.csv',
        hydrotypesGebied_vlakken = f'{interim_dir}/ogr/hydrotypesGebied_vlakken.csv',
        slootGebied = f'{interim_dir}/ogr/slootGebied.gpkg',
        slootGebied_vlakken = f'{interim_dir}/ogr/slootGebied_vlakken.gpkg',
    output:
        slootGebied_GT_hydrotype = f'{interim_dir}/ogr/slootGebied_GT_hydrotype.gpkg',
        slootGebied_vlakken_hydrotype = f'{interim_dir}/ogr/slootGebied_vlakken_hydrotype.gpkg'
    script: 
        f'{script_dir}/3_assign_GT_hydrotype.py'


rule buffer_lijnen:
    input:
        script = f'{script_dir}/4_buffer_lijnen.py', 
        slootGebied_GT_hydrotype = f'{interim_dir}/ogr/slootGebied_GT_hydrotype.gpkg',
        slootGebied_vlakken_hydrotype = f'{interim_dir}/ogr/slootGebied_vlakken_hydrotype.gpkg', 
        **multi(f'{interim_dir}'+'/{}_params_hydrotype.csv','params_{}', [pst]),
        params_tertiair_GT = f'{interim_dir}/tertiair_params_GT.csv',
        params_primair_vlakken = f'{interim_dir}/primair_vlakken_params_hydrotype.csv',
    output:
        **multi(f'{interim_dir}'+'/{}.gpkg','{}', [pst]),
        primair_vlakken = f'{interim_dir}/ogr/primair_vlakken.gpkg',
        **multi(f'{interim_dir}'+'/ogr/{}_params.gpkg','{}_params', [pst]),
        tertiair_params_all = f'{interim_dir}/ogr/tertiair_params_all.gpkg',
        primair_vlakken_params = f'{interim_dir}/ogr/primair_vlakken_params.gpkg',
        **multi(f'{interim_dir}'+'/ogr/{}_buffer.gpkg','{}_buffer', [pst]),
        **multi(f'{interim_dir}'+'/ogr/{}_peilgestuurd.gpkg','{}_peilgestuurd', [pst]),
        **multi(f'{interim_dir}'+'/ogr/{}_vrijafwater.gpkg','{}_vrijafwater', [pst]),
    script: 
        f'{script_dir}/4_buffer_lijnen.py'


rule zonal_aggregate:
    input:
        script = f'{script_dir}/5_zonal_aggregate.py', 
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        ahn = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        **multi(f'{interim_dir}'+'/ogr/{}_peilgestuurd.gpkg','{}_peilgestuurd', [pst]),
    output:
        **multi(f'{interim_dir}'+'/{}_zonal_agg_peil_peilgestuurd.csv','{}_zonal_agg_peil_peilgestuurd', [pst]),
    script:
        f'{script_dir}/5_zonal_aggregate.py'


rule peil_logica_peilgestuurd:
    input:
        script = f'{script_dir}/6_peil_logica_peilgestuurd.py', 
        **multi(f'{interim_dir}'+'/{}_zonal_agg_peil_peilgestuurd.csv','{}_zonal_agg_peil_peilgestuurd', [pst]),
    output:
        **multi(f'{interim_dir}'+'/peil_bodemhoogte_{}_peilgestuurd.csv','peil_bodemhoogte_{}', [pst]),
    script: 
        f'{script_dir}/6_peil_logica_peilgestuurd.py'


rule celltable:
    input:
        script = f'{script_dir}/7_celltable.py', 
        **multi(f'{interim_dir}'+'/peil_bodemhoogte_{}_peilgestuurd.csv','peil_bodemhoogte_{}', [pst]),
        **multi(f'{interim_dir}'+'/ogr/{}_peilgestuurd.gpkg','{}_peilgestuurd', [pst]),
    output:
        **multi(f'{interim_dir}'+'/{}_celltable.csv','celltable_{}', [pst]),
        **multi(f'{interim_dir}'+'/{}_celltablegrouped.csv','celltable_grouped_{}', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_area_peilgestuurd_raster.nc','{}_area_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_bodemhoogte_{}_peilgestuurd_raster.nc','{}_bodemhoogte_{}_peilgestuurd', [pst,zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_breedte_peilgestuurd_raster.nc','{}_breedte_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_lengte_peilgestuurd_raster.nc','{}_lengte_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_waterdiepte_peilgestuurd_raster.nc','{}_waterdiepte_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_peil_{}_peilgestuurd_raster.nc','{}_peil_{}_peilgestuurd', [pst, zw]),
    script: 
        f'{script_dir}/7_celltable.py'


rule peil_logica_vrijafwater:
    input:
        script = f'{script_dir}/8_peil_logica_vrijafwater.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        drooglegging_hydrotype = f'{input_dir}/massop_tabellen/drooglegging_hydrotype.csv',
        **multi(f'{interim_dir}'+'/{}_vrijafwater.gpkg','{}_vrijafwater', [ps]),
    output:
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [ps,zw]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/8_peil_logica_vrijafwater.py'

rule peil_logica_vrijafwater_tertiair:
    input:
        script = f'{script_dir}/8_peil_logica_vrijafwater_tertiair.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        drooglegging_GTtype = f'{input_dir}/massop_tabellen/GT_drooglegging_tabel.csv',
        GT_modelkaart = f'{input_dir}/Basisdata/GT_kaart_LHM.idf',
        GT_karteerkaart = f'{input_dir}/Basisdata/WDM_Gt_mode.idf',
        tertiair_vrijafwater = f'{interim_dir}/ogr/tertiair_vrijafwater.gpkg',
    output:
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [pst,zw]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/8_peil_logica_vrijafwater_tertiair.py'

rule bodh_logica_vrijafwater:
    input:
        script = f'{script_dir}/8b_bodh_logica_vrijafwater.py',
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        waterdiepte_hydrotype = f'{input_dir}/massop_tabellen/waterdiepte_hydrotype.csv',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [pst,zw]),
    output:
        **multi(f'{output_dir}'+'/vrijafwater/{}_bodemhoogte_{}_vrijafwater_raster.nc','{}_bodemhoogte_{}_vrijafwater', [pst, zw]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/8b_bodh_logica_vrijafwater.py'


rule maak_rasters_sloten:
    input:
        script = f'{script_dir}/9_maak_rasters_sloten.py',
        dm_netwerk = f'{input_dir}/Basisdata/dmnetwerk_v17.shp',
        **multi(f'{interim_dir}'+'/ogr/{}_vrijafwater.gpkg','{}_vrijafwater', [pst]),
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [pst, zw]),
        **multi(f'{output_dir}'+'/vrijafwater/{}_bodemhoogte_{}_vrijafwater_raster.nc','{}_bodemhoogte_{}_vrijafwater', [pst, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_bodemhoogte_{}_peilgestuurd_raster.nc','{}_bodemhoogte_{}_peilgestuurd', [pst, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_peil_{}_peilgestuurd_raster.nc','{}_peil_{}_peilgestuurd', [pst, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_area_peilgestuurd_raster.nc','{}_area_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_breedte_peilgestuurd_raster.nc','{}_breedte_peilgestuurd', [pst]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_lengte_peilgestuurd_raster.nc','{}_lengte_peilgestuurd', [pst]),
    output:
        **multi(f'{output_dir}'+'/basisdata25m/{}_breedte_raster.nc', '{}_breedte', [pst]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_lengte_raster.nc', '{}_lengte', [pst]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_area_raster.nc', '{}_area', [pst]), 
        **multi(f'{interim_dir}'+'/{}_peil_{}_raster.nc','{}_peil_{}', [pst, zw]),
        **multi(f'{interim_dir}'+'/{}_bodemhoogte_{}_raster.nc','{}_bodemhoogte_{}', [pst, zw]),
        totaal_areaal = f'{output_dir}/basisdata25m/areaal_totaal.nc',
    params:
        celoppervlak_klein = celoppervlak_klein,
        celgrootte_klein = celgrootte_klein,
    script:
        f'{script_dir}/9_maak_rasters_sloten.py'
'''

rule watergangen_splitsen:
    input:
        script = f'{script_dir}/A_watergangen_splitsen.py',
        LSW_type = f'{input_dir}/Basisdata/LSW_type.shp',
        **multi(f'{input_dir}'+'/Basisdata/aanw_{}_25m.asc','{}_watergang', [pst]),
    output:
        **multi(f'{interim_dir}'+'/peilraster/{}_peilgestuurd.nc','{}_peilgestuurd', [pst]),
        **multi(f'{interim_dir}'+'/peilraster/{}_vrijafwater.nc','{}_vrijafwater', [pst]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/A_watergangen_splitsen.py'

rule droogleggingskaarten:
    input:
        script = f'{script_dir}/A2_droogleggingskaarten.py',
        drooglegging_GT_type = f'{input_dir}/massop_tabellen/GT_drooglegging_tabel.csv',
        GT_karteerkaart = f'{input_dir}/Basisdata/WDM_Gt_mode.idf',
        GT_modelkaart = f'{input_dir}/Basisdata/GT_kaart_LHM.IDF',
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        bodemdiepte_hydrotype = f'{input_dir}/massop_tabellen/bodemdiepte_hydrotype_ontwatering.csv',
    output:
        **multi(f'{interim_dir}'+'/drooglegging_afwatering_{}.nc','drooglegging_afwatering_{}', [zw]),
        drooglegging_ontwatering = f'{interim_dir}/drooglegging_ontwatering.nc',
        waterdiepte_afwatering_zomer = f'{interim_dir}/waterdiepte_afwatering_zomer.nc',
        waterdiepte_afwatering_winter = f'{interim_dir}/waterdiepte_afwatering_winter.nc',
        waterdiepte_ontwatering = f'{interim_dir}/waterdiepte_ontwatering.nc',
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/A2_droogleggingskaarten.py'

rule peil_logica_peilgestuurd:
    input:
        script = f'{script_dir}/B_peil_logica_peilgestuurd.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        scheiding_hydrotype = f'{input_dir}/massop_tabellen/scheiding_hydrotype_ont_afwatering.csv',
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        **multi(f'{interim_dir}'+'/drooglegging_afwatering_{}.nc','drooglegging_afwatering_{}', [zw]),
        **multi(f'{interim_dir}'+'/peilraster/{}_peilgestuurd.nc','{}_peilgestuurd', [ps]),
        drooglegging_ontwatering = f'{interim_dir}/drooglegging_ontwatering.nc',
    output:
        **multi(f'{output_dir}'+'/peilgestuurd/primair_peil_{}_peilgestuurd_raster.nc','primair_peil_{}_peilgestuurd', [zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/secundair_peil_{}_peilgestuurd_raster_{}.nc','secundair_peil_{}_peilgestuurd_{}', [zw, ao]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/B_peil_logica_peilgestuurd.py'

rule peil_logica_vrijafwater:
    input:
        script = f'{script_dir}/C_peil_logica_vrijafwater.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        scheiding_hydrotype = f'{input_dir}/massop_tabellen/scheiding_hydrotype_ont_afwatering.csv',
        hydrotypen = f'{input_dir}/Basisdata/hydrotypen.shp',
        **multi(f'{interim_dir}'+'/drooglegging_afwatering_{}.nc','drooglegging_afwatering_{}', [zw]),
        **multi(f'{interim_dir}'+'/peilraster/{}_vrijafwater.nc','{}_vrijafwater', [ps]),
        drooglegging_ontwatering = f'{interim_dir}/drooglegging_ontwatering.nc',
    output:
        **multi(f'{output_dir}'+'/vrijafwater/primair_peil_{}_vrijafwater_raster.nc','primair_peil_{}_vrijafwater', [zw]),
        **multi(f'{output_dir}'+'/vrijafwater/secundair_peil_{}_vrijafwater_raster_{}.nc','secundair_peil_{}_vrijafwater_{}', [zw, ao]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/C_peil_logica_vrijafwater.py'

rule peil_logica_tertiair:
    input:
        script = f'{script_dir}/D_peil_logica_tertiair.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc',
        ahnpeil = f'{input_dir}/peildata/AHN_STAGE_MIC_1000.nc',
        regiopeil_zomer = f'{input_dir}/peildata/peil_z_buf.nc',
        regiopeil_winter = f'{input_dir}/peildata/peil_w_buf.nc',
        peilgebieden = f'{input_dir}/peildata/Peilgebieden_noNaN.shp',
        drooglegging_ontwatering = f'{interim_dir}/drooglegging_ontwatering.nc',
        tertiair_vrijafwater = f'{interim_dir}/peilraster/tertiair_vrijafwater.nc',
        tertiair_peilgestuurd = f'{interim_dir}/peilraster/tertiair_peilgestuurd.nc',
    output:
        **multi(f'{output_dir}'+'/vrijafwater/tertiair_peil_{}_vrijafwater_raster.nc','tertiair_peil_{}_vrijafwater', [zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/tertiair_peil_{}_peilgestuurd_raster.nc','tertiair_peil_{}_peilgestuurd', [zw]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/D_peil_logica_tertiair.py'


rule bodh_logica_peilgestuurd:
    input:
        script = f'{script_dir}/E_bodh_logica_peilgestuurd.py',
        waterdiepte_primair = f'{input_dir}/waterdieptes/WDIEP1_250M.idf',
        waterdiepte_secundair = f'{input_dir}/waterdieptes/WDIEP2_250M.idf',
        **multi(f'{output_dir}'+'/peilgestuurd/{}_peil_{}_peilgestuurd_raster.nc','{}_peil_{}_peilgestuurd', [pt,zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/secundair_peil_{}_peilgestuurd_raster_{}.nc','secundair_peil_{}_peilgestuurd_{}', [zw, ao]),
    output:
        **multi(f'{output_dir}'+'/peilgestuurd/{}_bodemhoogte_{}_peilgestuurd_raster.nc','{}_bodemhoogte_{}_peilgestuurd', [pt, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/secundair_bodemhoogte_{}_peilgestuurd_raster_{}.nc','secundair_bodemhoogte_{}_peilgestuurd_{}', [zw, ao]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/E_bodh_logica_peilgestuurd.py'

rule bodh_logica_vrijafwater:
    input:
        script = f'{script_dir}/F_bodh_logica_vrijafwater.py',
        waterdiepte_primair = f'{input_dir}/waterdieptes/WDIEP1_250M.idf',
        waterdiepte_secundair = f'{input_dir}/waterdieptes/WDIEP2_250M.idf',
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [pt,zw]),
        **multi(f'{output_dir}'+'/vrijafwater/secundair_peil_{}_vrijafwater_raster_{}.nc','secundair_peil_{}_vrijafwater_{}', [zw, ao]),
    output:
        **multi(f'{output_dir}'+'/vrijafwater/{}_bodemhoogte_{}_vrijafwater_raster.nc','{}_bodemhoogte_{}_vrijafwater', [pt, zw]),
        **multi(f'{output_dir}'+'/vrijafwater/secundair_bodemhoogte_{}_vrijafwater_raster_{}.nc','secundair_bodemhoogte_{}_vrijafwater_{}', [zw, ao]),
    params:
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/F_bodh_logica_vrijafwater.py'


rule maak_rasters_sloten:
    input:
        script = f'{script_dir}/G_maak_rasters_sloten.py',
        dm_netwerk = f'{input_dir}/Basisdata/dmnetwerk_v17.shp',
        **multi(f'{output_dir}'+'/vrijafwater/{}_peil_{}_vrijafwater_raster.nc','{}_peil_{}_vrijafwater', [pt, zw]),
        **multi(f'{output_dir}'+'/vrijafwater/{}_bodemhoogte_{}_vrijafwater_raster.nc','{}_bodemhoogte_{}_vrijafwater', [pt, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_bodemhoogte_{}_peilgestuurd_raster.nc','{}_bodemhoogte_{}_peilgestuurd', [pt, zw]),
        **multi(f'{output_dir}'+'/peilgestuurd/{}_peil_{}_peilgestuurd_raster.nc','{}_peil_{}_peilgestuurd', [pt, zw]),
        **multi(f'{output_dir}'+'/vrijafwater/secundair_peil_{}_vrijafwater_raster_{}.nc','secundair_peil_{}_vrijafwater_{}', [zw, ao]),
        **multi(f'{output_dir}'+'/vrijafwater/secundair_bodemhoogte_{}_vrijafwater_raster_{}.nc','secundair_bodemhoogte_{}_vrijafwater_{}', [zw, ao]),
        **multi(f'{output_dir}'+'/peilgestuurd/secundair_bodemhoogte_{}_peilgestuurd_raster_{}.nc','secundair_bodemhoogte_{}_peilgestuurd_{}', [zw,ao]),
        **multi(f'{output_dir}'+'/peilgestuurd/secundair_peil_{}_peilgestuurd_raster_{}.nc','secundair_peil_{}_peilgestuurd_{}', [zw, ao]),
        **multi(f'{input_dir}'+'/Basisdata/opp_{}_25m.asc','{}_area', [pst]),
        **multi(f'{input_dir}'+'/Basisdata/lengte_{}_25m.asc','{}_lengte', [pst]),
    output:
        **multi(f'{output_dir}'+'/basisdata25m/{}_lengte_raster.nc', '{}_lengte', [pst]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_area_raster.nc', '{}_area', [pst]), 
        **multi(f'{interim_dir}'+'/{}_peil_{}_raster.nc','{}_peil_{}', [pst, zw]),
        **multi(f'{interim_dir}'+'/{}_bodemhoogte_{}_raster.nc','{}_bodemhoogte_{}', [pst, zw]),
        totaal_areaal = f'{output_dir}/basisdata25m/areaal_totaal.nc',
    params:
        celoppervlak_klein = celoppervlak_klein,
        celgrootte_klein = celgrootte_klein,
    script: 
        f'{script_dir}/G_maak_rasters_sloten.py'


rule primair_secundair_combi_peil:
    input:
        script = f'{script_dir}/10_primair_secundair_combi_peil.py',
        **multi(f'{interim_dir}'+'/{}_peil_{}_raster.nc', 'peil_{}_{}', [pst,zw]),
    output:
        peil_ps_zomer = f'{interim_dir}/ps_combi_peil_zomer.nc',
        peil_ps_winter = f'{interim_dir}/ps_combi_peil_winter.nc',
    script:
        f'{script_dir}/10_primair_secundair_combi_peil.py'


rule buisdrainage:
    input:
        script = f'{script_dir}/11_buisdrainage.py',
        peil_landsdekkend_zomer = f'{interim_dir}/ps_combi_peil_zomer.nc',
        peil_landsdekkend_winter = f'{interim_dir}/ps_combi_peil_winter.nc', 
        weerstand_buisdrainage = f'{input_dir}/buisdrainage/buisdrainage_weerstand.asc', 
        bodemdiepte_buisdrainage = f'{input_dir}/buisdrainage/diepte_buisdrainage.ASC', 
        landgebruik = f'{input_dir}/Basisdata/LGN_LHM_{scenario}.asc',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc', 
    output:
        bodemhoogte_buisdrainage = f'{output_dir}/basisdata25m/zonder_bodemdaling/bodemhoogte_buisdrainage.nc',
        conductance_buisdrainage = f'{output_dir}/basisdata25m/conductance_buisdrainage.nc',
    params:
        celoppervlak_klein = celoppervlak_klein,
        celgrootte_klein = celgrootte_klein
    script:
        f'{script_dir}/11_buisdrainage.py'


rule SOF:
    input:
        script = f'{script_dir}/12_SOF.py',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc', 
        watervlakken = f'{input_dir}/Basisdata/Watergangen_uit_SOF2.IDF',
        pleistocene = f'{input_dir}/Basisdata/HOLO-PLEISTO_25.ASC',
    output:
        bodemhoogte_SOF = f'{output_dir}/basisdata25m/zonder_bodemdaling/bodemhoogte_SOF.nc',
        conductance_SOF = f'{output_dir}/basisdata25m/conductance_SOF.nc',
    params:
        celoppervlak_klein = celoppervlak_klein,
        SOF_weerstand = 30.,
        SOF_diepte_pleistocene = 0.25,
        SOF_diepte_holoceen = 0.15,
        celgrootte_klein = celgrootte_klein
    script:
        f'{script_dir}/12_SOF.py'


rule greppels:
    input:
        script = f'{script_dir}/13_greppels.py',
        greppels = f'{input_dir}/greppels/BRP2012_mvgrep_fig_20.shp',
        veengebied_holland = f'{input_dir}/greppels/Holland_Utrecht_veengebied_BOFEK2020.shp',
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc', 
    output:
        oppervlak_greppels = f'{interim_dir}/area_greppels.nc',
        conductance_greppels = f'{output_dir}/basisdata25m/conductance_greppels.nc',
        bodemhoogte_greppels = f'{output_dir}/basisdata25m/zonder_bodemdaling/bodemhoogte_greppels.nc',
    params:
        celgrootte_klein = celgrootte_klein,
        greppelafstand_NL = 15.,
        greppelafstand_veenholland = 40.,
        greppel_breedte = 0.5,
        greppels_weerstand = 30.,
    script:
        f'{script_dir}/13_greppels.py'


rule bodemdaling:
    input:
        script = f'{script_dir}/14_bodemdaling.py',
        **multi(f'{interim_dir}'+'/{}_peil_{}_raster.nc', 'peil_{}_{}', [pst, zw]),
        **multi(f'{interim_dir}'+'/{}_bodemhoogte_{}_raster.nc', 'bodemhoogte_{}_{}', [pst, zw]), 
        **multi(f'{output_dir}'+'/basisdata25m/zonder_bodemdaling/bodemhoogte_{}.nc', 'bodemhoogte_{}', [bsg]),
        AHN = f'{input_dir}/Basisdata/ahn2_LHM_25_m.nc', 
        bodemdaling = f'{input_dir}/bodemdaling/bdal-{scenario}.ASC',
        landgebruik = f'{input_dir}/Basisdata/LGN_LHM_{scenario}.asc', 
    output:
        **multi(f'{output_dir}'+'/basisdata25m/{}_peil_{}.nc', 'peil_{}_{}', [pst, zw]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_bodemhoogte_{}.nc', 'bodemhoogte_{}_{}', [pst,zw]), 
        **multi(f'{output_dir}'+'/basisdata25m/bodemhoogte_{}.nc', 'bodemhoogte_{}', [bsg]),
    params:
        celgrootte_klein = celgrootte_klein,
        peilcorrectie_natte_natuur = 0.2, 
    script:
        f'{script_dir}/14_bodemdaling.py'


rule herschalen: 
    input:
        script = f'{script_dir}/15_herschalen.py',
        ibound = f'{input_dir}/Basisdata/IBOUND_L1.IDF',
        totaal_areaal = f'{output_dir}/basisdata25m/areaal_totaal.nc',
        **multi(f'{output_dir}'+'/basisdata25m/{}_bodemhoogte_{}.nc', 'bodemhoogte_{}_{}', [pst,zw]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_peil_{}.nc', 'peil_{}_{}', [pst,zw]),
        **multi(f'{output_dir}'+'/basisdata25m/{}_lengte_raster.nc', '{}_lengte', [pst]), 
        **multi(f'{output_dir}'+'/basisdata25m/{}_area_raster.nc', '{}_area', [pst]), 
        **multi(f'{output_dir}'+'/basisdata25m/bodemhoogte_{}.nc', 'bodemhoogte_{}', [bsg]),
        **multi(f'{output_dir}'+'/basisdata25m/conductance_{}.nc', 'conductance_{}', [bsg]),
    output:
        like_groveschaal = f'{interim_dir}/like_groveschaal.nc',
        totaal_areaal_herschaald = f'{output_dir}/herschaald/totaal_areaal_m2_groveschaal.nc',
        **multi(f'{output_dir}'+'/herschaald/{}_bodemhoogte_{}_groveschaal.nc', 'bodemhoogte_{}_{}_herschaald', [pst, zw]),
        **multi(f'{output_dir}'+'/herschaald/{}_peil_{}_groveschaal.nc', 'peil_{}_{}_herschaald', [pst,zw]),
        **multi(f'{output_dir}'+'/herschaald/{}_lengte_m_groveschaal.nc', 'lengte_{}_herschaald', [pst]),
        **multi(f'{output_dir}'+'/herschaald/{}_area_groveschaal.nc', 'area_{}_herschaald', [pst]),
        **multi(f'{output_dir}'+'/herschaald/bodemhoogte_{}_groveschaal.nc', 'bodemhoogte_{}_herschaald', [bsg]),
        **multi(f'{output_dir}'+'/herschaald/conductance_{}_groveschaal.nc', 'conductance_{}_herschaald', [bsg]),
    params:
        celgrootte_groot = celgrootte_groot,
    script:
        f'{script_dir}/15_herschalen.py'


rule conductance5_hydrotype_k:
    input:
        script = f'{script_dir}/conductance/5-hydrotype_k.py',
        D = f'{input_dir}/conductance/D.ASC',
        hydrotype = f'{input_dir}/Basisdata/hydrotypen.shp',
        k_waarden_hydrotypes = f'{input_dir}/conductance/k_waarden_hydrotypes.csv',
    output:
        hydrotype_kh = f'{interim_dir}/conductance/hydrotype_kh.idf',
        hydrotype_kv = f'{interim_dir}/conductance/hydrotype_kv.idf',
        test_D = f'{interim_dir}/conductance/test_D.tif',
    script:
        f'{script_dir}/conductance/5-hydrotype_k.py'


rule conductance6_leakage_c:
    input:
        script = f'{script_dir}/conductance/6-leakage_c.py',
        D = f'{input_dir}/conductance/D.ASC',
        C0 = f'{input_dir}/conductance/C0.ASC',
        mdl_vc_l1 = 'e:/LHM_4.3_prior/Data/2_Model_Input/modflow/C/MDL_C_l1.idf',
        hydrotype_kh = f'{interim_dir}/conductance/hydrotype_kh.idf',
        hydrotype_kv = f'{interim_dir}/conductance/hydrotype_kv.idf',
        AHN_F250 = f'{input_dir}/Basisdata/AHN_F250_M.IDF',
        bot = f'{input_dir}/conductance/bot_LHM',
        top = f'{input_dir}/conductance/top_LHM',
        kd = 'e:/LHM_4.3_prior/Data/2_Model_Input/modflow/KD',
        **multi(f'{output_dir}'+'/herschaald/{}_area_groveschaal.nc', 'area_{}_herschaald', [pst]),
        **multi(f'{output_dir}'+'/herschaald/{}_lengte_m_groveschaal.nc', 'lengte_{}_herschaald', [pst]), 
        **multi(f'{output_dir}'+'/herschaald/{}_peil_{}_groveschaal.nc', 'peil_{}_{}_herschaald', [pst, zw]),
    output:
        **multi(f'{output_dir}'+'/conductance/cond_{}.idf', 'cond_leakage_{}', [pst]),
        **multi(f'{output_dir}'+'/conductance/c_{}.idf', 'c_wet_{}', [pst]),
        kh1 = f'{output_dir}/conductance/kh1.idf',
        kh2 = f'{output_dir}/conductance/kh2.idf',
        kd = f'{output_dir}/conductance/kd.idf',
    script:
        f'{script_dir}/conductance/6-leakage_c.py'


rule buitenland_toevoegen:
    input:
        script = f'{script_dir}/15b_buitenland_toevoegen.py',
        ibound = f'{input_dir}/Basisdata/NHI-mask_02.asc',
        IB_peil_gemiddeld = f'{input_dir}/buitenland/rivers_IB/RIVER_PRIMAIR/RIVER_PRIMAIR_STAGE_GEMIDDELD.IDF',
        IB_peil_secundair = f'{input_dir}/buitenland/rivers_IB/DRN/VERSION_2/RIVER_SECUNDAIR_BOTTOM_WINTER.IDF',
        IB_peil_drain = f'{input_dir}/buitenland/rivers_IB/DRN/VERSION_2/DRAINAGE_STAGE.IDF',
        IB_bottom_winter = f'{input_dir}/buitenland/rivers_IB/RIVER_PRIMAIR/RIVER_PRIMAIR_BOTTOM_WINTER.IDF',
        IB_cond_gemiddeld = f'{input_dir}/buitenland/rivers_IB/RIVER_PRIMAIR/iPEST_RIVER_PRIMAIR_COND_GEMIDDELD.IDF',
        IB_cond_secundair = f'{input_dir}/buitenland/rivers_IB/DRN/VERSION_2/iPEST_RIVER_SECUNDAIR_COND_WINTER.IDF',
        IB_cond_drain = f'{input_dir}/buitenland/rivers_IB/DRN/VERSION_2/iPEST_DRAINAGE_CONDUCTANCE.IDF',

        BM_peil_winter = f'{input_dir}/buitenland/Rivers_Brabantmodel/hr1w_n.asc',
        BM_peil_zomer = f'{input_dir}/buitenland/Rivers_Brabantmodel/hr1z_n.asc',
        BM_bodem = f'{input_dir}/buitenland/Rivers_Brabantmodel/RD1_n.asc',
        BM_cond = f'{input_dir}/buitenland/Rivers_Brabantmodel/cond_n.asc',

        cond_LHM4_0 = f'{input_dir}/buitenland/SOF/COND_SOF_250_LHM40_buitenland.IDF',
        bodh_LHM4_0 = f'{input_dir}/buitenland/SOF/BODH_SOF_250_LHM40_buitenland.IDF',

        **multi(f'{output_dir}'+'/herschaald/bodemhoogte_{}_groveschaal.nc', 'bodemhoogte_{}_herschaald', [bsg]),
        **multi(f'{output_dir}'+'/herschaald/conductance_{}_groveschaal.nc', 'conductance_{}_herschaald', [bsg]),
        **multi(f'{output_dir}'+'/herschaald/{}_peil_{}_groveschaal.nc', 'peil_{}_{}_herschaald', [pst,zw]),
        **multi(f'{output_dir}'+'/herschaald/{}_bodemhoogte_{}_groveschaal.nc', 'bodemhoogte_{}_{}_herschaald', [pst, zw]),
        **multi(f'{output_dir}'+'/conductance/cond_{}.idf', 'cond_leakage_{}', [pst]),
    output:
        **multi(f'{output_dir}'+'/buitenland/{}_peil_{}_buitenland.nc', 'peil_{}_{}_buitenland', [pst,zw]),
        **multi(f'{output_dir}'+'/buitenland/{}_bodemhoogte_{}_buitenland.nc', 'bodemhoogte_{}_{}_buitenland', [pst, zw]),
        **multi(f'{output_dir}'+'/buitenland/cond_{}_buitenland.idf', 'cond_leakage_{}_buitenland', [pst]),
        **multi(f'{output_dir}'+'/buitenland/bodemhoogte_{}_buitenland.nc', 'bodemhoogte_{}_buitenland', [bsg]),
        **multi(f'{output_dir}'+'/buitenland/conductance_{}_buitenland.nc', 'conductance_{}_buitenland', [bsg]),
    params:
        celgrootte_groot = celgrootte_groot,
    script:
        f'{script_dir}/15b_buitenland_toevoegen.py'


rule correcties:
    input:
        script = f'{script_dir}/16_correcties.py',
        AHN_F250 = f'{input_dir}/Basisdata/AHN_F250_M.IDF',
        **multi(f'{output_dir}'+'/buitenland/{}_bodemhoogte_{}_buitenland.nc', 'bodemhoogte_{}_{}_buitenland', [pst, zw]),         
        **multi(f'{output_dir}'+'/buitenland/{}_peil_{}_buitenland.nc', 'peil_{}_{}_buitenland', [pst,zw]),
        **multi(f'{output_dir}'+'/buitenland/cond_{}_buitenland.idf', 'cond_leakage_{}', [pst]), 
        **multi(f'{output_dir}'+'/buitenland/bodemhoogte_{}_buitenland.nc', 'bodemhoogte_{}_buitenland', [bsg]),
        **multi(f'{output_dir}'+'/buitenland/conductance_{}_buitenland.nc', 'conductance_{}_buitenland', [bsg]),
        **multi(f'{input_dir}'+'/mask_lhm42/PEIL_{}_250.IDF', 'peil_lhm42_{}', [ps]),

        # infiltratiefactoren: later anders doen:
        infiltratie_mozart_primair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMZ_P1_250.IDF', 
        infiltratie_mozart_secundair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMZ_S1_250.IDF',
        infiltratie_mozart_tertiair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMZ_T1_250.IDF',
        infiltratie_modflow_primair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMF_P1_250.IDF', 
        infiltratie_modflow_secundair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMF_S1_250.IDF',
        infiltratie_modflow_tertiair = f'{input_dir}/Infiltratie_invoer/LHM_4.0/INFMF_T1_250.IDF',
    output:
        **multi(f'{output_dir}'+'/basisdata250m/PEIL_{}_{}.nc', 'peil_{}_{}_gecorrigeerd_nc', [pst,zw]),
        **multi(f'{output_dir}'+'/basisdata250m/PEIL_{}_{}.idf', 'peil_{}_{}_gecorrigeerd_idf', [pst,zw]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.nc', 'cond_leakage_{}_gecorrigeerd_nc', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.idf', 'cond_leakage_{}_gecorrigeerd_idf', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/BODH_{}_{}.nc', 'bodemhoogte_{}_{}_gecorrigeerd_nc', [pst,zw]),
        **multi(f'{output_dir}'+'/basisdata250m/BODH_{}_{}.idf', 'bodemhoogte_{}_{}_gecorrigeerd_idf', [pst,zw]),

        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MZ_{}.nc', 'infiltratiefactor_{}_mozart_gecorrigeerd_nc', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MZ_{}.idf', 'infiltratiefactor_{}_mozart_gecorrigeerd_idf', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MF_{}.nc', 'infiltratiefactor_{}_modflow_gecorrigeerd_nc', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MF_{}.idf', 'infiltratiefactor_{}_modflow_gecorrigeerd_idf', [pst]),

        **multi(f'{output_dir}'+'/basisdata250m/BODH_{}.nc', 'bodemhoogte_{}_gecorrigeerd_nc', [bsg]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.nc', 'conductance_{}_gecorrigeerd_nc', [bsg]),
        **multi(f'{output_dir}'+'/basisdata250m/BODH_{}.idf', 'bodemhoogte_{}_gecorrigeerd_idf', [bsg]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.idf', 'conductance_{}_gecorrigeerd_idf', [bsg]),

        **multi(f'{output_dir}'+'/diff/{}_peil_{}_diff.nc', 'peil_{}_{}_diff_nc', [pst,zw]),
        **multi(f'{output_dir}'+'/diff/{}_peil_{}_diff.idf', 'peil_{}_{}_diff_idf', [pst,zw]),
        **multi(f'{output_dir}'+'/diff/{}_bodemhoogte_{}_diff.nc', 'bodemhoogte_{}_{}_diff_nc', [pst,zw]),
        **multi(f'{output_dir}'+'/diff/{}_bodemhoogte_{}_diff.idf', 'bodemhoogte_{}_{}_diff_idf', [pst,zw]),
        **multi(f'{output_dir}'+'/diff/bodemhoogte_{}_diff.nc', 'bodemhoogte_{}_diff_nc', [bsg]),
        **multi(f'{output_dir}'+'/diff/conductance_{}_diff.nc', 'conductance_{}_diff_nc', [bsg]),
        **multi(f'{output_dir}'+'/diff/bodemhoogte_{}_diff.idf', 'bodemhoogte_{}_diff_idf', [bsg]),
        **multi(f'{output_dir}'+'/diff/conductance_{}_diff.idf', 'conductance_{}_diff_idf', [bsg]),
    script:
        f'{script_dir}/16_correcties.py'


rule kalibratiefactoren:
    input:
        script = f'{script_dir}/18_kalibratiefactoren.py',
        batch_file = f'{input_dir}/kalibratiefactoren/toepassen_kalibratiefactoren.bat',
        **multi(f'{input_dir}'+'/kalibratiefactoren/LHM_4.2/RI_Z999_SYS{}.IDF', 'RI_sys_{}', [pstnum]),
        **multi(f'{input_dir}'+'/kalibratiefactoren/LHM_4.2/RC_SUMMER_SYS{}.IDF', 'RC_zsys_{}', [pstnum]),
        **multi(f'{input_dir}'+'/kalibratiefactoren/LHM_4.2/RC_WINTER_SYS{}.IDF', 'RC_wsys_{}', [pstnum]),
        **multi(f'{input_dir}'+'/kalibratiefactoren/LHM_4.2/DC_Z999_SYS{}.IDF', 'DC_sys_{}', [bsgnum]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.idf', 'COND_{}', [bsg]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/COND_{}.idf', 'COND_{}', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MF_{}.idf', 'INF_modflow_{}', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/ongekalibreerd/INF_MZ_{}.idf', 'INF_mozart_{}', [pst]),
    output:
        batch_file = 'e:/Topsysteem/kalibratie_batch/kalibratiefactor_run.bat',
    params:
        loc_configuratie = 'e:/Topsysteem/kalibratie_batch',
        path_output = f'{output_dir}/basisdata250m',
        **multi(f'{output_dir}'+'/basisdata250m/COND_{}', 'COND_{}', [bsg]),
        **multi(f'{output_dir}'+'/basisdata250m/COND_{}_zomer', 'COND_{}_zomer', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/COND_{}_winter', 'COND_{}_winter', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/INF_modflow_{}', 'INF_modflow_{}', [pst]),
        **multi(f'{output_dir}'+'/basisdata250m/INF_mozart_{}', 'INF_mozart_{}', [pst]),
    script:
        f'{script_dir}/18_kalibratiefactoren.py'

rule output_naar_csv:
    input:
        script = f'{script_dir}/17_output_naar_csv.py',
        LSW_kaart = f'{lhmmaster_dir}/coupling/lsws.shp',
        WA_kaart = f'{input_dir}/mozart/weirarea_v1.shp',
        **multi(f'{output_dir}'+'/basisdata250m/PEIL_{}_{}.nc', 'peil_{}_{}', [pst, zw]),
        wateroppervlak_totaal = f'{output_dir}/herschaald/totaal_areaal_m2_groveschaal.nc', 
        zout_water = f'{input_dir}/mozart/saltnew.asc', 
        verhard_opp = f'{input_dir}/mozart/verhard250.asc',
        ID_gridcel_metaswap = f'{input_dir}/mozart/thiessen.asc',
        like_groveschaal = f'{interim_dir}/like_groveschaal.nc',
        LSW_type = f'{input_dir}/Basisdata/LSW_type.shp',
    output:
        LSWattr = f'{output_dir}/mozart/LSWattr.csv',
        MFtoLSW = f'{output_dir}/mozart/MFtoLSW.csv',
        MFtoLSW_zomer = f'{output_dir}/mozart/MFtoLSW_zomer.csv', 
        PlottoLSW = f'{output_dir}/mozart/PlottoLSW.csv',
        WAattr = f'{output_dir}/mozart/WAattr.csv',
        LSW_peilopzet = f'{output_dir}/mozart/LSW_peilopzet_new.csv',
    params:
        celoppervlak_groot = celoppervlak_groot
    script:
        f'{script_dir}/17_output_naar_csv.py'


rule gemiddelde_stationair:
    input:
        script = f'{script_dir}/19_gemiddelde_stationair.py',
        **multi(f'{output_dir}'+'/basisdata250m/PEIL_{}_{}.idf', 'peil_{}_{}', [pst, zw]),
        **multi(f'{output_dir}'+'/basisdata250m/BODH_{}_{}.idf', 'bodemhoogte_{}_{}', [pst, zw]),
        **multi(f'{output_dir}'+'/basisdata250m/COND_{}.idf', 'cond_{}_{}', [pst, zw]),
    output:
        **multi(f'{output_dir}'+'/gemiddeld/PEIL_{}_gemiddelde.idf', 'peil_{}_gemiddelde', [pst]),
        **multi(f'{output_dir}'+'/gemiddeld/BODH_{}_gemiddelde.idf', 'bodemhoogte_{}_gemiddelde', [pst]),
        **multi(f'{output_dir}'+'/gemiddeld/COND_{}_gemiddelde.idf', 'cond_{}_gemiddelde', [pst]),
    script:
        f'{script_dir}/19_gemiddelde_stationair.py'


rule prioriteit_LSW:
    input:
        script = f'{script_dir}/mozart/prioriteit_LSW.py',
        bodem = f'{input_dir}/mozart/Bodem/BODEM_BOFEK.shp',
        natuur = f'{input_dir}/mozart/Natuur/cat1_met_bufferzones.shp',
        doorspoel = f'{input_dir}/mozart/Doorspoelkaart_mmjaar/mzpriority_Huidig.shp',
        beregening = f'{input_dir}/mozart/Beregening/Beregening.shp',
        lsw = f'{input_dir}/mozart/LSWs_peilgebieden/Afwateringseenheden_MOZART.shp',
        landgebruik = f'{input_dir}/mozart/Landgebruikskaart/landbouw.shp',
    output:
        peilbeheer_tabel = f'{interim_dir}/mozart/lsw_peilbeheerst.csv',
        doorspoeling_tabel = f'{interim_dir}/mozart/lsw_doorspoeling.csv',
        beregening_tabel = f'{interim_dir}/mozart/lsw_beregening.csv',
    script:
        f'{script_dir}/mozart/prioriteit_LSW.py'


rule overzichtstabel_prioriteit_LSW:
    input:
        script = f'{script_dir}/mozart/overzichtstabel_prioriteit_LSW.py',
        peilbeheer_tabel = f'{interim_dir}/mozart/lsw_peilbeheerst.csv',
        doorspoeling_tabel = f'{interim_dir}/mozart/lsw_doorspoeling.csv',
        beregening_tabel = f'{interim_dir}/mozart/lsw_beregening.csv',
        lsw_RDO = f'{input_dir}/mozart/LSWs_peilgebieden/Intersection_lsw_RDO.shp',
        lsw = f'{input_dir}/mozart/LSWs_peilgebieden/Afwateringseenheden_MOZART.shp',
    output:
        totaal_tabel = f'{output_dir}/mozart/totale_prioritering_LSW.csv',
    script:
        f'{script_dir}/mozart/overzichtstabel_prioriteit_LSW.py'


rule integreren_mozart:
    input:
        script = f'{script_dir}/mozart/integreren_mozart.py',
        totaal_tabel = f'{output_dir}/mozart/totale_prioritering_LSW.csv',
        uslswdem_dik = f'{input_dir}/mozart/uslswdem.dik',
    output:
        uslsw_new = f'{interim_dir}/mozart/uslsw_verdringingsreeks.dik',
        uslswdem_new = f'{interim_dir}/mozart/uslswdem_verdringingsreeks.dik',
    script:
        f'{script_dir}/mozart/integreren_mozart.py'

rule correctie_peilopzet:
    input:
        script = f'{script_dir}/mozart/corr_peilopzet.py',
        lsw = f'{input_dir}/mozart/mozartin/lsw.dik',
        uslswdem = f'{interim_dir}/mozart/uslswdem_verdringingsreeks.dik',
        LSW_peilopzet = f'{output_dir}/mozart/LSW_peilopzet_new.csv',
    output:
        uslsw_new = f'{interim_dir}/mozart/uslsw_peilopzet.dik',
        uslswdem_new = f'{interim_dir}/mozart/uslswdem_peilopzet.dik',
    script:
        f'{script_dir}/mozart/corr_peilopzet.py'


rule mozart_zeeland:
    input:
        script = f'{script_dir}/mozart/lhm_zeeland.py',
        lsws = f'{lhmmaster_dir}/coupling/lsws.shp',
        lbwl_voorzgebied = f'{input_dir}/mozart/zeeland/lbwl_voorzgebied.shp',
        uslsw = f'{interim_dir}/mozart/uslsw_peilopzet.dik',
        uslswdem = f'{interim_dir}/mozart/uslswdem_peilopzet.dik',
        lsw = f'{input_dir}/mozart/mozartin/lsw.dik',
        ladvalue = f'{input_dir}/mozart/mozartin/ladvalue.dik',
        vadvalue = f'{input_dir}/mozart/mozartin/vadvalue.dik',
        vlvalue = f'{input_dir}/mozart/mozartin/vlvalue.dik',
        weirarea = f'{input_dir}/mozart/mozartin/weirarea.dik',
        lswattr = f'{output_dir}/mozart/LSWattr.csv',
        waattr = f'{output_dir}/mozart/WAattr.csv',
        MFtoLSW = f'{output_dir}/mozart/MFtoLSW.csv',
    output:
        uslsw_out = f'{output_dir}/mozart/mozartin/uslsw.dik',
        uslswdem_out = f'{output_dir}/mozart/mozartin/uslswdem.dik',
        lsw_out = f'{output_dir}/mozart/mozartin/lsw.dik',
        ladvalue_out = f'{output_dir}/mozart/mozartin/ladvalue.dik',
        vadvalue_out = f'{output_dir}/mozart/mozartin/vadvalue.dik',
        vlvalue_out = f'{output_dir}/mozart/mozartin/vlvalue.dik',
        weirarea_out = f'{output_dir}/mozart/mozartin/weirarea.dik',
        waattr = f'{output_dir}/mozart/mozartin/WAattr.csv',
        MFtoLSW = f'{output_dir}/mozart/mozartin/MFtoLSW.csv',
    script:
        f'{script_dir}/mozart/lhm_zeeland.py'


mozartin_output_filenames = [
    'dw.dik', 
    'dwvalue.dik', 
    'ladvalue.dik', 
    'lsw.dik', 
    'lswROUT.err', 
    'lswrouting.dik', 
    'lswurban.dik', 
    'lswVADV.err', 
    'lswvalue.dik', 
    'lswVLV.err', 
    'Mozart.err', 
    'uslsw.dik', 
    'uslswdem.dik', 
    'vadvalue.dik', 
    'vlvalue.dik', 
    'wavalue.dik', 
    'weirarea.dik', 
]

rule run_mozartin:
    input:
        exe = r'e:\LHM_master\Software/exe/MozartIn_rev1193.exe',
        prm = f'{input_dir}/mozart/mozartin/mozart.prm',
        dw = f'{lhmmaster_dir}/mozart/mozartin/dw.dik',
        dwvalue = f'{lhmmaster_dir}/mozart/mozartin/dwvalue.dik',
        lsw = f'{lhmmaster_dir}/mozart/mozartin/lsw.dik',
        lswvalue = f'{lhmmaster_dir}/mozart/mozartin/lswvalue.dik',
        ladvalue = f'{lhmmaster_dir}/mozart/mozartin/ladvalue.dik',
        lswrouting = f'{lhmmaster_dir}/mozart/mozartin/lswrouting.dik',
        uslsw = f'{output_dir}/mozart/uslsw.dik',
        uslswdem = f'{output_dir}/mozart/uslswdem.dik',
        weirarea = f'{lhmmaster_dir}/mozart/mozartin/weirarea.dik',
        vadvalue = f'{lhmmaster_dir}/mozart/mozartin/vadvalue.dik',
        vlvalue = f'{lhmmaster_dir}/mozart/mozartin/vlvalue.dik',
        lswattr = f'{output_dir}/mozart/LSWattr.csv',
        waattr = f'{output_dir}/mozart/WAattr.csv',
        lswrouting_dbc = f'{lhmmaster_dir}/mozart/mozartin/LswRouting_dbc.dik',
    output:
        expand(f'{output_dir}/mozart/mozartin/{{filename}}', filename = mozartin_output_filenames),
    script:
        f'{script_dir}/run_mozartin.py'
